const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
const FCM = require('fcm-node')
// var serverKey = 'AAAAhiv3_GE:APA91bF49fXdpiIvJY49UxPX8nscslDHw7dbGN2nObklSNBMhhizZGORhLzoZcRZQ5XvX7rmhpsiTCr9GT6CQjGFfTd_C0cH2vcKnPkyqrfqvB8FjMORirBSgUOJab_B-umq8AYGKTVI';
var serverKey = require("../config/packag-file.json");
var fcm = new FCM(serverKey);
var twilio = require('twilio');
var path = require('path')
//attach the plugin to the nodemailer transporter
// const client = new twilio(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'prince.bhatia.henceforth@gmail.com',
    pass: 'nozuwdslsyadzbkh'
  }
});
const options = {
  viewEngine: {
    partialsDir: path.join(__dirname, "../public/views/partials"),
    layoutsDir: path.join(__dirname, "../public/views/layouts"),
    extname: ".hbs"
  },
  extName: ".hbs",
  viewPath: ""
};
transporter.use("compile", hbs(options));

class Common {
  static async sendMail(email, url, subject, text, link_text) {
    var date = new Date();
    var year = date.getFullYear(); 


    try {
      var mailOptions = {

        from: 'prince.bhatia.henceforth@gmail.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/views/email-verification"),
        context: {
          // Data to be sent to template engine.
          url: 'https://henceforthsolutions.com/',
          text: text,
          subject: subject,
          year: year,
          link_text: link_text,
          link_url: url
        }
      }

      let mail = await transporter.sendMail(mailOptions)
      console.log(mail);

    } catch (err) {
      console.log(err);
      throw err;
    }

    return "success"
  }

  //Generate Random String
  static async generateRandomString(length) {
    var text = '';
    const possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  //common function for push notification
  static async push(fcm_ids, notification, data, collapse_key = null) {
    console.log("push start")

    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
      // to: fcm_ids[0],
      registration_ids: fcm_ids,
      notification: notification,
      data: data
    };

    fcm.send(message, function (err, response) {
      if (err) {
        console.log("Something has gone wrong!");
      } else {
        console.log("Successfully sent with response: ", response);
        return response
      }
    });
  }

  //send messages
  static async sendMessage(number, body) {
    // client.messages.create(
    //   {
    //     to: number,
    //     from: process.env.TWILIO_FROM,
    //     body: body,
    //   },
    //   function (error, message) {
    //     if (!error) {
    //       console.log('Success! The SID for this SMS message is:');
    //       console.log(message.sid);
    //       console.log('Message sent on:');
    //       console.log(message.dateCreated);
    //     } else {

    //       console.log('Oops! There was an error.');
    //       console.log(error)
    //       throw ('Oops! There was an error.'
    //       );
    //       //return error.Error;
    //     }
    //   },
    // );
  }

  static async generateRandomNumber(length) {
    var text = "";
    const possible = "123456789";

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    var random = parseInt(text);
    return random;
  }

  static async calculateOffset(page, limit) {
    var offset = 0
    if (page > 0 && limit) {
      var offset = (page - 1) * limit;
    } else if (limit) {
      var limit = limit
    } else {
      var limit = 20
    }
    return { offset, limit }
  }

  static async minPage(page = 1) {
    if (!page || page <= 1) {
      page = 0;
    }
    console.log("page ", page);
    return page;
  }

  static async minLimit(limits = 20, default_limit = null) {
    var limit = parseInt(limits, 10);
    if (isNaN(limit)) {
      limit = 10;
    } else if (limit < 1) {
      limit = 1;
    }
    console.log("limit ", limit);
    return limit
  }

  static getData(data, limit, offset) {
    let start = offset;
    let end = start + limit;
    let result = data.slice(start, end);
    return result;
  }
}
module.exports = Common;