-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: autoflex
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address_requests`
--

DROP TABLE IF EXISTS `address_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `emirate_id` int(11) DEFAULT NULL,
  `area` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id_fk` (`user_id`),
  KEY `emirate_id_fk` (`emirate_id`),
  CONSTRAINT `emirate_id_fk` FOREIGN KEY (`emirate_id`) REFERENCES `emirates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_requests`
--

LOCK TABLES `address_requests` WRITE;
/*!40000 ALTER TABLE `address_requests` DISABLE KEYS */;
INSERT INTO `address_requests` VALUES (1,91,1,'string','2020-12-16 12:44:02');
/*!40000 ALTER TABLE `address_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `emirate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_53c6f46d63bde28342456fac837` (`emirate_id`),
  CONSTRAINT `emirate_id` FOREIGN KEY (`emirate_id`) REFERENCES `emirates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas`
--

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` VALUES (2,'Al Khawaneej 1',1),(3,'Al Khawaneej 2',1),(4,'Al Mizhar 1',1),(5,'Al Mizhar 2',1),(6,'Al Mushrif',1),(7,'Al Rashidiya',1),(8,'Al Twar 1',1),(9,'Al Twar 2',1),(10,'Al Twar 3',1),(11,'Al Warqa 1',1),(12,'Al Warqa 2',1),(13,'Al Warqa 3',1),(14,'Al Warqa 4',1),(15,'Al Warqa 5',1),(16,'Mirdif',1),(17,'Muhaisnah 1',1),(18,'Muhaisnah 2',1),(19,'Muhaisnah 3',1),(20,'Muhaisnah 4',1),(21,'Nad Al Hamar',1),(22,'Nad Shamma',1),(23,'Oud Al Muteena',1),(24,'Umm Ramool',1),(25,'Warsan 1',1),(26,'Warsan 2',1),(27,'Academic City',1),(28,'Business Bay',1),(29,'Downtown Burj Khalifa',1),(30,'Jumeirah 1',1),(31,'Jumeirah 2',1),(32,'Jumeirah 3',1),(33,'Nad Al Sheba 1',1),(34,'Nad Al Sheba 2',1),(35,'Nad Al Sheba 3',1),(36,'Nad Al Sheba 4',1),(37,'Ras Al Khor Indsutrial Area 1',1),(38,'Ras Al Khor Indsutrial Area 2',1),(39,'Ras Al Khor Indsutrial Area 3',1),(40,'Ras Al Khor',1),(41,'The Villa',1);
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_days`
--

DROP TABLE IF EXISTS `block_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_block_days_fk` (`company_id`),
  CONSTRAINT `company_block_days_fk` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_days`
--

LOCK TABLES `block_days` WRITE;
/*!40000 ALTER TABLE `block_days` DISABLE KEYS */;
INSERT INTO `block_days` VALUES (1,1,'2020-11-04'),(2,1,'2020-12-03'),(3,2,'2020-12-01');
/*!40000 ALTER TABLE `block_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_users`
--

DROP TABLE IF EXISTS `block_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blocked_user` int(11) NOT NULL,
  `blocked_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_users`
--

LOCK TABLES `block_users` WRITE;
/*!40000 ALTER TABLE `block_users` DISABLE KEYS */;
INSERT INTO `block_users` VALUES (3,11,16);
/*!40000 ALTER TABLE `block_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_image` (`image`),
  CONSTRAINT `category_image` FOREIGN KEY (`image`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,'string232332'),(2,NULL,'Car towing & tyre services'),(3,NULL,'Mechanic Service'),(5,NULL,'string'),(6,NULL,'string'),(7,NULL,'string'),(8,NULL,'string');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `trade_license` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `emirate_id` varchar(255) DEFAULT NULL,
  `is_trade_license_verified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 No, 1 Yes ',
  `is_passport_verified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 No, 1 Yes ',
  `is_emirate_id_verified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 No, 1 Yes ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `company_user` (`user_id`),
  CONSTRAINT `company_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,5,'jkbkb','string','striyttung','kjj',0,0,0,'2020-12-09 07:08:24','2020-12-11 06:30:14'),(2,8,'string','3424','string','string',0,0,0,'2020-12-09 07:14:28','2020-12-09 07:14:28'),(3,17,'string','string','string','string',0,0,0,'2020-12-09 12:53:52','2020-12-10 10:01:25'),(4,18,'string','string','string','1',0,0,0,'2020-12-09 14:37:48','2020-12-10 10:01:28'),(5,19,NULL,NULL,NULL,NULL,0,0,0,'2020-12-09 14:49:18','2020-12-10 10:01:30'),(6,57,'ABc','','','',0,0,0,'2020-12-11 10:12:04','2020-12-11 10:12:04'),(7,58,'abc','','','',0,0,0,'2020-12-11 10:24:58','2020-12-11 10:24:58'),(8,59,'Henceforth','','','',0,0,0,'2020-12-11 10:26:27','2020-12-11 10:26:27'),(9,60,'Henceforth','','','',0,0,0,'2020-12-11 10:30:46','2020-12-11 10:30:46'),(10,61,'Henceforth','','','',0,0,0,'2020-12-11 10:32:42','2020-12-11 10:32:42'),(11,62,'Henceforth','','','',0,0,0,'2020-12-11 10:51:32','2020-12-11 10:51:32'),(12,78,'Henceforth','','','',0,0,0,'2020-12-14 05:06:25','2020-12-14 05:06:25'),(13,79,'Henceforth','','','',0,0,0,'2020-12-14 05:07:46','2020-12-14 05:07:46'),(14,80,'Henceforth','','','',0,0,0,'2020-12-14 05:09:38','2020-12-14 05:09:38'),(15,81,'Henceforth','','','',0,0,0,'2020-12-14 05:11:08','2020-12-14 05:11:08'),(16,94,'Henceforth','','','',0,0,0,'2020-12-23 06:01:56','2020-12-23 06:01:56');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `country_code` varchar(5) NOT NULL,
  `description` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `contact_us_ibfk_1` (`user_id`),
  CONSTRAINT `contact_us_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_us`
--

LOCK TABLES `contact_us` WRITE;
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
INSERT INTO `contact_us` VALUES (2,91,'prince','bhatia','prince.bhatia.henceforth@gmail.com','8920039612','91','Hey there , your service is too good.','2020-12-23 11:45:52'),(3,91,'prince','bhatia','prince.bhatia.henceforth@gmail.com','8920039612','91','Hey there , your service is very good.','2020-12-23 11:45:59');
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emirates`
--

DROP TABLE IF EXISTS `emirates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emirates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emirates`
--

LOCK TABLES `emirates` WRITE;
/*!40000 ALTER TABLE `emirates` DISABLE KEYS */;
INSERT INTO `emirates` VALUES (1,'Dubai'),(2,'abc'),(3,'vbn');
/*!40000 ALTER TABLE `emirates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` varchar(255) NOT NULL,
  `storage_type` tinyint(1) NOT NULL COMMENT '1:DO, 2: AWS, 3:Azure, 4:GC, 5:Local',
  `environment` tinyint(1) NOT NULL COMMENT '1:live, 2:test, 3:dev, 4:local',
  `is_default_asset` tinyint(1) NOT NULL COMMENT '0:yes, 1:no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES ('string',5,4,0,'2020-12-15 07:01:31','2020-12-15 07:01:31'),('string22',5,4,0,'2020-12-15 07:50:49','2020-12-15 07:50:49'),('string222',5,4,0,'2020-12-15 07:50:59','2020-12-15 07:50:59'),('string2223',5,4,0,'2020-12-15 07:54:15','2020-12-15 07:54:15');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`,`user_id`),
  KEY `user_like_fk` (`user_id`),
  CONSTRAINT `company_like_fk` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_like_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (4,1,3),(2,1,91),(5,2,12);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `conversation_id` text,
  `message` text,
  `media` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_from` (`user_from`),
  KEY `user_to` (`user_to`),
  KEY `deleted_by` (`deleted_by`),
  KEY `media_file` (`media`),
  CONSTRAINT `deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `media_file` FOREIGN KEY (`media`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_from` FOREIGN KEY (`user_from`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_to` FOREIGN KEY (`user_to`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (12,15,33,'11_33','string',NULL,0,'2020-12-02 15:21:15',NULL),(14,11,33,'11_33','string',NULL,1,'2020-12-02 15:21:25',NULL),(15,16,81,'16_81','string',NULL,0,'2020-12-04 12:19:16',NULL),(16,16,94,'16_94','string',NULL,0,'2020-12-04 12:20:47',NULL),(17,15,94,'15_94','string',NULL,0,'2020-12-04 12:21:02',NULL),(18,15,94,'15_94','string',NULL,0,'2020-12-04 12:51:52',NULL),(20,33,11,'11_33','string',NULL,1,'2020-12-07 13:07:18',NULL),(21,33,11,'11_33','stringfdfgg',NULL,1,'2020-12-07 13:07:24',NULL);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `service_extra_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userfk` (`order_id`),
  KEY `servicefk` (`service_id`),
  KEY `companyfk` (`company_id`),
  KEY `extra_service_fk` (`service_extra_id`),
  KEY `category_fk` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` VALUES (10,91,1,11,2,5,10,60,'2020-12-21 14:43:59'),(11,91,1,11,2,6,8,80,'2020-12-21 14:43:59'),(12,91,1,17,2,5,2,20,'2020-12-21 14:44:52'),(13,91,1,17,2,6,4,60,'2020-12-21 14:44:52'),(20,91,1,21,2,5,2,10,'2020-12-21 15:40:22');
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `worker_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `arrival_time` time DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0 pending, 1 complete',
  `cancelled_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_a922b820eeef29ac1c6800e826a` (`user_id`),
  KEY `FK_02945665b494bb195c5d37c8e98` (`address_id`),
  KEY `vehicle_id` (`vehicle_id`),
  KEY `worker_id` (`worker_id`),
  CONSTRAINT `address_id` FOREIGN KEY (`address_id`) REFERENCES `user_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_order_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vehicle_id` FOREIGN KEY (`vehicle_id`) REFERENCES `user_vehicles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `worker_id` FOREIGN KEY (`worker_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (7,91,NULL,2,3,NULL,NULL,NULL,0,NULL,'2020-12-18 12:50:15','2020-12-18 12:50:15',NULL),(11,91,NULL,2,3,NULL,'2020-12-18',NULL,0,NULL,'2020-12-18 13:06:01','2020-12-18 13:06:01',NULL),(12,91,NULL,2,3,NULL,'2020-12-18',NULL,0,NULL,'2020-12-18 13:09:08','2020-12-18 13:09:08',NULL),(14,91,NULL,2,3,NULL,'2020-12-18',NULL,0,NULL,'2020-12-18 13:12:42','2020-12-18 13:12:42',NULL);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `rating` tinyint(1) DEFAULT NULL,
  `review` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bbd6ac6e3e6a8f8c6e0e8692d63` (`order_id`),
  KEY `FK_78e136f5bd9e23499d0b3922bb9` (`user_id`),
  KEY `FK_efdc857eb20a18cce2887c59c28` (`company_id`),
  CONSTRAINT `company_id` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `review_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,2,1,1,2,'3dd','2020-12-18 06:59:22',NULL),(2,5,12,2,5,'fdsfs','2020-12-18 07:00:17',NULL),(3,8,3,1,2,'324234','2020-12-18 07:00:29',NULL),(4,11,22,1,1,'sfsdf','2020-12-18 07:00:43',NULL);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_business_hours`
--

DROP TABLE IF EXISTS `service_business_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_business_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '0 ALL , 1 selected',
  `day` tinyint(1) DEFAULT NULL COMMENT '1-mon, 2-tue, 3-wed, 4-thu, 5-fri, 6-sat, 7-sun',
  `time_slot_id` int(11) DEFAULT NULL,
  `worker` int(4) DEFAULT '1' COMMENT 'No of worker',
  PRIMARY KEY (`id`),
  KEY `FK_b84d859df6f05aa53282471a518` (`service_id`),
  KEY `time_slot_fk` (`time_slot_id`),
  CONSTRAINT `company_hour_id` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `time_slot_fk` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_business_hours`
--

LOCK TABLES `service_business_hours` WRITE;
/*!40000 ALTER TABLE `service_business_hours` DISABLE KEYS */;
INSERT INTO `service_business_hours` VALUES (1,2,0,1,2,NULL),(8,9,0,1,8,NULL),(10,11,0,1,2,NULL),(11,17,1,1,1,2),(12,17,1,1,2,1),(13,17,1,1,4,5),(14,17,1,1,7,1),(15,17,1,2,4,2),(16,17,1,2,2,1),(17,17,1,2,8,5),(18,17,1,2,7,1),(19,17,1,5,4,2),(20,17,1,5,2,1),(21,17,1,5,8,5),(22,17,1,5,7,1),(29,21,1,1,2,2),(30,21,1,1,3,2),(31,21,1,1,4,2),(32,21,1,2,2,3),(33,21,1,2,3,3),(34,21,1,2,4,3);
/*!40000 ALTER TABLE `service_business_hours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_extras`
--

DROP TABLE IF EXISTS `service_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_extras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `extra_service_id` (`service_id`),
  CONSTRAINT `extra_service_id` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_extras`
--

LOCK TABLES `service_extras` WRITE;
/*!40000 ALTER TABLE `service_extras` DISABLE KEYS */;
INSERT INTO `service_extras` VALUES (5,11,'string',10,'string'),(6,17,'fdffdf',10,'stfdfring'),(7,21,'stdffddring',10,'stfdfddring'),(8,21,'strinfdsfg',20,'strifddfng');
/*!40000 ALTER TABLE `service_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_highlights`
--

DROP TABLE IF EXISTS `service_highlights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_highlights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1 Tick, 2 Circle ',
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_highlight_id` (`service_id`),
  CONSTRAINT `service_highlight_id` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_highlights`
--

LOCK TABLES `service_highlights` WRITE;
/*!40000 ALTER TABLE `service_highlights` DISABLE KEYS */;
INSERT INTO `service_highlights` VALUES (1,9,1,'string'),(8,11,1,'string'),(9,17,2,'string'),(10,17,1,'string'),(11,17,1,'strifdsdffdng'),(12,21,1,'string'),(13,21,2,'string');
/*!40000 ALTER TABLE `service_highlights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_locations`
--

DROP TABLE IF EXISTS `service_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `emirate_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_service_id` (`service_id`),
  KEY `emirate` (`emirate_id`),
  KEY `area_emirate` (`area_id`),
  CONSTRAINT `area_emirate` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `company_service_id` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `emirate` FOREIGN KEY (`emirate_id`) REFERENCES `emirates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_locations`
--

LOCK TABLES `service_locations` WRITE;
/*!40000 ALTER TABLE `service_locations` DISABLE KEYS */;
INSERT INTO `service_locations` VALUES (7,2,1,2),(8,2,1,3),(9,2,1,4),(10,2,1,5),(11,2,1,6),(12,2,1,7),(45,9,2,2),(46,9,1,5),(47,9,1,7),(48,9,1,8),(67,11,1,6),(68,11,1,7),(69,12,1,3),(70,12,1,5),(71,12,1,6),(72,13,1,3),(73,13,1,5),(74,13,1,6),(75,14,1,3),(76,14,1,5),(77,14,1,6),(78,15,1,3),(79,15,1,5),(80,15,1,6),(81,16,1,3),(82,16,1,5),(83,16,1,6),(84,17,1,3),(85,17,1,5),(86,17,1,6),(91,21,1,5),(92,21,1,8);
/*!40000 ALTER TABLE `service_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_vehicles`
--

DROP TABLE IF EXISTS `service_vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `vehicle_type_id` int(11) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_type_id` (`vehicle_type_id`),
  KEY `company_service_idd` (`service_id`),
  CONSTRAINT `company_service_idd` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vehicle_type_id` FOREIGN KEY (`vehicle_type_id`) REFERENCES `vehicle_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_vehicles`
--

LOCK TABLES `service_vehicles` WRITE;
/*!40000 ALTER TABLE `service_vehicles` DISABLE KEYS */;
INSERT INTO `service_vehicles` VALUES (4,9,1,2),(5,9,2,42),(8,11,1,20),(9,17,1,20),(10,17,2,20),(11,21,1,20),(12,21,4,203);
/*!40000 ALTER TABLE `service_vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `service_time` time DEFAULT NULL,
  `reach_time` time DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_powered` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 No, 1 Yes',
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `service_category` (`category_id`),
  KEY `service_sub_category` (`sub_category_id`),
  KEY `service_company` (`company_id`),
  CONSTRAINT `service_category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `service_company` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `service_sub_category` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (2,1,3,1,25,NULL,NULL,NULL,NULL,0,0,NULL),(9,1,5,1,34,NULL,'00:44:56','02:44:55','string',0,0,NULL),(11,2,1,1,10,NULL,'11:11:11','34:33:44','string',10,0,'2020-12-11 15:35:53'),(12,1,6,1,10,NULL,NULL,NULL,NULL,0,0,'2020-12-14 16:15:32'),(13,1,7,1,10,NULL,NULL,NULL,NULL,0,0,'2020-12-14 16:16:34'),(14,1,8,1,10,NULL,NULL,NULL,NULL,0,0,'2020-12-14 16:17:01'),(15,1,9,1,10,NULL,NULL,NULL,NULL,0,0,'2020-12-14 16:18:26'),(16,1,10,1,10,NULL,NULL,NULL,NULL,0,0,'2020-12-14 16:20:38'),(17,2,10,1,10,NULL,'20:20:20','30:20:42','string',1,0,'2020-12-14 16:22:08'),(21,3,3,2,20,NULL,'04:33:32','30:34:34','string',1,0,'2020-12-15 13:29:44');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static_pages`
--

DROP TABLE IF EXISTS `static_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_images` (`image`),
  CONSTRAINT `page_images` FOREIGN KEY (`image`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_pages`
--

LOCK TABLES `static_pages` WRITE;
/*!40000 ALTER TABLE `static_pages` DISABLE KEYS */;
INSERT INTO `static_pages` VALUES (1,NULL,'About-Us','<p>About-Us About Project Admin AutoFlex</p><p>25</p>');
/*!40000 ALTER TABLE `static_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_categories`
--

LOCK TABLES `sub_categories` WRITE;
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` VALUES (1,2,'Car towing'),(2,2,'Accident Towing'),(3,2,'Battery Jumpstart'),(4,2,'Flat tyre Replace'),(5,2,'Car key Repair'),(6,1,'car wash'),(7,1,'car dryclean'),(8,3,'mehcanic service 1'),(9,3,'mechanic service 2'),(10,3,'mechanic service 3'),(11,7,'string'),(12,7,'string'),(13,8,'string'),(14,8,'string');
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_slots`
--

DROP TABLE IF EXISTS `time_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_slots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_slot` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_slots`
--

LOCK TABLES `time_slots` WRITE;
/*!40000 ALTER TABLE `time_slots` DISABLE KEYS */;
INSERT INTO `time_slots` VALUES (1,10),(2,11),(3,12),(4,1),(5,2),(7,3),(8,4),(9,5),(10,6),(11,7),(12,8),(13,9),(14,10),(15,11),(16,12);
/*!40000 ALTER TABLE `time_slots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `transaction_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-payment, 2-extra',
  `card_last_four` int(4) DEFAULT NULL,
  `invoice_id` varchar(100) DEFAULT NULL,
  `currency` varchar(10) NOT NULL DEFAULT 'USD',
  `description` varchar(100) DEFAULT NULL,
  `amount` float DEFAULT NULL COMMENT 'negative means refund',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fba75deb63bb89de7b5fc92746a` (`order_id`),
  KEY `FK_e9acc6efa76de013e8c1553ed2b` (`user_id`),
  KEY `FK_80ad48141be648db2d84ff32f79` (`card_id`),
  KEY `FK_9c5709f984dda0ced648bdf6f9e` (`service_id`),
  CONSTRAINT `company_service_t_id` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_id_t` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_addresses`
--

DROP TABLE IF EXISTS `user_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `emirate_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0' COMMENT '0-not default address, 1-default address',
  `type` tinyint(1) NOT NULL COMMENT '1 House, 2 Appartment, 3 Work',
  `street_name` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `extra_direction` varchar(100) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7a5100ce0548ef27a6f1533a5ce` (`user_id`),
  KEY `FK_3c9b4d155e1f5cc98d6b7063050` (`area_id`),
  KEY `FK_92e5757bf62b8f1368dc42b0973` (`emirate_id`),
  CONSTRAINT `area_id` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_address_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_emirate_id` FOREIGN KEY (`emirate_id`) REFERENCES `emirates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_addresses`
--

LOCK TABLES `user_addresses` WRITE;
/*!40000 ALTER TABLE `user_addresses` DISABLE KEYS */;
INSERT INTO `user_addresses` VALUES (9,91,1,2,0,1,NULL,'string','string',23.2222,31.3232,'2020-12-22 09:51:29',NULL,NULL),(10,91,1,2,0,1,NULL,'string','string',23.2222,31.3232,'2020-12-22 09:51:41',NULL,NULL),(11,91,1,2,0,1,NULL,'string','string',23.2222,31.3232,'2020-12-22 09:52:19',NULL,NULL),(12,91,1,2,0,1,NULL,'string','string',23.2222,31.3232,'2020-12-22 09:53:41',NULL,NULL),(13,91,1,2,0,1,NULL,'string','string',23.2222,31.3232,'2020-12-22 09:55:30',NULL,NULL),(14,91,1,2,1,1,NULL,'string','string',23.2222,31.3232,'2020-12-22 09:56:04',NULL,NULL),(15,91,1,2,0,1,NULL,'string','string',23.2222,31.3232,'2020-12-22 09:56:30',NULL,NULL);
/*!40000 ALTER TABLE `user_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_bank_accounts`
--

DROP TABLE IF EXISTS `user_bank_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bank_accounts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_name` varchar(25) DEFAULT NULL,
  `branch` varchar(25) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `account_no` varchar(20) DEFAULT NULL,
  `ifsc` varchar(11) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_bank_accounts`
--

LOCK TABLES `user_bank_accounts` WRITE;
/*!40000 ALTER TABLE `user_bank_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_bank_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_carts`
--

DROP TABLE IF EXISTS `user_carts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `service_extra_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userfk` (`user_id`),
  KEY `servicefk` (`service_id`),
  KEY `companyfk` (`company_id`),
  KEY `extra_service_fk` (`service_extra_id`),
  KEY `category_fk` (`category_id`),
  CONSTRAINT `category_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `companyfk` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `extra_service_fk` FOREIGN KEY (`service_extra_id`) REFERENCES `service_extras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `servicefk` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userfk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_carts`
--

LOCK TABLES `user_carts` WRITE;
/*!40000 ALTER TABLE `user_carts` DISABLE KEYS */;
INSERT INTO `user_carts` VALUES (10,91,1,11,2,5,10,60,'2020-12-21 14:43:59'),(11,91,1,11,2,6,8,80,'2020-12-21 14:43:59'),(12,91,1,17,2,5,2,20,'2020-12-21 14:44:52'),(13,91,1,17,2,6,4,60,'2020-12-21 14:44:52'),(20,91,1,21,2,5,2,10,'2020-12-21 15:40:22');
/*!40000 ALTER TABLE `user_carts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_credits`
--

DROP TABLE IF EXISTS `user_credits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_credits` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaciton_id` (`transaction_id`),
  KEY `user_credit_id` (`user_id`),
  CONSTRAINT `transaciton_id` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_credit_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_credits`
--

LOCK TABLES `user_credits` WRITE;
/*!40000 ALTER TABLE `user_credits` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_credits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_logins`
--

DROP TABLE IF EXISTS `user_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_type` tinyint(4) NOT NULL COMMENT '1-Android, 2-ios',
  `fcm_id` text,
  `device_id` varchar(250) NOT NULL,
  `expired_at` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_id` (`device_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_logins`
--

LOCK TABLES `user_logins` WRITE;
/*!40000 ALTER TABLE `user_logins` DISABLE KEYS */;
INSERT INTO `user_logins` VALUES (10,91,0,'string','string',NULL,'2020-11-03 06:00:22'),(11,11,1,'titiuti','',NULL,'2020-12-01 06:38:53');
/*!40000 ALTER TABLE `user_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_vehicles`
--

DROP TABLE IF EXISTS `user_vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `vehicle_type` int(11) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0' COMMENT '0-not default address, 1-default address',
  `make` varchar(100) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `year` varchar(5) DEFAULT NULL,
  `plate_no` varchar(20) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `vehicle_type` (`vehicle_type`),
  KEY `image` (`image`),
  CONSTRAINT `image` FOREIGN KEY (`image`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vehicle_type` FOREIGN KEY (`vehicle_type`) REFERENCES `vehicle_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_vehicles`
--

LOCK TABLES `user_vehicles` WRITE;
/*!40000 ALTER TABLE `user_vehicles` DISABLE KEYS */;
INSERT INTO `user_vehicles` VALUES (3,91,'string222',2,0,'string','string','2020','string','string','string','2020-12-15 13:20:59'),(4,91,'string2223',2,0,'string','string','2020','string','string','string','2020-12-15 13:24:15');
/*!40000 ALTER TABLE `user_vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `user_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Customer, 2:Owner, 3:Worker,4:Admin',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `profile_image` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `country_abbr` varchar(5) DEFAULT NULL,
  `whatsapp_number` varchar(15) DEFAULT NULL,
  `whatsapp_country_code` varchar(5) DEFAULT NULL,
  `fb_id` varchar(100) DEFAULT NULL,
  `google_id` varchar(100) DEFAULT NULL,
  `apple_id` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `description` text,
  `reset_pass_token` varchar(10) DEFAULT NULL,
  `email_verify_token` varchar(10) DEFAULT NULL,
  `phone_verify_code` varchar(10) DEFAULT NULL,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 No, 1 Yes ',
  `is_email_verified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 No, 1 Yes ',
  `is_deactivated` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 No, 1 Yes ',
  `is_blocked` tinyint(1) DEFAULT '0' COMMENT '0 No, 1 Yes ',
  `deactivate_reason` text,
  `is_email_notified` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 No, 1 Yes ',
  `is_push_notified` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 No, 1 Yes ',
  `is_sms_notified` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 No, 1 Yes ',
  `is_online` tinyint(1) DEFAULT '0' COMMENT '0 Away, 1 online',
  `is_profile_completed` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 No, 1 Yes ',
  `last_seen_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `company_user_id` (`company_id`),
  KEY `user_image` (`profile_image`),
  CONSTRAINT `company_user_id` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,4,'Admin','Panel',NULL,NULL,'admin@gmail.com','$2b$10$Vp.Li7kU6u087BKtfZejcuC5NkcAw8CPjZFIRck6xl9KpxRB6yjo6','24324323',NULL,NULL,'324234432','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-08 18:23:59','2020-12-08 12:53:59','2020-12-23 05:42:09',NULL),(2,2,3,'dfssfd','dfsf',NULL,'prince','fsfd','$2b$10$JMeCvTzKCXjuir0t72bnDORgM5c/Dzh2rz7GtWKIzjzjUFcCkYa5W','89200039612',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 10:26:47','2020-12-09 04:56:47','2020-12-11 10:58:07',NULL),(3,NULL,2,'prince','bhatia',NULL,NULL,'string@gmail.com','$2b$10$XVWpvyHFywO38IQipLyxMepMm2GOmhUFqZXpaayb3n30eUiJtL/py','324243432',NULL,NULL,'34324','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 12:37:13','2020-12-09 07:07:13','2020-12-11 10:58:07',NULL),(5,NULL,2,'prince','bhatia',NULL,NULL,'stringf@gmail.com','$2b$10$XVWpvyHFywO38IQipLyxMepMm2GOmhUFqZXpaayb3n30eUiJtL/py','324243432',NULL,NULL,'34324','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 12:38:24','2020-12-09 07:08:24','2020-12-11 10:58:07',NULL),(8,NULL,2,'string','string',NULL,NULL,'stringdf@gmail.com','$2b$10$XVWpvyHFywO38IQipLyxMepMm2GOmhUFqZXpaayb3n30eUiJtL/py','914332434342',NULL,NULL,'2332323','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 12:44:28','2020-12-09 07:14:28','2020-12-11 10:58:07',NULL),(9,1,3,'string','string',NULL,'string',NULL,'$2b$10$eVVvcO4SOT0Iec5Yg1d6ruhCVfK8kKxs8IB9pavy2mXykwRz0XrQm','123456789',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 15:13:43','2020-12-09 09:43:43','2020-12-11 10:58:07',NULL),(10,NULL,3,'Worker','Example',NULL,'',NULL,'$2b$10$jRfhsQJirDCrIF3bGNttM.1W5yHb2Ue3/s4jQG2WKGDqaaKXmdxO.','9023456789',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 17:55:51','2020-12-09 12:25:51','2020-12-11 10:58:07',NULL),(11,NULL,3,'Worker1','Example',NULL,'Active',NULL,'$2b$10$RLFwfQCGCW3XP/xduXZFwOHnJbgoYdt1ZJg/I2bDI.WD.sg8gaJDu','9034567890',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 18:00:52','2020-12-09 12:30:52','2020-12-11 10:58:07',NULL),(12,NULL,3,'Reena','Devi',NULL,'hhhhh',NULL,'$2b$10$Ibm.edsBaru6UFzp8mVZ6.7OntQm5ufReMgT.FKXf5XR9RgYK5vLu','9023456784',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 18:01:43','2020-12-09 12:31:43','2020-12-11 10:58:07',NULL),(13,NULL,1,'Jeevan','Signh',NULL,NULL,'string+1@gmail.com','$2b$10$6FJ093NI2Io.d0nRtx.W5O4DBKOZm2chy7kH1pFXbjV6SO4uV.Pxu','231232323',NULL,NULL,'32134234','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 18:20:00','2020-12-09 12:50:00','2020-12-11 10:58:07',NULL),(16,NULL,2,'Jeevan','Signh',NULL,NULL,'string+2@gmail.com','$2b$10$itNwszMzngUXXG8x5xyIUOVNSvYaqdgh54GCplbGVwynlqf5IuObS','23122323',NULL,NULL,'32134234','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 18:23:25','2020-12-09 12:53:25','2020-12-11 10:58:07',NULL),(17,NULL,2,'Jeevan','Signh',NULL,NULL,'string+23@gmail.com','$2b$10$kyTYL0Au3wQEHH.8v/isM.wNJA6AqPbeUCdnxjXYYMcmX92ayshZG','2312872323',NULL,NULL,'32134234','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 18:23:52','2020-12-09 12:53:52','2020-12-11 10:58:07',NULL),(18,NULL,2,'sss','sss',NULL,NULL,'string1@gmail.com','$2b$10$hi58Dxl0.SBuxYeRdEOmH.FfzI.ivoXfiMlW1GUUxrgRCHzlG262e','9056789098',NULL,NULL,'9034856789','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 20:07:48','2020-12-09 14:37:48','2020-12-11 10:58:07',NULL),(19,NULL,2,'wsdwd','dwdw',NULL,NULL,'neha@gmail.com','$2b$10$AE3gWcZ3A3c1lncjr.LHfuYNOxUwmwuL5sGUwTlwAz7vA41jNoJW6','423423434',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 20:19:18','2020-12-09 14:49:18','2020-12-11 10:58:07',NULL),(20,NULL,1,'w335','533545',NULL,NULL,'neha11@gmail.com','$2b$10$EafKg5ey.0T1o94LyHUD/e/qh3mVvn2AGMnu8SsOBMYSR0YQb1/Xy','534534545',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 20:20:29','2020-12-09 14:50:29','2020-12-11 10:58:07',NULL),(21,NULL,1,'dsd','sdsd',NULL,NULL,'newwha@gmail.com','$2b$10$AmX8l1tGzZhzcs7ztUFM9eB.7VG6Ii0KZuswg8wWuTX.DV/SfKHF2','3434324',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-09 20:21:02','2020-12-09 14:51:02','2020-12-11 10:58:07',NULL),(22,NULL,1,'hh','kjh',NULL,NULL,'','$2b$10$Kl8VmTI0n9S.fHmrNqo6YuXBB/IUzRwviA3JkgFiNUk8RR75F..9a','1234567890',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,'ferfef',1,1,1,0,0,'2020-12-09 20:28:09','2020-12-09 14:58:09','2020-12-11 10:58:07',NULL),(23,NULL,1,'Neha1','Dhiman1',NULL,NULL,'neha1@gmail.com','$2b$10$2m19pVpgDYO.JBPyAPDa4OWpkXzK25ufyF6oH2QcWCTkH99VhU4ee','9023456787',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 10:22:16','2020-12-10 04:52:16','2020-12-11 10:58:07',NULL),(24,NULL,1,'Aastha','Sharma',NULL,NULL,'aastha12@gmail.com','$2b$10$xsJHsRkVyBZUnhvKSmWzJuJdvlMb3JGf8ctx6d1AdgeEoqINdZoKe','9067345612',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 10:52:53','2020-12-10 05:22:53','2020-12-11 10:58:07',NULL),(25,NULL,1,'Neha1','ewdewd',NULL,NULL,'ddewde@gmail.com','$2b$10$lKS2IZdSdW1Fj6TV4FVI5eV723cD5AjOtUCELuaB64PO9mJecvDC2','9033232233',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 10:53:40','2020-12-10 05:23:40','2020-12-11 10:58:07',NULL),(26,NULL,1,'dsad','fssdf',NULL,NULL,'sdfdsf@gmail.com','$2b$10$9uYQSbCBHlvYRwsvQgMTvOgZKFZjvzAkFldz.snl5iQuOByxQdCL2','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 10:54:14','2020-12-10 05:24:14','2020-12-11 10:58:07',NULL),(27,NULL,1,'dwed','wdd',NULL,NULL,'ewdewd@gmail.com','$2b$10$si1l7kRFPmTbYv8n49zZF.0sHLcv10KGH/k5WjUwzGFlNRykyvV3C','9034567890',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 11:43:13','2020-12-10 06:13:13','2020-12-11 10:58:07',NULL),(28,NULL,1,'dwedew','dewd',NULL,NULL,'dewd@gmail.com','$2b$10$9RzJWsLFTslmQsBh8fmdCumqXUZ5jsWaoeoKwGuPHSXwXCu9eSE8S','9034567890',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 11:43:49','2020-12-10 06:13:49','2020-12-11 10:58:07',NULL),(29,NULL,1,'dewdfe','ewdewd',NULL,NULL,'edde@gmail.com','$2b$10$nHzN2Ckn52PvgbfAJbK94O4Dv2J.QmccGX7C1ivwUkv0b2MdAI1lq','9034567890',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 11:51:43','2020-12-10 06:21:43','2020-12-11 10:58:07',NULL),(30,NULL,1,'ewfcewf','ewfewf',NULL,NULL,'ewffew@gmail.com','$2b$10$6.Nb8mLx/.LHylUC5M2qi.9E0VxRQeLVdZpweOTarnDw0XnblxnIW','9034567890',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 11:53:01','2020-12-10 06:23:01','2020-12-11 10:58:07',NULL),(31,NULL,0,'string','string',NULL,NULL,'aaa@gmail.com','$2b$10$6gxa5imxduv8Z3xKG3hSouHpoe9R03p/TI38vsCojXssv8UQpgH.O','0',NULL,'IN','9034567890','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 12:03:14','2020-12-10 06:33:14','2020-12-11 10:58:07',NULL),(32,NULL,1,'Meeba','Chawla',NULL,NULL,'aeeeaa@gmail.com','$2b$10$6Niwq2UkNVl5XRc9GjI1Fus3YzspHdSCnPZ8ruuAkQ3q0WbLCoPwS','9034893456',NULL,'IN','9034567890','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 12:03:55','2020-12-10 06:33:55','2020-12-11 10:58:07',NULL),(33,NULL,1,'yggg','cfgvcg',NULL,NULL,'gfhf@gmail.com','$2b$10$AEU/IIJr4bnZIp8nhRhQ0O2TaToEwhXgDV.bMnHze/NloY1Hankmq','9034557890',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 12:55:06','2020-12-10 07:25:06','2020-12-11 10:58:07',NULL),(34,NULL,1,'User','1',NULL,NULL,'User@gmail.com','$2b$10$nBh2jUVNbNNKu3RxPiDA.uOnzbZN8pgvp4JqN/v0fiDHXU.8ewmOe','9056789012',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 13:25:27','2020-12-10 07:55:27','2020-12-11 10:58:07',NULL),(35,NULL,1,'User','2',NULL,NULL,'User2@gmail.com','$2b$10$9i01z4WzomdH6dkZOT9lZ.TpMGV0PeQ4dzIpYQruLnaPy.L3eoR/K','9056789012','591',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 13:26:13','2020-12-10 07:56:13','2020-12-11 10:58:07',NULL),(36,NULL,1,'Neha','Dhiman',NULL,NULL,'NehaD123@gmail.com','$2b$10$Zoe.9X366N7nSsiuRSzn7ufzB1VASz4GP5.2hyjx4CX1uCMan87Ba','9056789090',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 14:29:48','2020-12-10 08:59:48','2020-12-11 10:58:07',NULL),(37,NULL,1,'neha','Dhiman',NULL,NULL,'wdwdw@gmail.com','$2b$10$M2q29NoGPsAu9jczUd0kSeFvrYwHwp2LF8KyYvu/OMHjyJwTQ0UXq','1234568569','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-10 15:20:18','2020-12-10 09:50:18','2020-12-11 10:58:07',NULL),(38,NULL,1,'hhh','iu',NULL,NULL,'wdwd@gmail.com','$2b$10$Faj0w6Z7bNQ4s2RphBZjQuYRvWVqkAYivdkcTx7UEBsqiEu7ejk2e','9056789056','62',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,1,NULL,1,1,1,0,0,'2020-12-10 15:48:25','2020-12-10 10:18:25','2020-12-11 11:40:39',NULL),(45,NULL,1,'deew','wedewd',NULL,NULL,'edewd@gmail.com','$2b$10$YNhqV4e0gMPLbwsfeZE.4.CpnViN3ou8Vo6QNxd2ZbdSv86TkAc4y','9034678912','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,NULL,1,1,1,0,0,'2020-12-10 17:13:57','2020-12-10 11:43:57','2020-12-11 11:29:03',NULL),(46,NULL,1,'saxsx','saxsasa',NULL,NULL,'asxsa@gmail.com','$2b$10$DmACJGBzVN.yy3TEwTk48uX0cTWrKluaTI08ClshwzSsOoZnCutTS','9034678912','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,NULL,1,1,1,0,0,'2020-12-10 17:21:21','2020-12-10 11:51:21','2020-12-11 11:24:53',NULL),(47,NULL,1,'deewd','wdwdwd',NULL,NULL,'wdwed@gmail.com','$2b$10$hTIlRVTYANbmZNm05hzKm.J77ttRSw95m0yauBq1.lP70nBdGfAQy','9045678909','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,2,NULL,1,1,1,0,0,'2020-12-10 17:29:38','2020-12-10 11:59:38','2020-12-11 11:19:24',NULL),(48,NULL,1,'efewf','efewf',NULL,NULL,'ewfewf@gmail.com','$2b$10$JcvQRR/CJBknAKjkcv2GrO8P0LFmxwTydrpK5EtesxbtgPOWk.0oC','9034678914','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,NULL,1,1,1,0,0,'2020-12-10 18:12:16','2020-12-10 12:42:16','2020-12-11 11:21:44',NULL),(49,NULL,1,'Neha','Dhiman',NULL,NULL,'Neha2@Gmail.com','$2b$10$ixdbBFX0iTVT7mYvgNABV.LBGztS/HIZ3d2E5nkk3DIN9BfP8q26m','9034678914','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,NULL,1,1,1,0,0,'2020-12-10 18:18:31','2020-12-10 12:48:31','2020-12-11 11:21:04',NULL),(50,NULL,1,'Neha','Dhiman',NULL,NULL,'Neha7@gmail.com','$2b$10$Zt8U5x2uiBE3VDVEAxGOP.O8d6VCMSTgx9Uxj6MGUbf5Og0kiAPiK','9034678910','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,NULL,1,1,1,0,0,'2020-12-10 18:19:21','2020-12-10 12:49:21','2020-12-11 11:20:58',NULL),(51,NULL,1,'ewd','wwe',NULL,NULL,'edwede@gmail.com','$2b$10$YJqOyKMPjbqI2et6djBGcOsvhvnLualX7aOmeyqwxM0/eSStAbvCy','9023456789','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,2,NULL,1,1,1,0,0,'2020-12-11 10:39:50','2020-12-11 05:09:50','2020-12-11 11:18:08',NULL),(52,NULL,1,'Neha','Dhiman',NULL,NULL,'Neha5@gmail.com','$2b$10$O/oMvGUpvOBoEwSG0JLzReCp/gviyxEu6FoeQiPxYV1aS5i/Rdk9G','9045678901','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,2,NULL,1,1,1,0,0,'2020-12-11 10:40:45','2020-12-11 05:10:45','2020-12-11 11:17:12',NULL),(57,NULL,2,'Neha','Dhiman','',NULL,'Neha52@gmail.com','$2b$10$HiKukGEc6L1tkGieb0jRm.NTzW7vNeM8REFq6BO36cztDYPJjqMcW','8923456123','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 15:42:04','2020-12-11 10:12:04','2020-12-11 14:04:40',NULL),(58,NULL,2,'Neha','Dhiman','',NULL,'Neha9@gmail.com','$2b$10$DzT3Letnauw6Hu02QoxKhea.4vK/IvQTIGpLjnS9lwhskSnLtBzfe','9034567890','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 15:54:58','2020-12-11 10:24:58','2020-12-11 10:24:58',NULL),(59,NULL,2,'Neha','Dhiman','',NULL,'Neha90@gmail.com','$2b$10$nwM6PKnkyfIbpU7gRtij8e/sZZij7soDqmD3bMDgxbcwvRWpfmFiC','9034567898','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 15:56:27','2020-12-11 10:26:27','2020-12-11 10:26:27',NULL),(60,NULL,2,'Admin','Panel','',NULL,'Admin90@gmail.com','$2b$10$78bvLuvBUrT1nqY/miiyT.bruSGptdysUF2GRBPLNo4z9dkBtM9BS','9034123456','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 16:00:46','2020-12-11 10:30:46','2020-12-11 10:30:46',NULL),(61,NULL,2,'neha','Dhiman','',NULL,'Admin12@gmail.com','$2b$10$lQWrRHfLE1v6YcPYmUcDjuK16YzGAO4SlhSPfv9RZVqYInBI99Y6y','9056349086','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 16:02:42','2020-12-11 10:32:42','2020-12-11 11:46:09',NULL),(62,NULL,2,'ff','erfef','',NULL,'Admin1@gmail.com','$2b$10$7Uwu13zuXsBSJoFjylQAquKRyR1Qlt4H18bdEiAt2rGVdD7KjBZ8u','1234567890','353',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 16:21:32','2020-12-11 10:51:32','2020-12-14 05:08:54',NULL),(63,NULL,3,'string','string','strin2','string123',NULL,'$2b$10$xDVznoBAsEESrwrPqo/.3.3DEF0yzJl444rQjySMVI1qi3tXk/9na','8920039123','+91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:18:32','2020-12-11 11:48:32','2020-12-11 11:48:32',NULL),(64,NULL,3,'string','string','strin2','string1234',NULL,'$2b$10$YSpdj63/zql9UakiOj.TEOwbcXUeWZCkRAlV/Cxo9/W05d4mL5/aW','8920039123','+91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:18:47','2020-12-11 11:48:47','2020-12-11 11:48:47',NULL),(65,NULL,3,'string','string','strin2','string12344',NULL,'$2b$10$VCXNVTfU22XgDFntOlfL2u0X0IJbMnLVac7qVcIv4ds/SoFSIh.6e','8920039123','+91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:18:51','2020-12-11 11:48:51','2020-12-11 11:48:51',NULL),(66,NULL,3,'string','string','strin2','string124344',NULL,'$2b$10$eZSXjf7i0pPpjFWnkjAMgOUg5/ThEcR984Ij.nBLiC.ZdCxQLEM7.','8920039123','+91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:18:54','2020-12-11 11:48:54','2020-12-11 11:48:54',NULL),(67,NULL,3,'string','string','strin2','string1243434',NULL,'$2b$10$1p1Wp22ynrI5yxVzrKp8YObnc8nlYi3cSUmuXzoLno/TpOvyAJrWy','8920039123','+91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:22:01','2020-12-11 11:52:01','2020-12-11 11:52:01',NULL),(68,NULL,3,'string','string','strin2','string12e43434',NULL,'$2b$10$qHEvu.NJXW1HKBtexvapl.y0HhuUfgymSo6GB3k1gDIHYVaQ6xwmG','8920039123','+91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:23:10','2020-12-11 11:53:10','2020-12-11 11:53:10',NULL),(69,NULL,3,'string','string','strin2','string12ef43434',NULL,'$2b$10$JP.y7Q2R/rAO90Q4Cg7O3.Nmdf8WLe53ttvDP2Onz3Kq9BMnfPGPa','8920039123','+91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:24:28','2020-12-11 11:54:28','2020-12-11 11:54:28',NULL),(70,NULL,3,'string','string','string','strin',NULL,'$2b$10$CwdYyMhV6VHej2x0SiYrr.rhpsTiH.wdh7W7YI0QD.SC.f682t9ES','34234','43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:26:31','2020-12-11 11:56:31','2020-12-11 11:56:31',NULL),(71,NULL,3,'string','string','string','strinfd',NULL,'$2b$10$GUKwSp6OFZ1H8AIyoL1c8eZmrRGktaU2VZdBiOqVHmqG8Dg/Lm6ge','34234','43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:27:07','2020-12-11 11:57:07','2020-12-11 11:57:07',NULL),(72,NULL,3,'string','string','string','strfinfd',NULL,'$2b$10$o6tpzASIku3GTQHZ/VLq1.hZTRbdSobHQJIKUW2zqRDdKUdnp5itG','34234','43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:28:09','2020-12-11 11:58:09','2020-12-11 11:58:09',NULL),(73,1,3,'string','string','string','strffinfd',NULL,'$2b$10$fjBfkaIml0ZeJEOTB6cCdOIYC4l2KENBkp/bfgWIoEaXU3Ge5I.Ou','34234','43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 17:29:41','2020-12-11 11:59:41','2020-12-11 11:59:41',NULL),(74,NULL,3,'Shivani','Sharma','','Shivani Anand',NULL,'$2b$10$3Hx3Zdtp9U/jYeW81uEGXuNuU1kiMKTch1JNOGdNp3qpyMRCDAaT2','9087563212','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,2,NULL,1,1,1,0,0,'2020-12-11 19:22:35','2020-12-11 13:52:35','2020-12-11 14:15:12',NULL),(75,NULL,3,'Worker','1','','Rajal',NULL,'$2b$10$4iJUjxYLtt5KgYhnULpPBum2w89U.aSCOOx8cHPJFlHHNfYdAdUkG','9056789023','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-11 21:30:59','2020-12-11 16:00:59','2020-12-11 16:00:59',NULL),(76,NULL,3,'string1232','string12321','string','string22',NULL,'$2b$10$text9LxYD/5wMGL48pV90uJBVmSUybVnbTA2CSuThO/L9JlmTLgJi','789456123','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 10:05:08','2020-12-14 04:35:08','2020-12-14 04:35:08',NULL),(77,NULL,1,'Suman','Sharma',NULL,NULL,'dneha4466@gmail.com','$2b$10$MgKVDG.cBhIUQy7IO4eQR.HZIxKOLjRXflJm8B.37lTXFqcOsMnvS','9076451234','86',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 10:32:07','2020-12-14 05:02:07','2020-12-14 05:04:01',NULL),(78,NULL,2,'Neha','Dhiman','',NULL,'dneha4462@gmail.com','$2b$10$uMK9Yyq49jbO8elZkcySRuaiphSyMwjatnnwYfjKWFC0XKzfqEw3S','9067543212','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 10:36:25','2020-12-14 05:06:25','2020-12-14 05:06:25',NULL),(79,NULL,2,'Neha','Dhiman','',NULL,'dneha6677@gmail.com','$2b$10$5.ScG/p8nK.m3q.qV9Qsw.NuP2PUBipHmI7lNribyY1B2Mvh40Xw.','9034596097','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 10:37:46','2020-12-14 05:07:46','2020-12-14 05:07:46',NULL),(80,NULL,2,'Neha','Dhiman','',NULL,'dneha67@gmail.com','$2b$10$u7vuXsLNHPfgZFIvKPh.r.czDaecbQBMI/Ekvd3/ON4zn66kMXB3K','8923424234','243',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 10:39:38','2020-12-14 05:09:38','2020-12-23 06:09:56',NULL),(81,NULL,2,'Admin','Panel','',NULL,'Admin102@gmail.com','$2b$10$Ia3.KVVkVDIVYGn2fNqoUuWIv7N0OxR1St.obbUpG2LQNQACRMIMi','6654654654','64',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 10:41:08','2020-12-14 05:11:08','2020-12-23 06:10:25',NULL),(82,NULL,3,'','Sharma','','Shivani',NULL,'$2b$10$VCSaxxIlJPu9scdMHQU/4uLk76rCxCUIh9tISsdt.9gXzqpiNC8e.','9087654323','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 10:50:22','2020-12-14 05:20:22','2020-12-14 07:11:43',NULL),(83,NULL,1,'string1234','string','string',NULL,'string+21@gmail.com','$2b$10$GAayINXDDRQFGaSoOhHx8OItwDb5LXhqLaG4LNzl9eXGVpbfFieLS','7894562133','91','in','789456213','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 11:52:39','2020-12-14 06:22:39','2020-12-14 06:39:23',NULL),(84,NULL,1,'Neha','Dhiman',NULL,NULL,'dneha06@gmail.com','$2b$10$f2M1Kme3q5WaFjhBegMf.eX.BD9ogZVZBNcVn0ySMiUTOm.o22SIi','9087654323','1',NULL,'9087654321','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 12:01:56','2020-12-14 06:31:56','2020-12-14 06:31:56',NULL),(85,NULL,1,'wedfew','efewf',NULL,NULL,'Shubh@gmail.com','$2b$10$Y.2dAXtSzr4DRZnjJSdx1O6s/FsNKwD.DbpIjM1FliYIovraWnX7y','1234567890','973',NULL,'1234567890','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 12:07:20','2020-12-14 06:37:20','2020-12-14 06:39:40',NULL),(86,NULL,1,'Neha1','Dhiman1',NULL,NULL,'Neha022@gmail.com','$2b$10$frTg7wxP86qIFswkcMtvHuGHvm96tG9eGjKO6906xhkgXK4Qmi2dq','9078653456','1',NULL,'9078653456','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 12:12:03','2020-12-14 06:42:03','2020-12-14 06:42:03',NULL),(87,NULL,1,'ewe','wedde',NULL,NULL,'Nehaw022@gmail.com','$2b$10$nYJhbDDeMPdolMSls4qiQumcTtL2i4q0HMmi9B27xzNv219zef.yK','1234567890','1',NULL,'1234567890','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 12:21:02','2020-12-14 06:51:02','2020-12-14 06:51:09',NULL),(88,NULL,3,'Neha2','Dhiman2','','Sheenam',NULL,'$2b$10$wnYcH2qcB9ZiakkqhDwONeISi.ErrmUolpvAyOvN0gsLDHV8eMn9q','9067654321','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 12:22:56','2020-12-14 06:52:56','2020-12-14 06:52:56',NULL),(89,NULL,3,'Neha','Sharma','','Neha022@gmail.com',NULL,'$2b$10$hrdKIDl4rdrT/uRqYSBKlel.UDo7TBl9cY7uPPbyP0y9fPPAdVziy','9078654321','226',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 12:34:29','2020-12-14 07:04:29','2020-12-14 07:08:54',NULL),(90,NULL,3,'Sapna','Dhiman','','Sheenam1',NULL,'$2b$10$fQ/azO8YBjGdihaogffcEO44cDRaOA3ZANB2vzczm7IDV.L0H3EtC','9087554545','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,0,NULL,1,1,1,0,0,'2020-12-14 12:49:33','2020-12-14 07:19:33','2020-12-14 07:20:00',NULL),(91,NULL,1,'string','string',NULL,NULL,'user1@gmail.com','$2b$10$YCWY0o.SDclZlU3pmVvizO019anVYLBLL6TZXriStQpcnTLbSq9dC','789456321','91',NULL,'789456321','91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-15 10:45:34','2020-12-15 05:15:34','2020-12-23 05:42:54',NULL),(92,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'string','string','string',NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,1,1,1,0,0,'2020-12-16 15:46:33','2020-12-16 10:16:33','2020-12-16 10:16:33',NULL),(93,NULL,1,'Neha','Dhima',NULL,NULL,'dneha4476@gmail.com','$2b$10$zi1VEWJkXDzjbcbuHIgC2eLl/dGh6h7/9c0ntJ09NN56LkZ7K.nD.','9087542378','297',NULL,'9087542378','297',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'111111',0,0,0,1,NULL,1,1,1,0,0,'2020-12-23 11:05:48','2020-12-23 05:35:48','2020-12-23 05:36:03',NULL),(94,NULL,2,'Neha','Dhiman','',NULL,'dneha4766@Gmail.com','$2b$10$rn1Ilp7aVi/718pDvaxZ7O98cMHojx7nC.5O.JEPLFtcrYUBhKADW','9087654323','355',NULL,'9087654323','355',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1,NULL,1,1,1,0,0,'2020-12-23 11:31:56','2020-12-23 06:01:56','2020-12-23 06:05:18',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_types`
--

DROP TABLE IF EXISTS `vehicle_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `image_vehicle` (`image`),
  CONSTRAINT `image_vehicle` FOREIGN KEY (`image`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_types`
--

LOCK TABLES `vehicle_types` WRITE;
/*!40000 ALTER TABLE `vehicle_types` DISABLE KEYS */;
INSERT INTO `vehicle_types` VALUES (1,'SEDAN',NULL,NULL),(2,'SUV (5 Seater)',NULL,NULL),(3,'SUV (7 Seater)',NULL,NULL),(4,'MOTORCYCLE',NULL,NULL);
/*!40000 ALTER TABLE `vehicle_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `withdraw_requests`
--

DROP TABLE IF EXISTS `withdraw_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `withdraw_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'owner id',
  `amount` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-requested, 2-released, 3-cancelled',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_6132d6b711440ce13b37f1f0210` (`user_id`),
  CONSTRAINT `FK_6132d6b711440ce13b37f1f0210` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `withdraw_requests`
--

LOCK TABLES `withdraw_requests` WRITE;
/*!40000 ALTER TABLE `withdraw_requests` DISABLE KEYS */;
INSERT INTO `withdraw_requests` VALUES (1,1,233,3,'2020-12-14 12:55:03','2020-12-14 12:55:03'),(2,5,100,1,'2020-12-14 13:07:25','2020-12-14 13:07:25'),(3,5,100,1,'2020-12-14 13:08:15','2020-12-14 13:08:15');
/*!40000 ALTER TABLE `withdraw_requests` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-23 12:06:19
