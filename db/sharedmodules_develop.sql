-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2020 at 10:58 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sharedmodulesnodejs`
--

-- --------------------------------------------------------

--
-- Table structure for table `block_user`
--

CREATE TABLE `block_user` (
  `id` int(11) NOT NULL,
  `blocked_user` int(11) NOT NULL,
  `blocked_by` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0 = unblock , 1 = blocked'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `block_user`
--

INSERT INTO `block_user` (`id`, `blocked_user`, `blocked_by`, `status`) VALUES
(3, 11, 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `user_id`, `message`) VALUES
(1, 16, 'hello'),
(2, 16, 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` varchar(255) NOT NULL,
  `storage_type` tinyint(4) NOT NULL COMMENT '1:DO, 2: AWS, 3:Azure, 4:GC, 5:Local',
  `environment` tinyint(4) NOT NULL COMMENT '1:live, 2:test, 3:dev, 4:local',
  `is_default_asset` tinyint(4) NOT NULL COMMENT '0:yes, 1:no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `storage_type`, `environment`, `is_default_asset`, `created_at`, `updated_at`) VALUES
('1599909574248-background-ppt-128984-4971491.jpg', 5, 4, 1, '2020-09-12 11:19:34', '2020-09-12 11:19:34'),
('1599909599046-background-ppt-128984-4971491.jpg', 5, 4, 1, '2020-09-12 11:19:59', '2020-09-12 11:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `conversation_id` text NOT NULL,
  `message` text NOT NULL,
  `media` text NOT NULL DEFAULT '',
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_for_me` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0-nothing,1-for me',
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `user_from`, `user_to`, `conversation_id`, `message`, `media`, `is_read`, `deleted_for_me`, `created_at`) VALUES
(2, 16, 11, '11_16', 'Hello', '', 0, 0, '2020-09-08 14:59:39'),
(4, 8, 16, '8_16', 'How you doin?', '', 0, 0, '2020-09-08 15:54:56'),
(5, 16, 15, '15_16', 'How you doing', '', 0, 1, '2020-09-08 15:55:00'),
(7, 11, 16, '11_16', 'Nodejs', '', 0, 0, '2020-09-10 18:06:32'),
(8, 16, 10, '10_16', 'Angular', '', 0, 0, '2020-09-10 18:06:32'),
(9, 16, 12, '12_16', 'sfddgfghfgh', '', 1, 0, '2020-09-11 09:51:25'),
(10, 16, 13, '13_16', 'Hello world', '', 0, 0, '2020-09-11 12:21:34'),
(11, 13, 16, '13_16', 'Hello world', 'fdfdfxcx ', 1, 0, '2020-09-11 12:21:51'),
(12, 16, 11, '11_16', 'string', 'string', 0, 0, '2020-09-11 12:31:18'),
(14, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:34:56'),
(15, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:36:25'),
(16, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:36:59'),
(17, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:37:59'),
(18, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:38:26'),
(19, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:39:31'),
(20, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:51:17'),
(21, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:51:49'),
(22, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:52:59'),
(23, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:53:16'),
(24, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:53:28'),
(25, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:54:30'),
(26, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:56:49'),
(27, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:57:42'),
(28, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 16:59:54'),
(29, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:00:13'),
(30, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:02:07'),
(31, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:02:30'),
(32, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:02:58'),
(33, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:04:57'),
(34, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:06:45'),
(35, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:07:15'),
(36, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:07:49'),
(37, 16, 13, '13_16', 'Hello world', 'fdfdfxcx ', 0, 0, '2020-09-15 17:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `message`, `created_at`) VALUES
(1, 'first notification', 'first notification', '2020-09-16 11:07:37'),
(2, 'first notification', 'first notification', '2020-09-16 11:08:02'),
(3, 'first notification', 'first notification', '2020-09-16 11:09:30'),
(4, 'first notification', 'first notification', '2020-09-16 11:10:17'),
(5, 'first notification', 'first notification', '2020-09-16 11:10:56'),
(6, 'first notification', 'first notification', '2020-09-16 11:13:05'),
(7, 'first notification', 'first notification', '2020-09-16 11:16:16'),
(8, 'first notification', 'first notification', '2020-09-16 11:18:19'),
(9, 'first notification', 'first notification', '2020-09-16 11:19:23'),
(10, 'first notification', 'first notification', '2020-09-16 11:19:36'),
(11, 'first notification', 'first notification', '2020-09-16 11:23:18'),
(12, 'first notification', 'first notification', '2020-09-16 11:24:26'),
(13, 'first notification', 'first notification', '2020-09-16 11:25:06'),
(14, 'first notification', 'first notification', '2020-09-16 11:25:38'),
(15, 'first notification', 'first notification', '2020-09-16 11:26:12'),
(16, 'first notification', 'first notification', '2020-09-16 11:26:41'),
(17, 'first notification', 'first notification', '2020-09-16 11:27:04'),
(18, 'first notification', 'first notification', '2020-09-16 11:27:29'),
(19, 'first notification', 'first notification', '2020-09-16 11:27:41'),
(20, 'first notification', 'first notification', '2020-09-16 11:27:56'),
(21, 'first notification', 'first notification', '2020-09-16 11:28:09'),
(22, 'first notification', 'first notification', '2020-09-16 11:28:32'),
(23, 'first notification', 'first notification', '2020-09-16 11:28:57'),
(24, 'first notification', 'first notification', '2020-09-16 11:29:33'),
(25, 'first notification', 'first notification', '2020-09-16 11:30:44'),
(26, 'first notification', 'first notification', '2020-09-16 11:31:18'),
(27, 'first notification', 'first notification', '2020-09-16 11:32:57'),
(28, 'first notification', 'first notification', '2020-09-16 11:35:43'),
(29, 'first notification', 'first notification', '2020-09-16 11:36:08'),
(30, 'first notification', 'first notification', '2020-09-16 11:36:39'),
(31, 'first notification', 'first notification', '2020-09-16 11:37:05'),
(32, 'first notification', 'first notification', '2020-09-16 11:38:11'),
(33, 'first notification', 'first notification', '2020-09-16 11:38:32'),
(34, 'first notification', 'first notification', '2020-09-16 11:40:09'),
(35, 'first notification', 'first notification', '2020-09-16 11:42:38'),
(36, 'first notification', 'first notification', '2020-09-16 11:43:10'),
(37, 'first notification', 'first notification', '2020-09-16 11:43:40'),
(38, 'first notification', 'first notification', '2020-09-16 12:56:45'),
(39, 'first notification', 'first notification', '2020-09-16 12:57:00'),
(40, 'first notification', 'first notification', '2020-09-16 12:57:28'),
(41, 'string', 'string', '2020-09-16 17:02:42');

-- --------------------------------------------------------

--
-- Table structure for table `notification_users`
--

CREATE TABLE `notification_users` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notification_users`
--

INSERT INTO `notification_users` (`id`, `notification_id`, `user_id`) VALUES
(1, 5, 16),
(2, 6, 1),
(3, 29, 16),
(4, 30, 1),
(5, 40, 1),
(6, 40, 16),
(7, 40, 3),
(8, 41, 11),
(9, 41, 12),
(10, 41, 13),
(11, 41, 16);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT current_timestamp(),
  `end_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `user_id`, `title`, `description`, `price`, `location`, `start_date`, `end_date`) VALUES
(20, 16, 'string', 'string', '20000.00', 'string', '2020-09-11 13:51:02', NULL),
(21, 16, 'string', 'string', '20000.00', 'string', '2020-09-11 13:53:18', NULL),
(22, 16, 'samebeef 1', 'song', '10000000.00', 'moosewala 1 1', '2020-09-11 13:55:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_images`
--

CREATE TABLE `project_images` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_images`
--

INSERT INTO `project_images` (`id`, `project_id`, `image`, `created_at`, `updated_at`) VALUES
(24, 20, 'string', '2020-09-19 17:08:18', '2020-09-19 17:08:18'),
(25, 21, 'string', '2020-09-19 17:08:18', '2020-09-19 17:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `profile_image` varchar(250) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `apple_id` varchar(255) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `country_code` varchar(250) DEFAULT NULL,
  `country_abbr` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `reset_pass_token` varchar(100) DEFAULT NULL,
  `email_verify_token` varchar(100) DEFAULT NULL,
  `is_email_verified` tinyint(4) NOT NULL DEFAULT 2 COMMENT '1:yes, 2:no',
  `is_deactivated` int(11) NOT NULL DEFAULT 2 COMMENT '1:yes, 2:no',
  `is_blocked` tinyint(4) DEFAULT 2 COMMENT '1:yes,2:no',
  `deactivate_reason` text DEFAULT NULL,
  `is_profile_completed` tinyint(4) NOT NULL DEFAULT 2 COMMENT '1:yes, 2:no',
  `user_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:user, 2:admin, 3:staff',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `profile_image`, `email`, `fb_id`, `google_id`, `apple_id`, `phone_number`, `country_code`, `country_abbr`, `city`, `state`, `country`, `zip`, `password`, `address`, `lat`, `lng`, `reset_pass_token`, `email_verify_token`, `is_email_verified`, `is_deactivated`, `is_blocked`, `deactivate_reason`, `is_profile_completed`, `user_type`, `created_at`, `updated_at`) VALUES
(8, 'string', 'string', 'string', 'muskan@gmail.com', NULL, NULL, NULL, 92386554, NULL, NULL, NULL, NULL, NULL, NULL, '$2b$10$RbYJ733BPrBsw8gf8wa8BedGBcz7TThFWcWRu8MvROc7XwLPLzB7e', 'string', NULL, NULL, '1597747532707', NULL, 2, 2, NULL, NULL, 2, 2, '2020-08-13 13:38:02', '2020-09-18 18:28:30'),
(9, 'Shared1', NULL, NULL, 'shared1@gmail.com', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '$2b$10$4hbiE24ay28l3GnphUO.yOqs.HQwiB0Qk9UtpmzCTAfPiXiw3OjFG', 'string', NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, 2, 1, '2020-08-18 10:01:04', '2020-08-18 10:01:04'),
(10, 'string', NULL, NULL, 'string12@gmail.com', 'string', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'string@12', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, 2, 1, '2020-08-21 05:36:42', '2020-08-21 05:36:42'),
(11, 'string', NULL, NULL, 'string123@gmail.com', 'string12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, 2, 1, '2020-08-21 05:57:12', '2020-08-21 05:57:12'),
(12, 'string', NULL, NULL, 'string124@gmail.com', 'string122', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, 2, 1, '2020-08-21 06:15:38', '2020-08-21 06:15:38'),
(15, 'string', NULL, NULL, 'string1233@gmail.com', NULL, NULL, NULL, 9287348, NULL, NULL, NULL, NULL, NULL, NULL, '$2b$10$.KEB3OIKvLZwaz1VsDaXZehg/s/Y2xNuaSwfxdPzqhPSDbmJolzWa', 'string', NULL, NULL, NULL, '1597990827210', 2, 2, NULL, NULL, 2, 1, '2020-08-21 06:20:27', '2020-08-21 06:20:27'),
(16, 'Jeevanjot', 'singh', 'nhi hai', 'jivanjot1001@gmail.com', NULL, NULL, NULL, 9138448778, '91', NULL, 'ontario', 'Canada', 'Canada', 134003, '$2b$10$lQ7607G6iCQMMQmyO4rEF.Rr/f0U6uC/V38IS5xElAC9zSrwuxdTa', 'Kaziwara', 23, 22, NULL, '1599563876121', 2, 2, 2, NULL, 2, 1, '2020-09-08 11:17:56', '2020-09-08 11:17:56'),
(17, 'admin', 'moosewala', 'nahi dikhaunga pic', 'admin1@gmail.com', NULL, NULL, NULL, 9138448778, '91', NULL, 'Ambala', 'Haryana', 'India', 134003, '$2b$10$8Te9zahMo7ScEoaAmSmpOefj/7BoZBXrrYu8AcY/Zit3gNs/TRCRW', 'Kaziwara, Ambala city', 88, 77, NULL, NULL, 2, 2, NULL, NULL, 2, 2, '2020-09-16 06:01:34', '2020-09-20 08:06:31'),
(18, 'JJ', 'Singh', 'string', 'jivanjot1001+1@gmail.com', NULL, NULL, NULL, 8572878778, '91', NULL, 'ambala city', 'haryana', 'india', NULL, NULL, 'Ambala', NULL, NULL, NULL, NULL, 2, 2, 2, NULL, 2, 3, '2020-09-18 07:02:04', '2020-09-18 07:02:04'),
(20, 'jj', 'singh', 'string', 'jivanjot1001+2@gmail.com', NULL, NULL, NULL, 8929729459, '91', NULL, 'string', 'string', 'string', NULL, '$2b$10$YdafrYNBmTb/OgnZBa24ke8Srjb6trWGnZ73DYIVKL2AqzfXN/u/K', 'ambala', NULL, NULL, NULL, NULL, 2, 2, 2, NULL, 2, 3, '2020-09-18 07:36:54', '2020-09-18 07:36:54'),
(21, 'jj', 'singh', 'string', 'jivanjot1001+3@gmail.com', NULL, NULL, NULL, 8929729459, '91', NULL, 'string', 'string', 'string', NULL, '$2b$10$PhRBK7v7lfjQ5wKVnHRIkupIHTEI7IkbDoFfoXZ4PzsL85k5Co4mi', 'ambala', NULL, NULL, NULL, NULL, 2, 2, 2, NULL, 2, 3, '2020-09-18 07:42:11', '2020-09-18 07:42:11'),
(22, 'project', 'admin', 'string', 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2b$10$2/09C1JGNG63emWlfEu4TOS.nv.E1tAxmATFij0umvMoPWagKLKGa', NULL, NULL, NULL, NULL, '1600589194552', 2, 2, 2, NULL, 2, 2, '2020-09-20 08:06:34', '2020-09-20 08:17:27');

-- --------------------------------------------------------

--
-- Table structure for table `user_logins`
--

CREATE TABLE `user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fcm_id` text DEFAULT NULL,
  `device_id` varchar(250) NOT NULL,
  `expired_at` date DEFAULT NULL,
  `device_type` tinyint(4) NOT NULL COMMENT '1-Android, 2-ios',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_logins`
--

INSERT INTO `user_logins` (`id`, `user_id`, `fcm_id`, `device_id`, `expired_at`, `device_type`, `created_at`) VALUES
(5, 22, 'string', 'string', NULL, 1, '2020-08-13 13:29:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `block_user`
--
ALTER TABLE `block_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_us_ibfk_1` (`user_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_users`
--
ALTER TABLE `notification_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `project_images`
--
ALTER TABLE `project_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_images_ibfk_1` (`project_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_logins`
--
ALTER TABLE `user_logins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `device_id` (`device_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `block_user`
--
ALTER TABLE `block_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `notification_users`
--
ALTER TABLE `notification_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `project_images`
--
ALTER TABLE `project_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_logins`
--
ALTER TABLE `user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD CONSTRAINT `contact_us_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_images`
--
ALTER TABLE `project_images`
  ADD CONSTRAINT `project_images_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_logins`
--
ALTER TABLE `user_logins`
  ADD CONSTRAINT `user_logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
