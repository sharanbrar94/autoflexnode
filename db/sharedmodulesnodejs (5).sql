-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2020 at 08:44 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sharedmodulesnodejs`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `user_id`, `message`) VALUES
(1, 16, 'hello'),
(2, 16, 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` varchar(255) NOT NULL,
  `storage_type` tinyint(4) NOT NULL COMMENT '1:DO, 2: AWS, 3:Azure, 4:GC, 5:Local',
  `environment` tinyint(4) NOT NULL COMMENT '1:live, 2:test, 3:dev, 4:local',
  `is_default_asset` tinyint(4) NOT NULL COMMENT '0:yes, 1:no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `storage_type`, `environment`, `is_default_asset`, `created_at`, `updated_at`) VALUES
('1599909574248-background-ppt-128984-4971491.jpg', 5, 4, 1, '2020-09-12 11:19:34', '2020-09-12 11:19:34'),
('1599909599046-background-ppt-128984-4971491.jpg', 5, 4, 1, '2020-09-12 11:19:59', '2020-09-12 11:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT current_timestamp(),
  `end_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `user_id`, `title`, `description`, `price`, `location`, `start_date`, `end_date`) VALUES
(20, 16, 'string', 'string', '20000.00', 'string', '2020-09-11 13:51:02', NULL),
(21, 16, 'string', 'string', '20000.00', 'string', '2020-09-11 13:53:18', NULL),
(22, 16, 'samebeef 1', 'song', '10000000.00', 'moosewala 1 1', '2020-09-11 13:55:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_images`
--

CREATE TABLE `project_images` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_images`
--

INSERT INTO `project_images` (`id`, `project_id`, `image`, `created_at`, `updated_at`) VALUES
(24, 20, 'string', '2020-09-19 17:08:18', '2020-09-19 17:08:18'),
(25, 21, 'string', '2020-09-19 17:08:18', '2020-09-19 17:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `profile_image` varchar(250) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `apple_id` varchar(255) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `country_code` varchar(250) DEFAULT NULL,
  `country_abbr` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `reset_pass_token` varchar(100) DEFAULT NULL,
  `email_verify_token` varchar(100) DEFAULT NULL,
  `is_email_verified` tinyint(4) NOT NULL DEFAULT 2 COMMENT '1:yes, 2:no',
  `is_deactivated` int(11) NOT NULL DEFAULT 2 COMMENT '1:yes, 2:no',
  `is_blocked` tinyint(4) DEFAULT 2 COMMENT '1:yes,2:no',
  `deactivate_reason` text DEFAULT NULL,
  `is_profile_completed` tinyint(4) NOT NULL DEFAULT 2 COMMENT '1:yes, 2:no',
  `user_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:user, 2:admin, 3:staff',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `profile_image`, `email`, `fb_id`, `google_id`, `apple_id`, `phone_number`, `country_code`, `country_abbr`, `city`, `state`, `country`, `zip`, `password`, `address`, `lat`, `lng`, `reset_pass_token`, `email_verify_token`, `is_email_verified`, `is_deactivated`, `is_blocked`, `deactivate_reason`, `is_profile_completed`, `user_type`, `created_at`, `updated_at`) VALUES
(8, 'string', 'string', 'string', 'muskan@gmail.com', NULL, NULL, NULL, 92386554, NULL, NULL, NULL, NULL, NULL, NULL, '$2b$10$RbYJ733BPrBsw8gf8wa8BedGBcz7TThFWcWRu8MvROc7XwLPLzB7e', 'string', NULL, NULL, '1597747532707', NULL, 2, 2, NULL, NULL, 2, 2, '2020-08-13 13:38:02', '2020-09-18 18:28:30'),
(9, 'Shared1', NULL, NULL, 'shared1@gmail.com', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '$2b$10$4hbiE24ay28l3GnphUO.yOqs.HQwiB0Qk9UtpmzCTAfPiXiw3OjFG', 'string', NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, 2, 1, '2020-08-18 10:01:04', '2020-08-18 10:01:04'),
(10, 'string', NULL, NULL, 'string12@gmail.com', 'string', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'string@12', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, 2, 1, '2020-08-21 05:36:42', '2020-08-21 05:36:42'),
(11, 'string', NULL, NULL, 'string123@gmail.com', 'string12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, 2, 1, '2020-08-21 05:57:12', '2020-08-21 05:57:12'),
(12, 'string', NULL, NULL, 'string124@gmail.com', 'string122', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, 2, 1, '2020-08-21 06:15:38', '2020-08-21 06:15:38'),
(15, 'string', NULL, NULL, 'string1233@gmail.com', NULL, NULL, NULL, 9287348, NULL, NULL, NULL, NULL, NULL, NULL, '$2b$10$.KEB3OIKvLZwaz1VsDaXZehg/s/Y2xNuaSwfxdPzqhPSDbmJolzWa', 'string', NULL, NULL, NULL, '1597990827210', 2, 2, NULL, NULL, 2, 1, '2020-08-21 06:20:27', '2020-08-21 06:20:27'),
(16, 'Jeevanjot', 'singh', 'nhi hai', 'jivanjot1001@gmail.com', NULL, NULL, NULL, 9138448778, '91', NULL, 'ontario', 'Canada', 'Canada', 134003, '$2b$10$lQ7607G6iCQMMQmyO4rEF.Rr/f0U6uC/V38IS5xElAC9zSrwuxdTa', 'Kaziwara', 23, 22, NULL, '1599563876121', 2, 2, 2, NULL, 2, 1, '2020-09-08 11:17:56', '2020-09-08 11:17:56'),
(17, 'admin', 'moosewala', 'nahi dikhaunga pic', 'admin@gmail.com', NULL, NULL, NULL, 9138448778, '91', NULL, 'Ambala', 'Haryana', 'India', 134003, '$2b$10$8Te9zahMo7ScEoaAmSmpOefj/7BoZBXrrYu8AcY/Zit3gNs/TRCRW', 'Kaziwara, Ambala city', 88, 77, NULL, NULL, 2, 2, NULL, NULL, 2, 2, '2020-09-16 06:01:34', '2020-09-16 06:01:34'),
(18, 'JJ', 'Singh', 'string', 'jivanjot1001+1@gmail.com', NULL, NULL, NULL, 8572878778, '91', NULL, 'ambala city', 'haryana', 'india', NULL, NULL, 'Ambala', NULL, NULL, NULL, NULL, 2, 2, 2, NULL, 2, 3, '2020-09-18 07:02:04', '2020-09-18 07:02:04'),
(20, 'jj', 'singh', 'string', 'jivanjot1001+2@gmail.com', NULL, NULL, NULL, 8929729459, '91', NULL, 'string', 'string', 'string', NULL, '$2b$10$YdafrYNBmTb/OgnZBa24ke8Srjb6trWGnZ73DYIVKL2AqzfXN/u/K', 'ambala', NULL, NULL, NULL, NULL, 2, 2, 2, NULL, 2, 3, '2020-09-18 07:36:54', '2020-09-18 07:36:54'),
(21, 'jj', 'singh', 'string', 'jivanjot1001+3@gmail.com', NULL, NULL, NULL, 8929729459, '91', NULL, 'string', 'string', 'string', NULL, '$2b$10$PhRBK7v7lfjQ5wKVnHRIkupIHTEI7IkbDoFfoXZ4PzsL85k5Co4mi', 'ambala', NULL, NULL, NULL, NULL, 2, 2, 2, NULL, 2, 3, '2020-09-18 07:42:11', '2020-09-18 07:42:11');

-- --------------------------------------------------------

--
-- Table structure for table `user_logins`
--

CREATE TABLE `user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fcm_id` text DEFAULT NULL,
  `device_id` varchar(250) NOT NULL,
  `expired_at` date DEFAULT NULL,
  `device_type` tinyint(4) NOT NULL COMMENT '1-Android, 2-ios',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_logins`
--

INSERT INTO `user_logins` (`id`, `user_id`, `fcm_id`, `device_id`, `expired_at`, `device_type`, `created_at`) VALUES
(5, 16, 'string', 'string', NULL, 1, '2020-08-13 13:29:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_us_ibfk_1` (`user_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `project_images`
--
ALTER TABLE `project_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_images_ibfk_1` (`project_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_logins`
--
ALTER TABLE `user_logins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `device_id` (`device_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `project_images`
--
ALTER TABLE `project_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_logins`
--
ALTER TABLE `user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD CONSTRAINT `contact_us_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_images`
--
ALTER TABLE `project_images`
  ADD CONSTRAINT `project_images_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_logins`
--
ALTER TABLE `user_logins`
  ADD CONSTRAINT `user_logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
