var express = require('express');
const { checkSchema } = require('express-validator');
var router = express.Router();
const CustomerController = require("./customer.controller");
let auth = require("../../middleware/auth")


// Vehicle management
router.post('/vehicle', auth.checkToken, CustomerController.addVehicle);
router.put('/vehicle', auth.checkToken, CustomerController.editVehicle);
router.delete('/vehicle', auth.checkToken, CustomerController.deleteVehicle);
router.get('/vehicles', auth.checkToken, CustomerController.getVehicles);

//Address management
router.post('/address', auth.checkToken, CustomerController.addAddress);
router.put('/address', auth.checkToken, CustomerController.editAddress);
router.delete('/address', auth.checkToken, CustomerController.deleteAddress);
router.get('/addresses', auth.checkToken, CustomerController.getAddresses);

//vehicle management
router.get('/vehicle-make',CustomerController.getVehicleMake)
router.get('/vehicle-model',CustomerController.getVehicleModel)

//Request Address management
router.post('/address/request', auth.checkToken, CustomerController.addAddressRequest);

//Add to cart
router.post("/cart",auth.checkToken,CustomerController.addtoCart);
router.put("/cart",auth.checkToken,CustomerController.updateCart);
router.delete('/cart',auth.checkToken,CustomerController.emptyCart);

//Get Services and Orders
router.get('/service/listing', auth.checkToken, CustomerController.getServices);
router.post('/order',auth.checkToken,CustomerController.addOrder);
router.get('/order',auth.checkToken,CustomerController.getOrder);
router.get('/orders',auth.checkToken,CustomerController.getOrders);
router.post('/order/cancel',auth.checkToken,CustomerController.cancelOrder);

//Coupon 
router.post('/applycoupon',auth.checkToken,CustomerController.applyCoupon);

//Checkout Order
router.post('/payment',auth.checkToken,CustomerController.checkoutOrder);

//Like Services
router.post('/like',auth.checkToken,CustomerController.likeService);

//Transaction management
router.get('/transactions',auth.checkToken,CustomerController.getTransactions);

//Give Rating
router.post('/review', auth.checkToken, CustomerController.addReview);

//Recharge
router.post('/recharge', auth.checkToken, CustomerController.RechargeWallet);


module.exports = router;