const logger = require("../../config/logger"); // importiing winston logger module
const CustomerService = require("./customer.service");
class CustomerController {
    static async getVehicles(req, res, next) {
        try {
            const { id } = req.user;
            const { keyword, limit, page } = req.query;
            const response = await CustomerService.getVehicles(id, keyword, limit, page);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getAddresses(req, res, next) {
        try {
            const { id } = req.user;
            const { keyword, limit, page } = req.query;
            const response = await CustomerService.getAddresses(id, keyword, limit, page);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getTransactions(req, res, next) {
        try {
            const { id } = req.user;
            const { limit, page } = req.query;
            const response = await CustomerService.getTransactions(id, keyword, limit, page);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getServices(req, res, next) {
        try {
            const { id } = req.user;
            const { area_id, arrival_date, vehicle_type, time_slot, filter, sort } = req.query;
            const response = await CustomerService.getServices(id, area_id, arrival_date, vehicle_type, time_slot, filter, sort);

            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async addOrder(req, res, next) {
        try {
            const { id } = req.user;
            const { services, area_id, address_id, vehicle_id, arrival_date, time_slot, total_amount } = req.body;
            const response = await CustomerService.addOrder(id, services, area_id, address_id, vehicle_id, arrival_date, time_slot, total_amount);

            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async checkoutOrder(req, res, next) {
        try {
            const { id } = req.user;
            const { id: order_id, amount, tracking_id, payment_method, coupon_code, transaction_id, currency, credit, payment_type, is_successful } = req.body;
            const response = await CustomerService.checkoutOrder(id, order_id, amount, tracking_id, payment_method, coupon_code, transaction_id, currency, credit, payment_type, is_successful);

            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }


    static async applyCoupon(req, res, next) {
        try {
            const { id } = req.user;
            const { amount, order_id, coupon } = req.body;
            const response = await CustomerService.applyCoupon(id, amount, order_id, coupon);

            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async RechargeWallet(req, res, next) {
        try {
            const { id } = req.user;
            const { amount } = req.body;
            const response = await CustomerService.RechargeWallet(id, amount);

            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getOrder(req, res, next) {
        try {
            const { id } = req.user;
            const { id: order_id } = req.query;
            const order = {}
            order["order"] = await CustomerService.getOrder(id, order_id);
            order['services'] = await CustomerService.getService(order_id);
        

            return res.status(200).json(order);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getOrders(req, res, next) {
        try {
            const { id } = req.user;
            const { limit, page } = req.query;
            const response = await CustomerService.getOrders(id, limit, page);

            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async cancelOrder(req, res, next) {
        try {
            const { id } = req.user;
            const { id: order_id } = req.body;
            const response = await CustomerService.cancelOrder(id, order_id);

            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async addVehicle(req, res, next) {
        try {
            const { id } = req.user;
            const { type, make, model, year, plate_no, image, color, description, is_default } = req.body;
            const response = await CustomerService.addVehicle(id, type, make, model, year, plate_no, image, color, description, is_default);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async addAddress(req, res, next) {
        try {
            const { id } = req.user;
            const { type, emirate_id, area_id, stree_name, address, extra_direction, lat, lng, is_default, house_no } = req.body;
            const response = await CustomerService.addAddress(id, type, emirate_id, area_id, stree_name, address, extra_direction, lat, lng, is_default, house_no);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getVehicleMake(req, res, next) {
        try {
            const { type } = req.query;
            const response = await CustomerService.getVehicleMake(type);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getVehicleModel(req, res, next) {
        try {
            const { year, make } = req.query;
            const response = await CustomerService.getVehicleModel(year, make);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async likeService(req, res, next) {
        try {
            const { id: logged_id } = req.user;
            const { id } = req.query;
            const response = await CustomerService.likeService(logged_id, id);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }


    static async addtoCart(req, res, next) {
        try {
            const { id } = req.user;
            const { products, service_id, category_id, company_id } = req.body;
            const response = await CustomerService.addtoCart(id, company_id, category_id, service_id, products);
            if (response == "bad_request") {
                return res.status(400).json({ message: "You cannot add other service items" });
            }
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async updateCart(req, res, next) {
        try {
            const { id } = req.user;
            const { extra_service_id, service_id, quantity } = req.body;
            const response = await CustomerService.updateCart(id, extra_service_id, service_id, quantity);
            if (response.affectedRows > 0) {
                return res.status(400).json({ message: "Cart Updated" });
            }
            // return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async emptyCart(req, res, next) {
        try {
            const { id } = req.user;
            const response = await CustomerService.emptyCart(id);
            if (response.affectedRows > 0) {
                return res.status(200).json({ message: "Cart Emptied successfully" });
            } else {
                return res.status(400).json({ message: "Error in empty" });
            }

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async addAddressRequest(req, res, next) {
        try {
            const { id } = req.user;
            const { emirate_id, area } = req.body;
            const response = await CustomerService.addAddressRequest(id, emirate_id, area);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async addReview(req, res, next) {
        try {
            const { id } = req.user;
            const { order_id, company_id, rating, review } = req.body;
            const response = await CustomerService.addReview(id, order_id, company_id, rating, review);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async editAddress(req, res, next) {
        try {
            const { id: logged_id } = req.user;
            const { id, type, emirate_id, area_id, stree_name, address, extra_direction, lat, lng, is_default, house_no } = req.body;
            const response = await CustomerService.editAddress(logged_id, id, type, emirate_id, area_id, stree_name, address, extra_direction, lat, lng, is_default, house_no);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async editVehicle(req, res, next) {
        try {
            const { id: logged_id } = req.user;
            const { id, type, make, model, year, plate_no, image, color, description, is_default } = req.body;
            const response = await CustomerService.editVehicle(logged_id, id, type, make, model, year, plate_no, image, color, description, is_default);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async deleteVehicle(req, res, next) {
        try {
            const { id } = req.body;
            const response = await CustomerService.deleteVehicle(id);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async deleteAddress(req, res, next) {
        try {
            const { id } = req.body;
            const response = await CustomerService.deleteAddress(id);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

}

module.exports = CustomerController;
