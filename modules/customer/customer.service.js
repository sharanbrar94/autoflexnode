const { sign } = require('jsonwebtoken');
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const Common = require('../../middleware/common')
const logger = require('../../config/logger');
const axios = require("axios");
var moment = require('moment');
class CustomerService {
    static async getVehicles(id, keyword, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select v.*,type.name,type.description, type.image from user_vehicles as v left join users as u on v.user_id= u.id left join vehicle_types as type on v.vehicle_type= type.id where u.id=? LIMIT ${limit} OFFSET ${offset}`
        let vehicles = await connection.query(sqlQuery, [id])

        let sqlQueryCount = `select v.*,type.name,type.description, type.image from user_vehicles as v left join users as u on v.user_id= u.id left join vehicle_types as type on v.vehicle_type= type.id where u.id=?`
        let vehicleCount = await connection.query(sqlQueryCount, [id])
        return { count: vehicleCount.length, vehicles }
    }

    static async getAddresses(id, keyword, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select v.* from user_addresses as v left join users as u on v.user_id= u.id where u.id=? LIMIT ${limit} OFFSET ${offset}`
        let addresses = await connection.query(sqlQuery, [id])

        let sqlQueryCount = `select v.* from user_addresses as v left join users as u on v.user_id= u.id where u.id=?`
        let addressCount = await connection.query(sqlQueryCount, [id])
        return { count: addressCount.length, addresses }
    }

    static async gettransactions(id, keyword, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select * from transactions where user_id=? LIMIT ${limit} OFFSET ${offset}`
        let transactions = await connection.query(sqlQuery, [id])

        let sqlQueryCount = `select * from transactions where user_id=?`
        let transactionCount = await connection.query(sqlQueryCount, [id])
        return { count: transactionCount.length, transactions }
    }

    static async addVehicle(id, type, make, model, year, plate_no, image, color, description, is_default) {
        var vehicles = await connection.query("select * from user_vehicles where user_id=?", [id])
        if (vehicles.lenght == 0) {
            is_default = 1
        } else {
            if (is_default == 1) {
                await connection.query('update user_vehicles set is_default= 0 where user_id = ?', [id])
            } else {
                is_default = 0
            }
        }
        if (image) {
            await connection.query('insert into files(id,storage_type,environment,is_default_asset) values (?,?,?,?)', [image, 5, 4, 0])
        }
        var response = await connection.query('insert into user_vehicles(user_id,vehicle_type,make, model, year, plate_no, image, color,description,is_default) values (?,?,?,?,?,?,?,?,?,?)', [id, type, make, model, year, plate_no, image, color, description, is_default])
        return { id: response.insertId, message: "Vehicle Added" }
    }

    static async addAddress(id, type, emirate_id, area_id, stree_name, address, extra_direction, lat, lng, is_default, house_no) {
        var addresses = await connection.query("select * from user_addresses where user_id=?", [id])
        if (addresses.length == 0) {
            is_default = 1
        } else {
            console.log(addresses)
            if (is_default == 1) {
                await connection.query('update user_addresses set is_default= 0 where user_id = ?', [id])
            } else {
                is_default = 0
            }
        }
        var response = await connection.query('insert into user_addresses(user_id,type,emirate_id,area_id,street_name, address,  extra_direction,lat, lng,is_default,house_no) values (?,?,?,?,?,?,?,?,?,?,?)', [id, type, emirate_id, area_id, stree_name, address, extra_direction, lat, lng, is_default, house_no])
        return { id: response.insertId, message: "Address Added" }
    }

    static async getVehicleMake(type) {
        if (type == 1) {
            var vehicle = "car"
        } else {
            var vehicle = "motorcycle"
        }
        return await axios.get(`https://vpic.nhtsa.dot.gov/api/vehicles/GetMakesForVehicleType/${vehicle}?format=json`).then(function (response) {
            console.log(response.data.Results)
            var result = response.data.Results
            var data = []
            for (var i in result) {
                data.push(result[i].MakeName)
            }
            return data;
        });


    }

    static async getVehicleModel(year, make) {
        return await axios.get(`https://vpic.nhtsa.dot.gov/api/vehicles/GetModelsForMake/${make}?format=json`).then(function (response) {
            console.log(response.data.Results)
            var result = response.data.Results
            var data = []
            for (var i in result) {
                data.push(result[i].Model_Name)
            }
            return data;
        });

    }

    static async addtoCart(id, company_id, category_id, service_id, products) {

        console.log(products)
        var company = await connection.query('select * from user_carts where company_id!=? and ? NOT IN (select DISTINCT category_id from user_carts where company_id=? and user_id=?) and user_id=?', [company_id, category_id, company_id, id, id])
        console.log(company)
        if (company.length == 0) {
            for (var i in products) {
                console.log(products[i].service_extra_id)
                var product = await connection.query('select * from user_carts where service_id=? and service_extra_id=? and user_id=? and category_id=?', [service_id, products[i].service_extra_id, id, category_id])
                if (product.length > 0) {
                    await connection.query('update user_carts set  quantity = quantity + ?, price = price + ?  where service_extra_id=? and service_id=? and user_id=?', [products[i].quantity, products[i].price, products[i].service_extra_id, service_id, id])
                } else {
                    var data = []
                    data.push([id, company_id, category_id, service_id, products[i].service_extra_id, products[i].quantity, products[i].price])
                    console.log(data)
                    await connection.query('insert into user_carts(user_id,company_id,category_id,service_id,service_extra_id,quantity,price) values ?', [data])

                }
            }


        } else {
            return "bad_request"
        }
        return { message: "Products Added" }
    }

    static async addAddressRequest(id, emirate_id, area) {
        var response = await connection.query('insert into address_requests(user_id,emirate_id,area) values (?,?,?)', [id, emirate_id, area])
        return { id: response.insertId, message: "Address request added" }
    }

    static async addReview(id, order_id, company_id, rating, review) {
        var response = await connection.query('insert into reviews(user_id,order_id, company_id,rating,review) values (?,?,?,?,?)', [id, order_id, company_id, rating, review])
        return { id: response.insertId, message: "Reviewed successfully" }
    }

    static async emptyCart(id) {
        return await connection.query('delete from user_cart where user_id=?', [id])
    }

    static async updateCart(id, extra_service_id, service_id, quantity) {
        if (service_id && extra_service_id && quantity > 0) {
            return await connection.query('update user_carts set quantity = ? where user_id=? and service_id=? and extra_service_id=?', [quantity, id, service_id, extra_service_id])
        }
        else if (service_id && extra_service_id && quantity == 0) {
            return await connection.query('delete from user_carts where user_id=? and service_id=? and extra_service_id=?', [id, service_id, extra_service_id])
        }
        else if (service_id) {
            return await connection.query('delete from user_carts where user_id=? and service_id=? ', [id, service_id])
        }

    }

    static async editVehicle(logged_id, id, type, make, model, year, plate_no, image, color, description, is_default) {
        let sqlQuery = 'update user_vehicles set '
        let params = []
        let sql = []
        if (type != undefined) {
            params.push(type)
            sql.push('vehicel_type=?')
        }
        if (make != undefined) {
            params.push(make)
            sql.push('make=?')
        }
        if (model != undefined) {
            params.push(model)
            sql.push('model=?')
        }
        if (year != undefined) {
            params.push(year)
            sql.push('year=?')
        }
        if (plate_no != undefined) {
            params.push(plate_no)
            sql.push('plate_no=?')
        }

        if (color != undefined) {
            params.push(color)
            sql.push('color=?')
        }
        if (description != undefined) {
            params.push(description)
            sql.push('description=?')
        }
        if (is_default != undefined) {
            params.push(is_default)
            sql.push('is_default=?')
            if (is_default == 1) {
                await connection.query('update user_vehicles set is_default= 0 where id != ? and user_id = ?', [id, logged_id])
            }
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        await connection.query(sqlQuery, params)
        return { message: "Vehicle udpated successfully" }
    }

    static async editAddress(logged_id, id, type, emirate_id, area_id, street_name, address, extra_direction, lat, lng, is_default, house_no) {
        console.log("type", type)
        let sqlQuery = 'update user_addresses set '
        let params = []
        let sql = []
        if (type != undefined) {
            params.push(type)
            sql.push('type=?')
        }
        if (emirate_id != undefined) {
            params.push(emirate_id)
            sql.push('emirate_id=?')
        }
        if (area_id != undefined) {
            params.push(area_id)
            sql.push('area_id=?')
        }
        if (street_name != undefined) {
            params.push(street_name)
            sql.push('street_name=?')
        }
        if (address != undefined) {
            params.push(address)
            sql.push('address=?')
        }
        if (extra_direction != undefined) {
            params.push(extra_direction)
            sql.push('extra_direction=?')
        }
        if (lat != undefined) {
            params.push(lat)
            sql.push('lat=?')
        }
        if (lng != undefined) {
            params.push(lng)
            sql.push('lng=?')
        }
        if (house_no != undefined) {
            params.push(house_no)
            sql.push('house_no=?')
        }
        if (is_default != undefined) {
            params.push(is_default)
            sql.push('is_default=?')
            if (is_default == 1) {
                await connection.query('update user_addresses set is_default= 0 where id != ? and user_id = ?', [id, logged_id])
            }
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=? and user_id=?'
        params.push(id)
        params.push(logged_id)
        console.log(sqlQuery);
        console.log(params);
        await connection.query(sqlQuery, params)
        return { message: "Address udpated successfully" }

    }

    static async deleteVehicle(id) {
        var response = await connection.query('delete from user_vehicles where id = ?', [id])
        if (response.affectedRows > 0) {
            return { message: "Vehicle deleted successfully" }
        } else {
            return { message: "Vehicle does not exist" }
        }
    }

    static async deleteAddress(id) {
        var response = await connection.query('delete from user_addresses where id = ?', [id])
        if (response.affectedRows > 0) {
            return { message: "Address deleted successfully" }
        } else {
            return { message: "Address does not exist" }
        }
    }

    static async likeService(logged_id, id) {
        await connection.query('insert into likes (company_id,user_id) values (?,?)', [id, logged_id])
        return { message: "Company liked" }
    }

    static async applyCoupon(id, amount, order_id, coupon) {
        let final_amount = 0;
        var order = await connection.query('select s.* from orders as o left join order_items as ot on ot.order_id=o.id left join services as s on s.id=ot.service_id where o.id =? and o.user_id=?', [order_id, id])
        if (order.length > 0) {
            let data = [];
            for (var i in order) {
                data.push([order[i].category_id, order[i].sub_category_id])
            }
            var count = 0
            for (var i in data) {
                var coupons = await connection.query('select * from coupons as c left join service_coupons as sc on sc.coupon_id=c.id where c.name like ? and sc.category_id=? and sc.sub_category_id=?', [coupon, data[i][0], data[i][1]])
                if (coupons.length > 0) {
                    count++;
                }
            }
            console.log("count", count)
            if (count > 0) {
                if (coupons[0].min_amount < amount) {
                    if (coupons[0].type == 0) {
                        final_amount = amount - coupons[0].amount;
                    } else if (coupons[0].type == 1) {
                        var a = (coupons[0].percent / 100 * amount)
                        if (a < coupons[0].amount) {
                            final_amount = amount - a;
                        } else if (a > coupons[0].amount) {
                            final_amount = amount - coupons[0].amount;
                        }
                    }
                    return { id: order_id, final_amount, message: 'Coupon Applied' };
                }
                else {
                    throw ("Coupon not applicable.")
                }
            } else {
                throw ('Coupon does not exist');
            }
        }

    }

    static async addOrder(id, services, area_id, address_id, vehicle_id, arrival_date, time_slot, total_amount) {
        var service_arr = []
        const date = new Date(arrival_date)
        const day = date.getDay();
        console.log(day)
        for (var i in services) {
            var query = `SELECT sbh.worker,sbh.worker-(select count(*) as count from orders as o left join order_items as ot on ot.order_id=o.id where ot.service_id=sbh.service_id and o.arrival_time=ds.time_slot_id and o.arrival_date=? and o.status=0)as vacancy  FROM service_business_hours as sbh left join day_slots as ds on ds.service_day_id=sbh.id WHERE sbh.service_id=? and sbh.day = ? and ds.time_slot_id=?`
            console.log()

            var vacancy = await connection.query(query, [arrival_date, services[i].id, day, time_slot])
            console.log("vacancy", vacancy)
            if (vacancy.length > 0) {
                if (vacancy[0].vacancy == 0) {
                    throw ("Order Cannot be placed.")
                }
            }
            service_arr.push(services[i].id)
        }
        console.log(service_arr)
        // var slot_vacant = await connection.query('select * from orders left join where arrival_date = ? and time_slot= ? and  ', [id, arrival_date, service_arr])
        // console.log(order_already)
        var order_already = await connection.query('select * from orders as o left join order_items as o_t on o_t.order_id=o.id where o.user_id = ? and o.arrival_date = ? and o_t.service_id IN (?)', [id, arrival_date, service_arr])
        console.log(order_already)
        if (order_already.length > 0) {
            throw ("Order Already Exist")
        } else {
            var response = await connection.query('insert into orders(user_id,area_id,address_id,vehicle_id,arrival_date,arrival_time,amount) values (?,?,?,?,?,?,?)', [id, area_id, address_id, vehicle_id, arrival_date, time_slot, total_amount])

            var data = []
            for (var i in services) {
                if (services[i].extras) {
                    for (var j in services[i].extras) {
                        data.push([response.insertId, services[i].id, services[i].price, services[i].extras[j].extra_service_id, services[i].extras[j].quantity, services[i].extras[j].price])
                    }
                } else {
                    data.push([response.insertId, services[i].id, services[i].price, null, null, null])
                }
            }
            console.log(data)
            var query = 'insert into order_items(order_id,service_id,service_price,extra_service_id,quantity,price) values ?'
            await connection.query(query, [data])
        }
        return { id: response.insertId, message: "Order placed" }

    }

    static async getOrder(id, order_id) {
        var order = await connection.query('select o.*,ot.*,if(u1.last_name is NOT null ,CONCAT(u1.first_name," ",u1.last_name),u1.first_name) as worker_name,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as user_name from orders as o left join order_items as ot on ot.order_id=o.id left join users as u on u.id=o.user_id left join users as u1 on u1.id=o.worker_id left join user_vehicles as u_v on u_v.id=o.vehicle_id left join user_addresses as u_a on u_a.id=o.address_id left join time_slots as t_s on t_s.id=o.arrival_time where o.id = ? and o.user_id=? and o.is_created=1', [order_id, id])
        if (order.length > 0) {
            return order
        } else {
            throw ('No Order exist')
        }
    }

    static async checkoutOrder(id, order_id, amount, tracking_id, payment_method, coupon_code, transaction_id, currency, credits, payment_type, is_successful) {
        var transaction = await connection.query('select * from transactions as t left join order_transactions as ot on ot.transaction_id=t.id left join orders as o on o.id=ot.order_id where o.id=? and t.is_successful = 1 ', [id])
        console.log(transaction)
        if (transaction.length > 0) {
            throw ("Order already paid")
        }
        var order = await connection.query('select * from orders where id=?', [order_id])
        var amount = order[0].amount;
        if (credits != 0) {
            var credit = await connection.query('select if(sum(credit) is null ,0,sum(credit)) as total from user_wallets where user_id=?', [id])
            var total_credit = credit[0].total
            console.log("in credit if")
            if (total_credit > amount) {
                console.log("if")
                var method = 3;   // credits
                var new_credits = total_credit - amount;
                var new_amount = 0;
                var paid_credits = amount;
            }
            else {
                console.log("else")
                if (payment_method == 1) {
                    var method = 4;  // credits and cash
                }
                if (payment_method == 2) {
                    var method = 5;  // card and credits
                }
                var new_credits = 0;
                var new_amount = amount - credits;
                var paid_credits = credits;
            }
        }
        else {
            console.log("in credit else")
            var method = payment_method;
            var new_amount = amount;
            var paid_credits = null;
            var new_credits = null;
        }

        var transactionId = await Common.generateRandomString(6);

        var transaction = await connection.query('insert into transactions (user_id, amount, tracking_id, payment_method, coupon_applied, transaction_id, currency, credits, payment_type,payable_amount,is_successful) values (?,?,?,?,?,?,?,?,?,?,?)', [id, amount, tracking_id, method, coupon_code, transactionId, currency, paid_credits, 1, new_amount, is_successful])

        await connection.query("insert into order_transactions(order_id,transaction_id) values (?,?)", [order_id, transaction.insertId])

        if (is_successful == 1) {
            await connection.query("update orders set is_created = 1 where id=?", [order_id])
        }

        var wallet = await connection.query('insert into user_wallets (transaction_id,user_id, credit) values (?,?,?)', [transaction.insertId, id, -paid_credits])

        return { id: order_id, message: 'Payment Done' };


    }

    static async RechargeWallet(id, amount) {
        var credit = 0
        var transaction = await connection.query('insert into transactions(user_id, order_id,transaction_id, transaction_type,card_last_four,invoice_id, currency,description,amount) values (?,?,?,?,?,?,?,?,?)', [id, null, "Transaction", 4, 1234, "INVOICE", "INR", "Wallet Recharge", amount])
        if (amount > 51) {
            credit = amount
        } else {
            credit = amount + 4
        }
        var wallet = await connection.query('insert into user_wallets (transaction_id,user_id, credit) values (?,?,?)', [transaction.insertId, id, credit])
        return wallet
    }

    static async getService(order_id) {
        var services = await connection.query('select * from order_items where order_id=?', [order_id])
        var service_ids = [];
        for (var i in services) {
            service_ids.push(services[i].service_id)
        }
        let service_id = [...new Set(service_ids)];
        console.log("services id", service_id)
        var service = await connection.query('select s.id,c.name as category_name,sub.name as sub_category_name,comp.name as company_name,s.price,s.discount,s.service_time,s.reach_time,s.description,s.is_powered,s.is_blocked,s.created_at from services as s left join categories as c on c.id=s.category_id left join sub_categories as sub on sub.id =s.sub_category_id left join companies as comp on comp.id=s.company_id where s.id IN (?)', [service_id])
        console.log(service)
        for (var i in service) {
            var extras = await connection.query('select * from order_items where order_id =? and service_id=?', [order_id, service[i].id])
            var extra_ids = [];
            for (var i in extras) {
                extra_ids.push(extras[i].extra_service_id)
            }
            console.log("extra ids", extra_ids)
            if (extra_ids.length > 0) {
                var extra_services = await connection.query('select * from service_extras where id IN (?)', [extra_ids])
                service[i].extra_services = extra_services
            } else {
                service[i].extra_services = []
            }
            var service_highlights = await connection.query('select * from service_highlights where service_id =?', [service[i].id])
            service[i].highlights = service_highlights
        }
        return service
    }

    static async getOrders(id, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
      var limit = parseInt(lmt)
        var sqlQuery = `select o.*,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as user_name from orders as o left join order_items as ot on ot.order_id=o.id left join users as u on u.id=o.user_id where o.user_id=? and o.is_created=1`
        sqlQuery += ` order by u.id desc LIMIT ${limit} OFFSET ${offset}`
        var order = await connection.query(sqlQuery, [id])
        var sqlQueryCount = `select o.*,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as user_name from orders as o left join order_items as ot on ot.order_id=o.id left join users as u on u.id=o.user_id where o.user_id=? and o.is_created=1`

        var order_count = await connection.query(sqlQueryCount, [id])
        return { count: order_count.length, order }
    }

    static async convertUTCDateToLocalDate(date) {
        var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();

        newDate.setHours(hours - offset);

        return newDate;
    }

    static async cancelOrder(id, order_id) {
        var order = await connection.query('select * from orders where id=? and user_id=? and is_created =1', [order_id, id])
        console.log(order)
        var thendate = order[0].arrival_date;
        var time = ''
        if (order[0].arrival_time.toString().length == 1) {
            time = '0' + order[0].arrival_time
        } else {
            time = order[0].arrival_time
        }
        var now = moment().format("DD/MM/YYYY HH:mm:ss", true);
        var then_date = moment(thendate).format("DD/MM/YYYY", true).toString();
        console.log(now)
        var then = then_date + ' ' + time + ':00:00'
        console.log(then_date)

        var ms = moment(then, "DD/MM/YYYY HH:mm:ss").diff(moment(now, "DD/MM/YYYY HH:mm:ss"));
        var d = moment.duration(ms);

        console.log(d.days() + ':' + d.hours() + ':' + d.minutes() + ':' + d.seconds());

        if (d.hours() < 24) {
            await connection.query('update orders set status = 4 where id=?', [order_id])
            return { message: "Order Cancelled." }
        } else {
            var total_amount = order[0].amount;
            var paid_amount = await connection.query('select if(sum(payable_amount) is null , 0 ,sum(payable_amount))as amount from order_transactions as ot left join transactions as t on ot.transaction_id=t.id  where ot.order_id=? and t.is_successful = 1', [order_id])
            console.log(paid_amount.amount)
            var refund_amount = total_amount - paid_amount
            if (refund_amount > 0) {
                console.log(refund_amount)
            }
            return order
        }


    }

    static async getServices(id, area_id, arrival_date, vehicle_type, time_slot, filter, sort) {
        // var cart_query= 'select * from services left join service_locations as s_l on s_l.service=id=services.id select area_id from user_carts where user_id=?'
        // var response =  await connection.query(cart_query, [id])

        // var change_query = 'select * from services left join service_locations as s_l on s_l.service=id=services.id'
        const date = new Date(arrival_date)
        const day = date.getDay();
        console.log(day)

        var query = `select * from (select services.*,c.name as category_name,sc.name as sub_category_name, (SELECT sbh.worker-(select count(*) as count from orders as o left join order_items as ot on ot.order_id=o.id where ot.service_id=sbh.service_id and o.arrival_time=ds.time_slot_id and o.arrival_date Like '${arrival_date}' and o.status=0)as vacancy  FROM service_business_hours as sbh left join day_slots as ds on ds.service_day_id=sbh.id WHERE sbh.service_id=services.id and sbh.day = ${day} and ds.time_slot_id=${time_slot}) as vacancy,(select Avg(rating) from reviews where reviews.company_id=services.company_id) as rating,(select count(*) as count from likes where company_id = services.company_id and user_id = ?) as is_liked,(select count(*) as count from likes where company_id = services.company_id) as likes from services left join service_business_hours as hour on hour.service_id=services.id left join day_slots as dsd on dsd.service_day_id=hour.id left join categories as c on c.id=services.category_id left join sub_categories as sc on sc.id=services.sub_category_id where company_id in (select id from companies where id not in (SELECT company_id FROM block_days where date= cast(now() as date))) and services.id IN ( select DISTINCT service_id from service_locations where area_id = ? )and services.id IN ( select DISTINCT service_id from service_vehicles where vehicle_type_id = ? ) and hour.day=? and dsd.time_slot_id=? )as data where vacancy >0 `
        if (filter) {
            query += `  and services.sub_category_id IN (${filter})`
        }
        if (sort) {
            if (sort == 1) {
                query += `  order by rating desc`
            }
            if (sort == 2) {
                query += `  order by likes desc`
            }
        }
        return await connection.query(query, [id, area_id, vehicle_type, day, time_slot])
    }

}

module.exports = CustomerService;