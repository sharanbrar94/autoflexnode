const { sign } = require('jsonwebtoken');
const nodemailer = require('nodemailer')
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const logger = require('../../config/logger');
const childProcess = require('child_process')
const fs = require('fs')
const moment = require('moment');
let Common = require("../../middleware/common");
const { profile } = require('console');
const { serialize } = require('v8');
const axios = require("axios");
const { xor } = require('lodash');
class AdminService {
    static async login(email, password) {
        let user = await this.getAdminDetail('email', email);
        console.log(user.length)
        if (user.length != 0) {
            if (user[0] && user[0].is_blocked == 0 && user[0].is_deactivated == 0) {
                let passwordResult = compareSync(password, user[0].password);
                if (!passwordResult) {
                    throw ("Please enter correct password");
                }
            } else {
                throw ("You are blocked or deactivated by the admin.");
            }
        } else {
            throw ("Please enter registered email id.")
        }
        delete user[0].password;
        let auth_token = await this.generateAdminToken(user);
        return ({ 'message': "Login successfully", 'token': auth_token, 'user': user[0] });
    }

    static async updateProfile(logged_id, first_name, last_name, profile_image) {
        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(logged_id)
        console.log(logged_id)
        await connection.query(sqlQuery, params)
        return { message: "Profile updated successfully" }
    }

    static async changePassword(new_password, old_password, logged_id) {
        const user = await this.getAdminDetail('id', logged_id)
        let response = compareSync(old_password, user[0].password);
        if (!response) {
            throw ('Please enter correct old password.');
        } else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            let sqlQuery = `update users set password=? where id=?`
            let params = [password, logged_id]
            const response = await connection.query(sqlQuery, params);
            if (response) {
                return ({ message: "Password changed" });
            }
        }
    }

    static async changeUserPassword(new_password, old_password, logged_id, type) {
        const user = await this.getUserData('id', logged_id, type)
        let response = compareSync(old_password, user[0].password);
        if (!response) {
            throw ('Please enter correct old password.');
        } else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            let sqlQuery = `update users set password=? where id=?`
            let params = [password, logged_id]
            const response = await connection.query(sqlQuery, params);
            if (response) {
                return ({ message: "Password changed" });
            }
        }
    }

    static async workerChangePassword(worker_id, password) {
        const salt = genSaltSync(10);
        var hash_password = hashSync(password, salt);
        let sqlQuery = `update users set password=? where id=? and user_type=3`
        let params = [hash_password, worker_id]
        const response = await connection.query(sqlQuery, params);
        // await Common.sendMessage(number, body);
        if (response.affectedRows > 0) {
            return { message: "Password changed" };
        } else {
            return { message: "Error in changing password" }
        }

    }

    static async getProfile(id) {
        let sqlQuery = `select * from users where id=${id}`
        return await connection.query(sqlQuery)

    }
    static async getUserByEmail(email) {
        let sqlQuery = `select * from users where email=? `
        return await connection.query(sqlQuery, [email])

    }
    static async generateToken(user) {
        let token = await sign({ user: user[0] }, 'secretkey', { expiresIn: "2Days" });
        return token

    }

    static async generateAdminToken(user) {
        let token = await sign({ user: user[0] }, 'secretAdminkey', { expiresIn: "2Days" });
        return token

    }

    static async getUserDetail(value) {
        let sqlQuery = `select u.*,c.name as company_name,c.trade_license,c.passport,c.emirate_id from users as u left join companies as c on c.user_id=u.id where u.id=?`
        const user = await connection.query(sqlQuery, [value]);
        delete user[0].password;
        return user[0]
    }

    static async getCategoryDetail(id) {
        let sqlQuery = `select c.id,c.name as category_name from categories as c where c.id=?`
        const category = await connection.query(sqlQuery, [id]);
        return category
    }

    static async getEmirateDetail(id) {
        let sqlQuery = `select e.id,e.name as emirate_name, area.name as area_name from emirates as e left join areas as area on area.emirate_id=e.id where e.id=?`
        const category = await connection.query(sqlQuery, [id]);
        return category
    }

    static async getAdminDetail(param, value) {
        let sqlQuery = `select * from users where ${param}=? and user_type != 1`
        const user = await connection.query(sqlQuery, [value]);
        return user;
    }

    static async getUserDetail(param, value) {
        let sqlQuery = `select * from users where ${param}=? and user_type = 1`
        const user = await connection.query(sqlQuery, [value]);
        return user;
    }

    static async getUserData(param, value, type) {
        let sqlQuery = `select * from users where ${param}=? and user_type = ?`
        const user = await connection.query(sqlQuery, [value, type]);
        return user;
    }

    //add worker
    static async addWorker(id, profile_image, first_name, last_name, password, country_code, phone_number, username) {
        let user = await this.getUserByUsername(username)
        if (user.length > 0) {
            throw ("Username already exist!");
        }
        const company = await this.getCompany(id)
        console.log(company)
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        const response = await connection.query('insert into users set first_name=?,last_name=?,username=?,password=?, country_code=?, phone_number=?,user_type=?, company_id=?, profile_image=?', [first_name, last_name, company[0].name + "." + username, password, country_code, phone_number, 3, id, profile_image]);
        console.log(response)
        if (response) {
            return ({ id: response.insertId, message: "Worker added" })
        }
    }

    //add category
    static async addCategory(name, image) {

        var response = await connection.query('insert into categories set name=?, image=?', [name, image]);

        if (response) {
            return ({ id: response.insertId, message: "Category added" })
        }
    }

    //add sub category
    static async addSubCategory(name, id) {
        var response = await connection.query('insert into sub_categories (name,category_id) values (?,?)', [name, id]);
        if (response) {
            return ({ id: response.insertId, message: "Sub Category added" })
        }
    }

    static async getCompany(id) {
        let sqlQuery = `select * from companies where id = ?`
        var response = await connection.query(sqlQuery, [id])
        console.log("response", response)
        return response
    }

    //add emirate
    static async addEmirate(name) {

        const response = await connection.query('insert into emirates set name=?', [name]);
        if (response) {
            return ({ id: response.insertId, message: "Emirate added" })
        }
    }

    static async getUserByUsername(username) {
        let sqlQuery = `select * from users where username=? `
        return await connection.query(sqlQuery, [username])
    }

    static async updateUserProfile(id, first_name, last_name, email, phone_number, country_code, profile_image, whatsapp_number, whatsapp_country_code, type, company_name, trade_license, emirate_id, passport, country_abbr) {
        console.log("type", type)
        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (email != undefined) {
            params.push(email)
            sql.push('email=?')
        }
        if (phone_number != undefined) {
            params.push(phone_number)
            sql.push('phone_number=?, is_phone_verified = 0, phone_verify_code = 111111')
        }
        if (country_code != undefined) {
            params.push(country_code)
            sql.push('country_code=?, is_phone_verified = 0, phone_verify_code = 111111')
        }

        if (whatsapp_number != undefined) {
            params.push(whatsapp_number)
            sql.push('whatsapp_number=?')
        }
        if (whatsapp_country_code != undefined) {
            params.push(whatsapp_country_code)
            sql.push('whatsapp_country_code=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        if (country_abbr != undefined) {
            params.push(country_abbr)
            sql.push('country_abbr=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        if (type == 2 && (trade_license || company_name || emirate_id || passport)) {
            let sqlQuery = 'update companies set '
            let params = []
            let sql = []
            if (trade_license != undefined) {
                params.push(trade_license)
                sql.push('trade_license=? , is_trade_license_verified = 0')
            }
            if (company_name != undefined) {
                params.push(company_name)
                sql.push('name=?')
            }
            if (passport != undefined) {
                params.push(passport)
                sql.push('passport=? , is_passport_verified = 0')
            }
            if (emirate_id != undefined) {
                params.push(emirate_id)
                sql.push('emirate_id=?, is_emirate_id_verified = 0')
            }
            sqlQuery += sql.toString()
            sqlQuery += ' where user_id=?'
            params.push(id)
            console.log(sqlQuery);
            console.log(params);
            await connection.query(sqlQuery, params)
        }
        return await connection.query(sqlQuery, params)

    }

    static async blockUser(id, unblock) {
        console.log(2)
        let sqlQuery = 'update users set users.is_blocked=? where users.id=? '
        return await connection.query(sqlQuery, [unblock, id])
    }

    static async blockService(id, is_blocked) {
        let sqlQuery = 'update services set is_blocked=? where id=? '
        await connection.query(sqlQuery, [is_blocked, id])
        return is_blocked == 0 ? "Service Unblocked" : "Service Blocked"

    }

    static async editWithdrawlRequest(id, status) {
        if (status == 1) {
            let sqlQuery = 'update withdraw_requests set status=2 where id=? '
            await connection.query(sqlQuery, [id])
        } else {
            let sqlQuery = 'update withdraw_requests set status=3 where id=? '
            await connection.query(sqlQuery, [id])
        }
        return status == 1 ? "Request Accepted" : "Request rejected"

    }

    static async getServices(id, keyword, page, limit, is_blocked) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = 'select s.*,cat.name as category_name ,sub_cat.name as sub_category_name, if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as owner_name, c.id as company_id,c.name as company_name from services as s left join companies as c on s.company_id=c.id left join users as u on u.id=c.user_id left join categories as cat on cat.id=s.category_id left join sub_categories as sub_cat on sub_cat.id= s.sub_category_id where s.category_id =? '
        if (keyword) {
            sqlQuery += ` and (c.name like  '%${keyword}%' or cat.name like  '%${keyword}%'   or sub_cat.name like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            sqlQuery += ` and s.is_blocked = ? `
        }

        sqlQuery += ` order by s.id desc LIMIT ${limit} OFFSET ${offset}`
        let services = await connection.query(sqlQuery, [id, is_blocked])

        let count_Query = 'select count(*) as count from services as s left join companies as c on s.company_id=c.id left join users as u on u.id=c.user_id left join categories as cat on cat.id=s.category_id left join sub_categories as sub_cat on sub_cat.id= s.sub_category_id where s.category_id=? '
        if (keyword) {
            count_Query += ` and (c.name like  '%${keyword}%' or cat.name like  '%${keyword}%'   or sub_cat.name like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            count_Query += ` and s.is_blocked = ? `
        }

        let count = await connection.query(count_Query, [id, is_blocked])
        return { service_count: count[0].count, services }
    }

    static async getWorkers(keyword, page, limit, is_blocked) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select * from users where users.user_type = ?`
        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'   or users.phone_number like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            sqlQuery += ` and users.is_blocked = ? `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`

        let user = await connection.query(sqlQuery, [3, is_blocked])

        let sqlQueryCount = `select * from users where users.user_type = ?`
        if (keyword) {
            sqlQuesqlQueryCountry += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'   or users.phone_number like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            sqlQueryCount += ` and users.is_blocked = ? `
        }


        let userCount = await connection.query(sqlQueryCount, [3, is_blocked])


        return { count: userCount.length, user }
    }

    static async getOwnerWorkers(id, keyword, page, limit, is_blocked) {
        var company = await this.getCompanyById(id)
        console.log("company", company)
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let sqlQuery = `select * from users where users.user_type = ? and users.company_id=? `
        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'   or users.phone_number like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            sqlQuery += ` and users.is_blocked = ? `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        let user = await connection.query(sqlQuery, [3, company[0].id, is_blocked])

        let main_array = [];
        user.map(item => {
            delete item.password;
            main_array.push(item);
        })

        return { count: user.length, user }
    }

    static async getOwnerServices(id) {
        var company = await this.getCompanyById(id)
        console.log("company", company)
        let services = await this.getServiceByCompanyId(company[0].id)
        return services.slice(0, 10)
    }

    static async getOwnerWithdrawls(id) {
        let withdrawls = await connection.query('select * from withdraw_requests where user_id=?', [id])
        return withdrawls
    }

    static async getAdminOrderDetail(order_id) {
        var services = await connection.query('select * from order_items where order_id=?', [order_id])
        var service_ids = [];
        for (var i in services) {
            service_ids.push(services[i].service_id)
        }
        let service_id = [...new Set(service_ids)];
        console.log(service_id)
        var service = await connection.query('select s.id,c.name as category_name,sub.name as sub_category_name,comp.name as company_name,s.price,s.discount,s.service_time,s.reach_time,s.description,s.is_powered,s.is_blocked,s.created_at from services as s left join categories as c on c.id=s.category_id left join sub_categories as sub on sub.id =s.sub_category_id left join companies as comp on comp.id=s.company_id where s.id IN (?)', [service_id])

        for (var i in service) {
            var extras = await connection.query('select * from order_items where order_id =? and service_id=?', [order_id, service[i].id])
            var extra_ids = [];
            for (var i in extras) {
                extra_ids.push(extras[i].extra_service_id)
            }
            console.log("extra ids", extra_ids)
            if (extra_ids.length > 0) {
                var extra_services = await connection.query('select ot.service_price,ot.quantity,ot.price,se.name,se.description,se.is_multiple from service_extras as se left join order_items as ot on ot.extra_service_id = se.id where se.id IN (?) and ot.order_id=? and ot.service_id=?', [extra_ids, order_id, service[i].id])
                service[i].extra_services = extra_services
            } else {
                service[i].extra_services = []
            }

        }
        return service
    };

    static async getOrderTransactions(order_id) {
        return await connection.query('select * from order_transactions as ot left join transactions as t on t.id=ot.transaction_id where ot.order_id=? and is_successful=1', [order_id])

    }

    static async getAddressRequests(keyword, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select ar.*,e.name as emirate_name,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as customer_name,u.profile_image as customer_image from address_requests as ar left join users as u on ar.user_id=u.id left join emirates as e on e.id=ar.emirate_id where 1 `
        if (keyword) {
            sqlQuery += ` and (ar.area like  '%${keyword}%' ) `
        }

        sqlQuery += ` order by ar.id desc LIMIT ${limit} OFFSET ${offset}`
        let requests = await connection.query(sqlQuery)

        let sqlQueryCount = `select ar.*,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as customer_name from address_requests as ar left join users as u on ar.user_id=u.id where 1 `
        if (keyword) {
            sqlQueryCount += ` and (ar.area like  '%${keyword}%' ) `
        }

        let request_count = await connection.query(sqlQueryCount)

        return { count: request_count.length, requests }

    }

    static async getCustomerDetails(id) {
        let user = {}
        user.user = await this.getUserById(id)
        user.addresses = await this.getUserAddressById(id);
        user.vehicles = await this.getUserVehicleById(id);
        user.bookings = await this.getUserBookingById(id);
        return user
    }

    static async getOrders(keyword, limit, page, status) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select o.id as order_id,o.*,u.profile_image as customer_image,u1.profile_image as owner_image,c.name as company_name,vt.name as vehicle_type ,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as customer_name,if(u1.last_name is NOT null ,CONCAT(u1.first_name," ",u1.last_name),u1.first_name) as owner_name from orders as o left join users as u on u.id=o.user_id left join order_items as ott on ott.order_id=o.id left join companies  as c on c.id=ott.company_id left join users as u1 on u1.id=c.user_id left join user_vehicles as uv on o.vehicle_id=uv.id left join vehicle_types as vt on vt.id= uv.vehicle_type where o.is_created =1 `
        if (keyword) {
            sqlQuery += ` and (c.name like  '%${keyword}%' or if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) like  '%${keyword}%' or if(u1.last_name is NOT null ,CONCAT(u1.first_name," ",u1.last_name),u1.first_name) like  '%${keyword}%' ) `
        }
        if (status) {
            sqlQuery += ` and o.status = ${status} `
        }
        sqlQuery += ` order by o.id desc LIMIT ${limit} OFFSET ${offset}`
        let orders = await connection.query(sqlQuery)

        let sqlQueryCount = `select o.id as order_id,o.*,u.profile_image as customer_image,u1.profile_image as owner_image,c.name as company_name,vt.name as vehicle_type ,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as customer_name,if(u1.last_name is NOT null ,CONCAT(u1.first_name," ",u1.last_name),u1.first_name) as owner_name from orders as o left join users as u on u.id=o.user_id left join order_items as ott on ott.order_id=o.id left join companies  as c on c.id=ott.company_id left join users as u1 on u1.id=c.user_id left join user_vehicles as uv on o.vehicle_id=uv.id left join vehicle_types as vt on vt.id= uv.vehicle_type where o.is_created =1 `
        if (keyword) {
            sqlQueryCount += ` and (c.name like  '%${keyword}%'  or if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) like  '%${keyword}%' or if(u1.last_name is NOT null ,CONCAT(u1.first_name," ",u1.last_name),u1.first_name) like  '%${keyword}%' )  `
        }
        if (status) {
            sqlQueryCount += ` and o.status = ${status} `
        }
        let order_count = await connection.query(sqlQueryCount)

        return { count: order_count.length, orders }
    }

    static async getOwnerDetails(id) {
        let owner = {}
        owner.detail = await this.getOwnerById(id)
        owner.workers = await this.getOwnerWorkersList(id);
        owner.withdrawl_requests = await this.getOwnerWithdrawlsList(id);
        owner.services = await this.getOwnerServices(id);
        owner.bookings = await this.getOwnerBookingList(id);
        owner.days = await this.getOwnerBlockDays(id);
        owner.accounts = await this.getOwnerAccounts(id);
        return owner
    }

    static async getWorkerDetails(id) {
        let worker = {}
        worker.detail = await this.getUserById(id)
        worker.bookings = await this.getWorkerBookingList(id);
        return worker
    }

    static async getVehicleMake(type) {
        if (type == 1) {
            var vehicle = "car"
        } else {
            var vehicle = "motorcycle"
        }
        return await axios.get(`https://vpic.nhtsa.dot.gov/api/vehicles/GetMakesForVehicleType/${vehicle}?format=json`).then(function (response) {
            var result = response.data.Results
            var data = []
            for (var i in result) {
                data.push(result[i].MakeName)
            }
            return data;
        });


    }

    static async getVehicleModel(year, make) {
        return await axios.get(`https://vpic.nhtsa.dot.gov/api/vehicles/GetModelsForMake/${make}?format=json`).then(function (response) {
            console.log(response.data.Results)
            var result = response.data.Results
            var data = []
            for (var i in result) {
                data.push(result[i].Model_Name)
            }
            return data;
        });

    }


    static async getOwnerBookingList(id) {
        var company = await this.getCompanyById(id)
        return await connection.query('select o.id as order_id,o.*,c.name as company_name,vt.name as vehicle_type ,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as user_name from orders as o left join users as u on u.id=o.user_id left join order_items as ott on ott.order_id=o.id left join companies  as c on c.id=ott.company_id left join user_vehicles as uv on o.vehicle_id=uv.id left join vehicle_types as vt on vt.id= uv.vehicle_type where o.id in (select DISTINCT ot.order_id from orders as o1 left join order_items as ot on ot.order_id=o1.id where ot.company_id=?)and o.is_created =1', [company[0].id])
    }

    static async getWorkerBookingList(id) {
        return await connection.query('select o.id as order_id,o.*,c.name as company_name,vt.name as vehicle_type ,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as user_name from orders as o left join users as u on u.id=o.user_id left join order_items as ott on ott.order_id=o.id left join companies  as c on c.id=ott.company_id left join user_vehicles as uv on o.vehicle_id=uv.id left join vehicle_types as vt on vt.id= uv.vehicle_type where o.worker_id=? and o.is_created =1', [id])
    }

    static async getOwnerAccounts(id) {
        return await connection.query('select * from user_accounts where user_id=?', [id])
    }

    static async getOwner(id) {
        return await connection.query("select * from users where id =? and user_type =2", [id])
    }

    static async getWorker(id) {
        return await connection.query("select * from users where id =? and user_type =3", [id])
    }

    static async getOwnerBlockDays(id) {
        var company = await this.getCompanyById(id)
        return await connection.query('select * from block_days where company_id=?', [company[0].id])
    }

    static async getUserById(id) {
        let user = await connection.query('select u.*,c.name as company_name,if(sum(uw.credit) is null,0,sum(uw.credit))  as credit from users as u left join user_wallets as uw on uw.user_id=u.id left join companies as c on c.id = u.company_id where u.id=?', [id])
        delete user[0].password
        return user[0]
    }

    static async getOwnerById(id) {
        let user = await connection.query('select u.*,c.name as company_name,c.trade_license,c.passport,c.emirate_id from users as u left join companies as c on c.user_id=u.id where u.id=?', [id])
        delete user[0].password
        return user[0]
    }

    static async editCustomerVehicle(id, type, make, model, year, plate_no, image, color, description) {
        let sqlQuery = 'update user_vehicles set '
        let params = []
        let sql = []
        if (type != undefined) {
            params.push(type)
            sql.push('vehicle_type=?')
        }
        if (make != undefined) {
            params.push(make)
            sql.push('make=?')
        }
        if (model != undefined) {
            params.push(model)
            sql.push('model=?')
        }
        if (year != undefined) {
            params.push(year)
            sql.push('year=?')
        }
        if (plate_no != undefined) {
            params.push(plate_no)
            sql.push('plate_no=?')
        }

        if (color != undefined) {
            params.push(color)
            sql.push('color=?')
        }
        if (description != undefined) {
            params.push(description)
            sql.push('description=?')
        }
        if (image != undefined) {
            params.push(image)
            sql.push('image=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        await connection.query(sqlQuery, params)
        return { message: "Vehicle udpated successfully" }
    }

    static async deleteCustomerVehicle(id) {
        var response = await connection.query('delete from user_vehicles where id = ?', [id])
        if (response.affectedRows > 0) {
            return { message: "Vehicle deleted successfully" }
        } else {
            throw ("Vehicle does not exist")
        }
    }


    static async addRechargePlan(amount, credit) {
        return await connection.query('insert into recharge_plans (amount,credit) values (?,?) ', [amount, credit])
    }

    static async addCommission(commission) {
        return await connection.query('update platform_values set commission = ? where id = 1', [commission])
    }

    static async getCommission() {
        var response = await connection.query('select commission from platform_values where id = 1')
        return response[0]
    }

    static async getRewards(page, limit) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select ur.*,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as customer_name ,u.profile_image as customer_image from user_rewards as ur left join users as u on u.id=ur.user_id where 1 LIMIT ${limit} OFFSET ${offset}`
        let rewards = await connection.query(sqlQuery)

        let sqlQueryCount = `select * from user_rewards `
        let rewardCount = await connection.query(sqlQueryCount)
        return { count: rewardCount.length, rewards }
    }

    static async sendReward(emails, title, image, description) {
        console.log(emails)
        let users = await connection.query('select u.id as user_id,ul.fcm_id from users as u left join user_logins as ul on ul.user_id=u.id where u.email in (?) and u.user_type = 1', [emails])
        let fcm_ids = [];
        let user_ids = [];
        let data = [];
        console.log(users)
        for (var i in users) {
            fcm_ids.push(users[i].fcm_id)
            user_ids.push(users[i].user_id)
        }
        console.log("userddd", user_ids)
        let ids = [...new Set(user_ids)];

        for (var i in ids) {
            data.push([ids[i], title, image, description])
        }
        console.log(data)
        if (data.length > 0) {
            let response = await connection.query('insert into user_rewards(user_id,title,image,description) values ?', [data])
            var title = "Reward Awarded";
            var body = "You won reward"
            await Common.push(fcm_ids, title, body);
            return response
        } else {
            return
        }
    }

    static async getRechargePlans(keyword, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select * from recharge_plans where 1 LIMIT ${limit} OFFSET ${offset}`
        let recharge = await connection.query(sqlQuery)

        let sqlQueryCount = `select * from recharge_plans `
        let rechargeCount = await connection.query(sqlQueryCount)
        return { count: rechargeCount.length, recharge }
    }

    static async getCoupons(keyword, limit, page, type) {

        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select c.*, cat.name as category_name from coupons as c left join service_coupons as sc on sc.coupon_id=c.id left join categories as cat on cat.id=sc.category_id where 1 `
        if (type) {
            sqlQuery += ' and c.type = ? '
        }
        if (keyword) {
            sqlQuery += ` and (cat.name like ? or c.name like ?) `
        }
        sqlQuery += `group by sc.coupon_id LIMIT ${limit} OFFSET ${offset}`
        let coupon = await connection.query(sqlQuery, [type, '%' + keyword + '%', '%' + keyword + '%'])

        let sqlQueryCount = `select c.*, cat.name as category_name from coupons as c left join service_coupons as sc on sc.coupon_id=c.id left join categories as cat on cat.id=sc.category_id  where 1 `
        if (type) {
            sqlQueryCount += ' and c.type = ? '
        }
        if (keyword) {
            sqlQueryCount += ` and cat.name like ? or c.name like ? `
        }
        sqlQueryCount += 'group by sc.coupon_id '
        let couponCount = await connection.query(sqlQueryCount, [type, '%' + keyword + '%', '%' + keyword + '%'])
        return { count: couponCount.length, coupon }
    }

    static async addCoupon(name, type, amount, percent, min_amount, expire_on, category_type, category_id, sub_category_type, sub_category_id) {
        var coupon = await connection.query('select * from coupons where name =? ', [name])
        if (coupon.length > 0) {
            throw ("Coupon name already exists")
        } else {
            var new_coupon = await connection.query('insert into coupons (name, type, amount, percent, min_amount,expire_on,is_selected) values(?,?,?,?,?,?,?)', [name, type, amount, percent, min_amount, expire_on, sub_category_type])
            if (new_coupon) {
                if (category_type == 0) {
                    let services = await connection.query('select * from services ');
                    let service_ids = []
                    console.log(new_coupon)
                    for (var i in services) {
                        service_ids.push([new_coupon.insertId, services[i].category_id, services[i].sub_category_id])
                    }
                    console.log(service_ids)
                    await connection.query('insert into service_coupons(coupon_id, category_id,sub_category_id) values ? ', [service_ids])
                    return new_coupon
                } else if (category_type == 1) {
                    console.log(sub_category_type, sub_category_id)
                    if (sub_category_type == 0) {
                        let sub_categories = await connection.query('select * from sub_categories where category_id = ?', [category_id]);
                        let sub_categories_ids = []
                        for (var i in sub_categories) {
                            sub_categories_ids.push([new_coupon.insertId, sub_categories[i].category_id, sub_categories[i].id])
                        }
                        await connection.query('insert into service_coupons(coupon_id, category_id,sub_category_id) values ? ', [sub_categories_ids])
                        return new_coupon
                    }
                    if (sub_category_type == 1) {
                        console.log("category_id, sub_category_id", category_id, sub_category_id)
                        let sub_categories = await connection.query('select * from sub_categories where category_id =? and id= ?', [category_id, sub_category_id]);
                        await connection.query('insert into service_coupons(coupon_id, category_id,sub_category_id) values (?,?,?) ', [new_coupon.insertId, sub_categories[0].category_id, sub_categories[0].id])
                        return new_coupon
                    }
                }
            }
        }
    }

    static async addService(id, service, location, schedule, timing, vehicle, description, is_powered, extras) {
        let company = await this.getCompanyById(id);
        console.log(company)
        let services = await this.getCompanyService(company[0].id, service.category_id, service.sub_category_id)
        console.log("services", services)
        if (services.length > 0) {
            throw ("Service already exist!");
        } else {
            var response = await this.addCompanyService(company[0].id, service.category_id, service.sub_category_id, service.price)
            var service_id = response.insertId
        }

        var location_response = await this.addServiceLocation(service_id, location.emirate_id, location.areas)
        console.log(location_response)
        var schedule_response = await this.addServiceSchedule(service_id, schedule.type, schedule.days)
        console.log(schedule_response)
        var service_time_response = await this.addServiceTiming(service_id, timing.service_time, timing.reach_time)
        console.log(service_time_response)
        var service_vehicles_response = await this.addServiceVehicles(service_id, vehicle)
        console.log(service_vehicles_response)
        var service_description_response = await this.addServiceDescription(service_id, description.description, description.highlights)
        console.log(service_description_response)
        var service_powered_response = await this.addServicePowered(service_id, is_powered)
        console.log(service_powered_response)
        var service_extras_response = await this.addServiceExtras(service_id, extras)
        console.log(service_extras_response)

        if (service_id) {
            return ({ id: service_id, message: "Service added" })
        }
    }

    static async getCompanyService(id, category_id, sub_category_id) {
        return await connection.query('select * from services where category_id=? and sub_category_id=? and company_id=?', [category_id, sub_category_id, id])
    }

    static async editCustomerAddress(id, type, emirate_id, area_id, street_name, address, house_no, extra_direction, lat, lng) {
        console.log("type", type)
        let sqlQuery = 'update user_addresses set '
        let params = []
        let sql = []
        if (type != undefined) {
            params.push(type)
            sql.push('type=?')
        }
        if (emirate_id != undefined) {
            params.push(emirate_id)
            sql.push('emirate_id=?')
        }
        if (area_id != undefined) {
            params.push(area_id)
            sql.push('area_id=?')
        }
        if (street_name != undefined) {
            params.push(street_name)
            sql.push('street_name=?')
        }
        if (address != undefined) {
            params.push(address)
            sql.push('address=?')
        }

        if (house_no != undefined) {
            params.push(house_no)
            sql.push('house_no=?')
        }
        if (extra_direction != undefined) {
            params.push(extra_direction)
            sql.push('extra_direction=?')
        }
        if (lat != undefined) {
            params.push(lat)
            sql.push('lat=?')
        }
        if (lng != undefined) {
            params.push(lng)
            sql.push('lng=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        var response = await connection.query(sqlQuery, params)
        return { message: "Address udpated successfully" }

    }

    static async deleteCustomerAddress(id) {
        var response = await connection.query('delete from user_addresses where id = ?', [id])
        if (response.affectedRows > 0) {
            return { message: "Address deleted successfully" }
        } else {
            throw ("Address does not exist")
        }
    }

    static async deleteCoupon(id) {
        var response = await connection.query('delete from coupons where id = ?', [id])
        if (response.affectedRows > 0) {
            return { message: "coupon deleted successfully" }
        } else {
            return { message: "coupon does not exist" }
        }
    }

    static async deleteRechargePlan(id) {
        var response = await connection.query('delete from recharge_plans where id = ?', [id])
        if (response.affectedRows > 0) {
            return { message: "Recharge plan deleted successfully" }
        } else {
            return { message: "Recharge plan does not exist" }
        }
    }

    static async getUserAddressById(id) {
        return await connection.query('select address.*,a.name as area_name, e.name as emirate_name from user_addresses as address left join areas as a on a.id=address.area_id left join emirates as e on e.id=address.emirate_id where user_id=?', [id])
    }

    static async getOwnerWorkersList(id) {
        var company = await this.getCompanyById(id)
        var workers = await connection.query('select * from users where users.user_type = ? and users.company_id=? Limit 10', [3, company[0].id])
        for (var i in workers) {
            delete workers[i].password
        }
        return workers
    }

    static async getUserVehicleById(id) {
        return await connection.query('select vehicle.*,type.name,type.description as type_description,type.image as type_image from user_vehicles as vehicle left join vehicle_types as type on type.id=vehicle.vehicle_type where user_id=?', [id])
    }

    static async getUserBookingById(id) {
        return await connection.query('select o.id, o.arrival_time, o.status, o.arrival_date ,if(o.amount is null, 0 ,o.amount)as amount,(select c.name from order_items as ot left join companies as c on c.id= ot.company_id where ot.order_id=o.id limit 1 ) as company_name,vt.name as vehicle_type from orders as o left join user_vehicles as v on v.id =o.vehicle_id left join vehicle_types as vt on v.vehicle_type=vt.id where o.user_id=? and o.is_created=1', [id])
    }


    static async getServiceDetail(id) {
        var company = await this.getServiceById(id);
        if (!company) {
            throw ("Service doe not exist")
        } else {
            let service = {}
            service.service = company
            service.location = await this.getServiceLocationById(id);
            service.schedule = await this.getServiceScheduleById(id);
            service.extras = await this.getServiceExtrasById(id);
            service.description = await this.getServiceHighlightById(id);
            service.vehicle = await this.getServiceVehicleById(id);
            console.log(service)
            return service
        }
    }

    static async editServiceLocation(emirate_id, areas, id) {
        var delete_query = "delete from service_locations where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceLocation(id, emirate_id, areas)
    }

    static async editServiceSchedule(type, days, id) {
        var delete_query = "delete from service_business_hours where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceSchedule(id, type, days)
    }

    static async editServiceExtras(id, extras) {
        var delete_query = "delete from service_extras where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceExtras(id, extras)
    }

    static async editServiceVehicle(id, vehicles) {
        var delete_query = "delete from service_vehicles where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceVehicles(id, vehicles)
    }

    static async editServicePowered(id, id_powered) {
        var delete_query = "update services set is_powered=? where id =?"
        await connection.query(delete_query, [id_powered, id])
    }

    static async editServiceHighlights(id, description, highlights) {
        var delete_query = "delete from service_highlights where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceDescription(id, description, highlights)
    }

    static async getServiceById(id) {
        let response = await connection.query('select s.*,c.name as company_name,c.trade_license,c.passport,c.emirate_id,c.is_trade_license_verified,c.is_passport_verified,c.is_emirate_id_verified,cat.name as category_name, sc.name as sub_category_name,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as owner_name,u.profile_image as owner_image,u.email as owner_email, u.phone_number as owner_phone, u.country_code as owner_country_code from services as s left join categories as cat on s.category_id=cat.id left join sub_categories as sc on sc.id=s.sub_category_id left join companies as c on c.id=s.company_id left join users as u on u.id=c.user_id where s.id=? ', [id])
        return response[0]
    }

    static async getServiceLocationById(id) {
        let response = await connection.query('select loc.id,a.name as area_name,e.name as emirate_name from service_locations as loc left join areas as a on loc.area_id=a.id left join emirates as e on loc.emirate_id=e.id where loc.service_id=?', [id])
        return response
    }

    static async getServiceScheduleById(id) {
        let response = await connection.query('select * from service_business_hours where service_id=?', [id])

        let day = [];
        for (var i in response) {
            day.push(response[i].id)
        }
        let day_ids = [...new Set(day)];
        console.log(day_ids)
        if (day_ids.length > 0) {
            let time_slot = await connection.query('select ds.service_day_id,ts.* from day_slots as ds left join time_slots as ts on ts.id=ds.time_slot_id where ds.service_day_id in (?)', [day_ids])
            response.map(a => {
                a.time_slot = []
                for (var i in time_slot) {
                    if (a.id == time_slot[i].service_day_id) {
                        a.time_slot.push(time_slot[i])
                    }
                }
            })
            console.log(response);
            return response
        } else {
            return
        }

    }

    static async addServiceTiming(id, service_time, reach_time) {
        return await connection.query('update services set service_time=? , reach_time=? where id = ?', [service_time, reach_time, id])
    }

    static async addCompanyService(id, category_id, sub_category_id, price) {
        return await connection.query('insert into services set category_id=? , sub_category_id=?, price=?, company_id=?', [category_id, sub_category_id, price, id])
    }


    static async addServiceLocation(id, emirate_id, areas) {
        var data = []
        for (var i in areas) {
            data.push([emirate_id, areas[i], id])
        }
        console.log(data)
        return await connection.query('insert into service_locations (emirate_id, area_id,service_id) values ?', [data])
    }

    static async addServiceVehicles(id, vehicle) {
        var data = []
        for (var i in vehicle) {
            data.push([vehicle[i].type, vehicle[i].price, id])
        }
        console.log(data)
        return await connection.query('insert into service_vehicles (vehicle_type_id, rate,service_id) values ?', [data])
    }


    static async addServiceSchedule(id, type, days) {
        var data = []
        if (type == 0) {
            var day = [1, 2, 3, 4, 5, 6, 7]
            var slot = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
            for (var i in day) {
                for (var j in slot) {
                    data.push([day[i], slot[j], 2, 0, id])
                }
            }
            return await connection.query('insert into service_business_hours (day,time_slot_id,worker,type,service_id) values ?', [data])
        } else if (type == 1) {
            for (var i in days) {
                var res = await connection.query('insert into service_business_hours (day,worker,type,service_id) values (?,?,?,?)', [days[i].day, days[i].worker, type, id])
                for (var j in days[i].time_slot) {
                    data.push([res.insertId, days[i].time_slot[j]])
                }
                await connection.query('insert into day_slots (service_day_id,time_slot_id) values ? ', [data])
            }
            console.log(data)
            return
        }
    }

    static async addServiceDescription(id, description, hightlights) {
        await connection.query('update services set description=? where id=?', [description, id])
        var data = [];
        for (var i in hightlights) {
            data.push([hightlights[i].type, hightlights[i].description, id])
        }
        console.log(data)
        return await connection.query('insert into service_highlights (type , description, service_id) values ?', [data])
    }

    static async addServiceExtras(id, extras) {
        var data = [];
        for (var i in extras) {
            data.push([extras[i].name, extras[i].description, extras[i].rate, id])
        }
        console.log(data)
        return await connection.query('insert into service_extras (name , description, rate, service_id) values ?', [data])
    }

    static async addServicePowered(id, is_powered) {
        return await connection.query('update services set is_powered=? where id=?', [is_powered, id])
    }

    static async getServiceExtrasById(id) {
        let response = await connection.query('select * from service_extras where service_id=?', [id])
        return response
    }

    static async getServiceHighlightById(id) {
        let response = await connection.query('select * from service_highlights where service_id=?', [id])
        return response
    }

    static async getServiceVehicleById(id) {
        let response = await connection.query('select vehicle.id,vehicle.rate,types.name as vehicle_name, types.description as vehicle_description, types.image from service_vehicles as vehicle left join vehicle_types as types on vehicle.vehicle_type_id= types.id where service_id=?', [id])
        return response
    }

    static async getServiceByCompanyId(id) {
        return await connection.query('select s.*,c.name as category_name,s_c.name as sub_category_name from services as s left join categories as c on s.category_id=c.id left join sub_categories as s_c on s.sub_category_id= s_c.id where s.company_id=? ', [id])
    }

    static async getDailyOrders() {
        return await connection.query('select DAYNAME(created_at) as date,count(id) as orders from orders where orders.created_at BETWEEN SUBDATE(NOW(), INTERVAL 7 DAY) AND NOW() and is_created = 1 group by date order by date asc')
    }

    static async getMonthlyOrders() {
        return await connection.query('select MONTHNAME(created_at) as date,count(id) as orders from orders where orders.created_at BETWEEN SUBDATE(NOW(), INTERVAL 30 DAY) AND NOW() and is_created = 1 group by date order by date asc')
    }

    static async getYearlyOrders() {
        return await connection.query('select YEAR(created_at) as date,count(id) as orders from orders where orders.created_at BETWEEN SUBDATE(NOW(), INTERVAL 365 DAY) AND NOW() and is_created = 1 group by date order by date asc')
    }

    static async getCompanies(keyword, page, limit) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select c.* from companies as c left join users as u on c.user_id=u.id `
        if (keyword) {
            sqlQuery += ` and (c.name like  '%${keyword}%' ) `
        }

        sqlQuery += ` order by c.id desc LIMIT ${limit} OFFSET ${offset}`
        let company = await connection.query(sqlQuery)

        return { count: company.length, company }
    }

    static async getWithdrawls(keyword, page, limit, is_accepted) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select w.*,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as owner_name ,u.profile_image as owner_image from withdraw_requests as w left join users as u on w.user_id=u.id where 1  `
        if (keyword) {
            sqlQuery += ` and (u.first_name like  '%${keyword}%' ) `
        }
        if (is_accepted) {
            sqlQuery += ` and w.status = ? `

        }

        sqlQuery += ` order by w.id desc LIMIT ${limit} OFFSET ${offset}`
        let withdrawl = await connection.query(sqlQuery, [is_accepted])

        let CountsqlQuery = `select w.*,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as owner_name ,u.profile_image as owner_image from withdraw_requests as w left join users as u on w.user_id=u.id where 1  `
        if (keyword) {
            CountsqlQuery += ` and (u.first_name like  '%${keyword}%' ) `
        }
        if (is_accepted) {
            CountsqlQuery += ` and w.status = ? `

        }

        CountsqlQuery += ` order by w.id desc `
        console.log(CountsqlQuery)
        let withdrawls = await connection.query(CountsqlQuery, [is_accepted])
        return { count: withdrawls.length, withdrawl }
    }

    static async getOwnerWithdrawlsList(id) {

        let sqlQuery = `select w.*,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as owner_name from withdraw_requests as w left join users as u on w.user_id=u.id where u.id=? Limit 10`
        let withdrawls = await connection.query(sqlQuery, [id])

        return withdrawls
    }

    static async getTimeSlots(page, limit) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select * from time_slots order by id desc LIMIT ${limit} OFFSET ${offset}`
        let time_slots = await connection.query(sqlQuery)

        return { count: time_slots.length, time_slots }
    }

    static async getWorkerDetail(id) {
        const workers = await connection.query('select u.*,c.name from users as u left join companies as c on u.id=c.user_id where u.id=? and u.user_type=3', [id]);
        return workers[0]
    }

    static async getCompanyById(id) {
        return await connection.query('select c.* from users as u left join companies as c on c.user_id= u.id where u.id=?', [id])
    }

    static async editWorker(id, first_name, last_name, phone_number, country_code, profile_image, password) {
        let sqlQuery = 'update companies set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (phone_number != undefined) {
            params.push(phone_number)
            sql.push('phone_number=?')
        }

        if (country_code != undefined) {
            params.push(country_code)
            sql.push('country_code=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        if (password != undefined) {
            const salt = genSaltSync(10);
            var hash_password = hashSync(password, salt);
            params.push(hash_password)
            sql.push('password=?')
        }

        sqlQuery += sql.toString()
        sqlQuery += ' where user_id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        const response = await connection.query(sqlQuery, params)
        return response
    }

    static async editCategory(id, name) {
        let sqlQuery = 'update categories set name= ? where id=?'
        const response = await connection.query(sqlQuery, [name, id])
        return response
    }

    static async getCategories(id) {
        let sqlQuery = 'select * from categories '
        if (id) {
            sqlQuery += `where id =?`
        }
        sqlQuery += `order by id desc`
        const response = await connection.query(sqlQuery, [id])
        return { count: response.length, response }
    }

    static async getSubCategories(id) {

        let sqlQuery = 'select * from sub_categories '
        if (id) {
            sqlQuery += `where category_id=?`
        }
        sqlQuery += `order by id desc`
        const sub_category_response = await connection.query(sqlQuery, [id])
        return { sub_category_count: sub_category_response.length, sub_category_response }

    }

    static async getEmirates() {
        let sqlQuery = 'select * from emirates'
        const response = await connection.query(sqlQuery)
        return response
    }

    static async getEmirateAreas(id) {
        let sqlQuery = 'select * from areas where emirate_id=?'
        const response = await connection.query(sqlQuery, [id])
        return response
    }

    static async getOrder(order_id) {
        var order = await connection.query('select comp.name as company_name,cat.name as category_name,o.id as order_id,o.*,ua.*,if(u1.last_name is NOT null ,CONCAT(u1.first_name," ",u1.last_name),u1.first_name) as worker_name,u1.profile_image as worker_image,u1.username as worker_username,u1.phone_number as worker_phone ,vt.name as vehicle_type, u1.country_code as worker_country_code,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as customer_name,u.email as customer_email, u.phone_number as customer_phone,u.country_code as customer_country_code,u.profile_image as customer_image from orders as o  left join users as u on u.id=o.user_id left join users as u1 on u1.id=o.worker_id left join companies as comp on comp.id=(select company_id from order_items as ot where order_id=o.id group by company_id) left join categories as cat on cat.id=(select category_id from order_items as ot where ot.order_id=o.id group by category_id) left join user_vehicles as u_v on u_v.id=o.vehicle_id left join vehicle_types as vt on vt.id=u_v.vehicle_type left join user_addresses as u_a on u_a.id=o.address_id left join time_slots as t_s on t_s.id=o.arrival_time left join user_addresses as ua on ua.id=o.address_id where o.id = ? and o.is_created=1', [order_id])
        return order[0]
    }

    static async getOrderDetails(order_id) {
        var services = await connection.query('select * from order_items where order_id=?', [order_id])
        var service_ids = [];
        for (var i in services) {
            service_ids.push(services[i].service_id)
        }
        let service_id = [...new Set(service_ids)];
        console.log(service_id)
        var service = await connection.query('select s.id,c.name as category_name,sub.name as sub_category_name,comp.name as company_name,s.price,s.discount,s.service_time,s.reach_time,s.description,s.is_powered,s.is_blocked,s.created_at from services as s left join categories as c on c.id=s.category_id left join sub_categories as sub on sub.id =s.sub_category_id left join companies as comp on comp.id=s.company_id where s.id IN (?)', [service_id])

        for (var i in service) {
            var extras = await connection.query('select * from order_items where order_id =? and service_id=?', [order_id, service[i].id])
            var extra_ids = [];
            for (var i in extras) {
                extra_ids.push(extras[i].extra_service_id)
            }
            console.log("extra ids", extra_ids)
            if (extra_ids.length > 0) {
                var extra_services = await connection.query('select ot.service_price,ot.quantity,ot.price,se.name,se.description,se.is_multiple from service_extras as se left join order_items as ot on ot.extra_service_id = se.id where se.id IN (?) and ot.order_id=? and ot.service_id=?', [extra_ids, order_id, service[i].id])
                service[i].extra_services = extra_services
            } else {
                service[i].extra_services = []
            }
            var service_highlights = await connection.query('select * from service_highlights where service_id = ?', [service[i].id])
            service[i].highlights = service_highlights
        }
        return service
    }

    static async getUsers(keyword, page, limit, type, is_blocked, is_rewarded) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        if (is_rewarded == 0) {
            let sqlQuery = `select users.*,companies.name,companies.trade_license,companies.passport,companies.emirate_id from users left join companies on companies.user_id=users.id where users.user_type = ?`
            if (keyword) {
                sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'  or users.email like  '%${keyword}%'  or users.phone_number like  '%${keyword}%' or companies.name like  '%${keyword}%' or users.username like  '%${keyword}%' ) `
            }
            if (is_blocked) {
                sqlQuery += ` and users.is_blocked = ? `
            }
            sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
            let user = await connection.query(sqlQuery, [type, is_blocked])
            // console.log(user.sql)
            let main_array = [];
            user.map(item => {
                delete item.password;
                main_array.push(item);
            })

            let get_count = `select users.*,companies.name,companies.trade_license,companies.passport,companies.emirate_id from users left join companies on companies.user_id=users.id where users.user_type =  ? `
            if (keyword) {
                get_count += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'  or users.email like  '%${keyword}%'  or users.phone_number like  '%${keyword}%'  or companies.name like  '%${keyword}%' or users.username like  '%${keyword}%' ) `
            } if (is_blocked) {
                get_count += ` and users.is_blocked =  ? `
            }
            let query = await connection.query(get_count, [type, is_blocked])
            console.log("count ", query.length);


            return { count: query.length, user }
        } else if (is_rewarded == 1) {
            let sqlQuery = `select users.*,companies.name,companies.trade_license,companies.passport,companies.emirate_id from users left join companies on companies.user_id=users.id where users.id not in (select user_id from user_rewards) and users.user_type = ?`
            if (keyword) {
                sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'  or users.email like  '%${keyword}%'  or users.phone_number like  '%${keyword}%'  ) `
            }
            if (is_blocked) {
                sqlQuery += ` and users.is_blocked = ? `
            }
            sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
            let user = await connection.query(sqlQuery, [type, is_blocked])
            // console.log(user.sql)
            let main_array = [];
            user.map(item => {
                delete item.password;
                main_array.push(item);
            })

            let get_count = `select count(id) as count from users where users.id not in (select user_id from user_rewards) and users.user_type = ${type} `
            if (keyword) {
                get_count += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'  or users.email like  '%${keyword}%'  or users.phone_number like  '%${keyword}%'    ) `
            } if (is_blocked) {
                get_count += ` and users.is_blocked =  ${is_blocked} `
            }
            let query = await connection.query(get_count)
            console.log("count ", query.length);


            return { count: query[0].count, user }

        }
    }

    static async getStaffList(keyword, page, limit) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select * from users where user_type=5`

        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or users.email like  '%${keyword}%' or users.city like  '%${keyword}%' or users.state like  '%${keyword}%' or users.country like  '%${keyword}%' or users.address like  '%${keyword}%' or users.user_type like  '%${keyword}%') `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        let user = await connection.query(sqlQuery);
        let main_array = [];
        user.map(item => {
            delete item.password;
            main_array.push(item);
        })

        let get_count = `select count(*) as count from users where user_type!=1`
        if (keyword) {
            get_count += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%' or users.email like  '%${keyword}%' or users.city like  '%${keyword}%' or users.state like  '%${keyword}%' or users.country like  '%${keyword}%' or users.address like  '%${keyword}%' or users.user_type like  '%${keyword}%') `
        }
        let query = await connection.query(get_count)
        // console.log("count ",query);
        return { count: query[0].count, user }

    }

    static async getStaffDetail(id, type) {
        const user = await connection.query('select * from users where id=? and user_type=?', [id, type]);
        if (user.length == 0) {
            throw ("User does not exist")
        } else {
            delete user[0].password;
        }
        return user[0];
    }

    static async deleteUser(id, logged_user) {
        if (logged_user == 2) {
            let response = await connection.query('delete from users where id=? ', [id]);
            console.log(response)
            if (response.affectedRows > 0) {
                return { message: "User Deleted" }
            } else {
                throw ("User does not exist")
            }
        } else {
            throw ("You are not authorized to delete the staff.!");
        }
    }

    static async deleteCategory(id) {
        let response = await connection.query('delete from categories where id=? ', [id]);
        console.log(response)
        if (response.affectedRows > 0) {
            return { message: "Category Deleted" }
        } else {
            throw ("Category does not exist")
        }

    }

    static async deactivateUser(id, logged_user, is_deactivate, deactivate_reason) {
        if (logged_user == 2) {
            if (is_deactivate == 1) {
                let response = await connection.query('update users set is_deactivated = 1,deactivate_reason=? where id=? ', [deactivate_reason, id]);
                return response
            } else {
                let response = await connection.query('update users set is_deactivated = 0, deactivate_reason = null where id=? ', [id]);
                console.log(response)
                return response

            }

        } else {
            throw ("You are not authorized to deactivate the staff.!");
        }
    }

    static async verifyUser(id, logged_user, is_verified) {
        if (logged_user == 2) {
            let response = await connection.query('update users set is_user_verified = ? where id=? ', [is_verified, id]);
            return response
        } else {
            throw ("You are not authorized to deactivate the staff.!");
        }
    }

    static async addUser(first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type) {
        if (await this.checkIsEmailExist(email)) {
            throw ("Email already exist!");
        }
        const salt = genSaltSync(10);
        let hashPassword = hashSync(password, salt);
        const response = await connection.query('insert into users set first_name=?,last_name=?,email=?,password=?, country_code=?, phone_number=?, whatsapp_number = ?,whatsapp_country_code=?,user_type=?', [first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type]);
        console.log(response)
        if (response) {
            return ({ id: response.insertId, message: "User added" })
        }
    }

    static async addContent(title, content, image) {
        const response = await connection.query('insert into static_pages set title=?,content=?, image=?', [title, content, image]);
        console.log(response)
        if (response.insertId > 0) {
            return ({ id: response.insertId, message: "Content added" })
        }
    }

    static async addProject(title, description, price, location, images) {
        const response = await connection.query('insert into projects set title=?,description=?,price=?,location=?', [title, description, price, location]);
        console.log(response)
        if (images) {
            for (var i in images) {
                await connection.query('insert into project_images set project_id=?,image=?', [response.insertId, images[i]]);
            }
        }
        if (response) {
            return ({ id: response.insertId, message: "Project added" })
        }
    }

    static async checkIsEmailExist(email) {
        let user = await this.getUserByEmail(email);
        if (user[0]) {
            return 1
        } else {
            return 0
        }
    }

    static async deleteWorker(id) {
        const response = await connection.query('delete from users where id = ? and user_type = 3', [id]);
        return response
    }

    static async addUser(first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type, company_name, trade_license, passport, emirate_id, profile_image, country_abbr) {
        let user = await this.getUserProfileByEmail(email);
        console.log(user)
        if (user[0]) {
            throw ("Email already exist!");
        }
        let user_phone = await this.getUserProfileByPhone(phone_number, country_code);
        if (user_phone[0]) {
            throw ("Phone number already exist!");
        }
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        const email_verify_token = await Common.generateRandomString(15);
        // const phone_verification_code = await Common.generateRandomNumber(6);
        // const phone_verification_code = 111111;
        console.log("whatsapp number", whatsapp_number)
        if (whatsapp_number == undefined) {
            whatsapp_number = phone_number
            whatsapp_country_code = country_code
        }
        var sqlQuery = 'insert into users(first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, user_type,profile_image,country_abbr) values (?,?,?,?,?,?,?,?,?,?,?)';
        let params = [first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type, profile_image, country_abbr]
        let userInsert = await connection.query(sqlQuery, params);
        if (type == 2) {
            var Query = "insert into companies(name, trade_license, passport, emirate_id , user_id ) values (?,?,?,?,?) "
            let params = [company_name, trade_license, passport, emirate_id, userInsert.insertId]
            await connection.query(Query, params);
        }

        // user = await this.getUserProfile(userInsert.insertId)
        // let token = await this.generateToken(user);
        // await this.insertOnUpdateDeviceInfo(device_id, fcm_id, device_type, userInsert.insertId)
        let email_subject = 'Email Verification'
        let text = "Verify your email by clicking on the below link."
        let link_text = "Verify Email"
        let url = `https://app-transfer.com:3008/verify-email/${email_verify_token}`
        // await Common.sendMail(email, url, email_subject, text, link_text);
        // if (phone_number && user[0].is_sms_notified == 1) {
        //     let message = "Your verification code is " + phone_verification_code
        //     let send_msg = await Common.sendMessage(phone_number, message)
        //     console.log(send_msg);
        // }
        // delete user[0].password
        // delete user[0].email_verify_token
        if (type == 1) {
            var user_type = "Customer"
        } else if (type == 2) {
            var user_type = "Owner"
        }
        return ({ 'message': `${user_type} created successfully` })

    }

    static async getUserProfile(id) {
        let sqlQuery = `select * from users where id=${id}`
        return await connection.query(sqlQuery)

    }

    static async getUserProfileByEmail(email) {
        let sqlQuery = `select * from users where email=? `
        return await connection.query(sqlQuery, [email])
    }

    static async getUserProfileByUsername(username, type) {
        let sqlQuery = `select * from users where username=? and user_type=?`
        return await connection.query(sqlQuery, [username, type])

    }

    static async getUserProfileByPhone(phone_no, country_code) {
        let sqlQuery = `select * from users where phone_number=? and country_code=?`
        return await connection.query(sqlQuery, [phone_no, country_code])

    }

    static async getContents(keyword, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select * from static_pages`
        if (keyword) {
            sqlQuery += ` where (static_pages.title like  '%${keyword}%' or static_pages.content like  '%${keyword}%') `
        }
        sqlQuery += ` order by static_pages.id asc LIMIT ${limit} OFFSET ${offset}`
        let content = await connection.query(sqlQuery);
        let main_array = [];
        content.map(item => {
            main_array.push(item);
        })
        let get_count = `select count(*) as count from static_pages`
        if (keyword) {
            get_count += ` where (static_pages.title like  '%${keyword}%' or static_pages.content like  '%${keyword}%') `
        }
        let query = await connection.query(get_count)
        return { count: query[0].count, content }
    }

    static async getContentDetail(id) {
        return await connection.query('select * from static_pages where id=?', [id]);
    }

    static async editContent(id, title, content, image) {
        let sqlQuery = 'update static_pages set '
        let params = []
        let sql = []
        if (title != undefined) {
            params.push(title)
            sql.push('title=?')
        }
        if (content != undefined) {
            params.push(content)
            sql.push('content=?')
        }
        if (image != undefined) {
            params.push(image)
            sql.push('image=?')
        }

        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        const response = await connection.query(sqlQuery, params)
        return response
    }

    static async deleteContent(content_id) {
        const response = await connection.query('delete from static_pages where id = ?', [content_id]);
        if (response.affectedRows > 0) {
            return ({ message: "Content deleted" })
        } else {
            throw ("Content does not exist")
        }
    }

    static async deleteContact(contact_id) {
        const response = await connection.query('delete from contact_us where id = ?', [contact_id]);
        if (response.affectedRows > 0) {
            return ({ message: "Contact deleted" })
        } else {
            throw ("Contact does not exist")
        }
    }

    static async sendMail(email, subject, content) {
        if (!email) {
            const data = await connection.query('select email from users where user_type=1 and is_email_notified = 1');
            var emails = []
            for (var i in data) {
                emails.push(data[i].email)
            }
            console.log(emails)
            // return await Common.sendMail(emails, subject, content)
            return await Common.sendMail(emails, null, subject, content, null)
        } else {
            return await Common.sendMail(email, null, subject, content, null)
        }

    }

    static async sendPush(email, subject, content) {
        if (!email) {
            const data = await connection.query('select fcm_id from user_logins inner join users on users.id=user_logins.user_id where is_push_notified = 1');
            var fcm_ids = []
            for (var i in data) {
                fcm_ids.push(data[i].fcm_id)
            }
            console.log(fcm_ids)
            return await Common.push(fcm_ids, subject, content)
        } else {
            const data = await connection.query('select fcm_id from user_logins inner join users on users.id=user_logins.user_id where users.email IN (?) and users.is_push_notified = 1', email);
            var fcm_ids = []
            for (var i in data) {
                fcm_ids.push(data[i].fcm_id)
            }
            console.log(fcm_ids)
            return await Common.push(fcm_ids, subject, content)
        }
    }

    static async getContacts(keyword, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)

        var limit = parseInt(lmt)
        let sqlQuery = `select * from contact_us `
        if (keyword) {
            sqlQuery += ` where ( contact_us.first_name like  '%${keyword}%' or contact_us.message like  '%${keyword}%' or contact_us.last_name like  '%${keyword}%' or contact_us.email like  '%${keyword}%') `
        }
        sqlQuery += ` order by contact_us.id desc LIMIT ${limit} OFFSET ${offset}`
        let contact = await connection.query(sqlQuery);
        let main_array = [];
        contact.map(item => {
            main_array.push(item);
        })
        let get_count = `select count(*) as count from contact_us`
        if (keyword) {
            get_count += ` where ( contact_us.first_name like  '%${keyword}%' or contact_us.message like  '%${keyword}%' or contact_us.last_name like  '%${keyword}%'  or contact_us.email like  '%${keyword}%')`
        }
        let query = await connection.query(get_count)
        return { count: query[0].count, contact }
    }

    static async dbBackup() {
        var username = process.env.DB_USER;
        var password = process.env.DB_PASSWORD;
        var databaseName = process.env.DB_DATABASE;
        console.log('123')
        //  var tablename= "users";
        //creating key for backup file
        // var key =
        //   databaseName + '-' + moment().format('YYYY-MM-DD-HH-mm-ss') + '.sql';
        var key = databaseName + '.sql'
        var dumpCommand = `mysqldump -u${username} -p${password} ${databaseName} > db/${key}`;

        // dumpCommand mysqldump -uroot -proot demo
        childProcess.execSync(dumpCommand, (error, stdout, stderr) => {

            // const mailOptions = {
            //     to: email, // reciever address,
            //     // to: 'janardhan@henceforthsolutions.com', // reciever address,
            //     subject: 'DB Backup', // Subject line
            //     text: 'Hi Admin, This is Latest DB Backup', // plaintext body
            //     html: `<p>Hi Admin,</br></p><p>This is Latest DB Backup</p>`,
            //     // <a href='${download_url}'>${download_url}</a>`, // HTML body content
            //     attachments: {
            //         filename: key,
            //         path: 'db/' + key,
            //         contentType: '.sql',
            //     },
            // };
            // // console.log(mailOptions.attachments);
            // var transporter = nodemailer.createTransport({
            //     service: 'gmail',
            //     auth: {
            //         user: 'prince.bhatia.henceforth@gmail.com',
            //         pass: 'nozuwdslsyadzbkh'
            //     }
            // });
            // // sending email
            // var response = transporter.sendMail(mailOptions, function (error, info) {
            //     if (error) {
            //         console.log(error)
            //         logger.error(`message - ${error.message}, stack trace - ${error.stack}`);
            //     }

            //     else {

            //         console.log('Email sent', info);

            //         return info
            //     }

            // });

            // return stdout

        });

        // fs.unlink("db/" + key,function (err, ) {
        //     if (err) {
        //         return err;
        //     }
        // // });
        // return res
        // return file

        var buffer = ("db/" + key);
        console.log('buffer', buffer);
        return key

    }



}
module.exports = AdminService;