var express = require('express');
const { checkSchema } = require('express-validator');
let adminSchema = require("./admin.validation.json");
var router = express.Router();
const valid = require('../../config/validator');
const AdminController = require("./admin.controller");
let auth = require("../../middleware/auth");

//Admin Profile
router.post('/login', checkSchema(adminSchema.login), valid, AdminController.login);
router.get('/profile', auth.checkAdminToken, AdminController.getProfile);
router.put('/profile', auth.checkAdminToken, AdminController.updateProfile);
router.put('/change-password', auth.checkAdminToken, AdminController.changePassword);

//Admin Dashboard
router.get('/dashboard', auth.checkAdminToken, AdminController.dashboard);

//User management
router.post('/user', auth.checkAdminToken, AdminController.addUser);
router.get('/users', auth.checkAdminToken, AdminController.getUsers);
router.get('/user', auth.checkAdminToken, AdminController.getUserDetail)
router.put('/user', auth.checkAdminToken, AdminController.editUserDetail);
router.put('/user/block', auth.checkAdminToken, AdminController.blockUser);
router.delete('/user', auth.checkAdminToken, AdminController.deleteUser);
router.post('/user/deactivate', auth.checkAdminToken, AdminController.deactivateUser);
router.post('/user/verify', auth.checkAdminToken, AdminController.verifyUser);
router.put('/user/change-password', auth.checkAdminToken, AdminController.changeUserPassword);

//category management
router.post('/category', auth.checkAdminToken, AdminController.addCategory);
router.post('/sub-category', auth.checkAdminToken, AdminController.addSubCategory);
router.get('/categories', auth.checkAdminToken, AdminController.getCategories);
router.get('/sub-categories', auth.checkAdminToken, AdminController.getSubCategories);
router.get('/category', auth.checkAdminToken, AdminController.getCategoryDetail);
router.put('/category', auth.checkAdminToken, AdminController.editCategory);
router.delete('/category', auth.checkAdminToken, AdminController.deleteCategory);

//worker management
router.post('/worker', auth.checkAdminToken, AdminController.addWorker);
router.get('/workers', auth.checkAdminToken, AdminController.getWorkers);
router.get('/worker', auth.checkAdminToken, AdminController.getWorkerDetail);
router.put('/worker', auth.checkAdminToken, AdminController.editWorker);
router.delete('/worker', auth.checkAdminToken, AdminController.deleteWorker);
router.post('/worker/change-password', auth.checkAdminToken, AdminController.workerChangePassword)

//service management
router.get('/services', auth.checkAdminToken, AdminController.getServices);
router.get('/service', auth.checkAdminToken, AdminController.getService);
router.post('/service', auth.checkAdminToken, AdminController.addService);
router.put('/service', auth.checkAdminToken, AdminController.editService);
router.put('/service/block', auth.checkAdminToken, AdminController.blockService);

//owner management
router.get('/owner', auth.checkAdminToken, AdminController.getOwnerDetails);
router.get('/owner/workers', auth.checkAdminToken, AdminController.getOwnerWorkers);
router.get('/owner/services', auth.checkAdminToken, AdminController.getOwnerServices);
router.get('/owner/withdrawl-requests', auth.checkAdminToken, AdminController.getOwnerWithdrawls);
router.get('/owner/block-days', auth.checkAdminToken, AdminController.getOwnerBlockDays);

//Emirate MAnagement
router.get('/emirates', auth.checkAdminToken, AdminController.getEmirates);
router.get('/areas', auth.checkAdminToken, AdminController.getEmirateAreas);

//customer management
router.get('/customer', auth.checkAdminToken, AdminController.getCustomerDetails);
router.put('/customer/vehicle', auth.checkAdminToken, AdminController.editCustomerVehicle);
router.delete('/customer/vehicle', auth.checkAdminToken, AdminController.deleteCustomerVehicle);

//recharge plans
router.post('/recharge-plan', auth.checkAdminToken, AdminController.addRechargePlan);
router.get('/recharge-plan', auth.checkAdminToken, AdminController.getRechargePlans);
router.delete('/recharge-plan', auth.checkAdminToken, AdminController.deleteRechargePlan);

//Bookings/Orders
router.get('/orders', auth.checkAdminToken, AdminController.getOrders);
router.get('/order', auth.checkAdminToken, AdminController.getOrderDetail);
router.get('/invoice', auth.checkAdminToken, AdminController.getOrderInvoice);

//customer management
router.post('/coupon', auth.checkAdminToken, AdminController.addCoupon);
router.get('/coupon', auth.checkAdminToken, AdminController.getCoupons);
router.delete('/coupon', auth.checkAdminToken, AdminController.deleteCoupon);

//vehicle management
router.get('/vehicle-make', auth.checkAdminToken, AdminController.getVehicleMake);
router.get('/vehicle-model', auth.checkAdminToken, AdminController.getVehicleModel);

//reward management
router.post('/reward', auth.checkAdminToken, AdminController.sendReward);
router.get('/reward', auth.checkAdminToken, AdminController.getRewards);

//reward management
router.post('/commission', auth.checkAdminToken, AdminController.addCommission);
router.get('/commission', auth.checkAdminToken, AdminController.getCommission);

//Address Request management
router.get('/address-requests', auth.checkAdminToken, AdminController.getAddressRequests);

//Admin Booking Invoice
router.get('/order-invoice', auth.checkAdminToken, AdminController.getAdminOrderInvoice);

//Address management
router.put('/customer/address', auth.checkAdminToken, AdminController.editCustomerAddress);
router.delete('/customer/address', auth.checkAdminToken, AdminController.deleteCustomerAddress);

//company management
router.get('/companies', auth.checkAdminToken, AdminController.getCompanies);

//Withdrawl Requests
router.get('/withdrawls', auth.checkAdminToken, AdminController.getWithdrawls);
router.put('/withdrawl-request', auth.checkAdminToken, AdminController.editWithdrawlRequest);

//Time Slots
router.get('/timeslots', auth.checkAdminToken, AdminController.getTimeSlots);

//staff management
router.get('/staffs', auth.checkAdminToken, AdminController.staffList);
router.get('/staff', auth.checkAdminToken, AdminController.getStaffDetail);

//content management
router.post('/content', auth.checkAdminToken, AdminController.addContent);
router.get('/contents', auth.checkAdminToken, AdminController.getContents);
router.get('/content', auth.checkAdminToken, AdminController.getContentDetail);
router.put('/content', auth.checkAdminToken, AdminController.editContent);
router.delete('/content', auth.checkAdminToken, AdminController.deleteContent);

//send mail
router.post('/notification', auth.checkAdminToken, AdminController.sendNotification);

//contact-us
router.get('/contact-us', auth.checkAdminToken, AdminController.getContacts);
router.delete('/contact-us', auth.checkAdminToken, AdminController.deleteContact);

//backup-db
router.get('/backup-db', auth.checkAdminToken, AdminController.dbBackup);


module.exports = router;