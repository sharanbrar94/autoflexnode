require('dotenv').config();
const logger = require('../../config/logger'); // importiing winston logger module
const jwt = require('jsonwebtoken')
const AdminService = require("./admin.service");
const path = require("path")
class AdminController {
  static async login(req, res, next) {
    try {
      let email = req.body.email;
      let password = req.body.password
      let user = await AdminService.login(email, password);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async dashboard(req, res, next) {

    try {
      let users = await AdminService.getUsers(null, null, null, 1, null, 0)
      console.log(users.count);
      let owners = await AdminService.getUsers(null, null, null, 2, null, 0)
      console.log(owners.count);
      let workers = await AdminService.getUsers(null, null, null, 3, null, 0)
      console.log(workers.count);
      let services = await AdminService.getServices(null, null, null, null);
      console.log(services.service_count)
      let categories = await AdminService.getCategories();
      console.log(categories.count);
      let bookings = await AdminService.getOrders(null, null, null, null);
      console.log(bookings.count);
      let withdrawals = await AdminService.getWithdrawls(null, null, null);
      console.log(withdrawals.count);
      let daily_orders = await AdminService.getDailyOrders();
      let monthly_orders = await AdminService.getMonthlyOrders();
      let yearly_orders = await AdminService.getYearlyOrders();

      return res.status(200).json({ customer_count: users.count, owner_count: owners.count, worker_count: workers.count, service_count: services.service_count, category_count: categories.count, booking_count: bookings.count, withdrawal_count: withdrawals.count, daily_count: daily_orders, monthly_count: monthly_orders, yearly_count: yearly_orders })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }

  }

  static async updateProfile(req, res, next) {
    try {
      let first_name = req.body.first_name;
      let last_name = req.body.last_name;
      let profile_image = req.body.profile_image;
      let logged_id = req.user.id
      const response = await AdminService.updateProfile(logged_id, first_name, last_name, profile_image)
      return res.status(200).json(response)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async changePassword(req, res, next) {
    try {
      let logged_id = req.user.id;
      let old_password = req.body.old_password;
      let new_password = req.body.new_password;
      console.log(logged_id)
      const response = await AdminService.changePassword(new_password, old_password, logged_id);
      return res.status(200).json(response)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async changeUserPassword(req, res, next) {
    try {
      let user_id = req.body.id;
      let type = req.body.type;
      let old_password = req.body.old_password;
      let new_password = req.body.new_password;

      const response = await AdminService.changeUserPassword(new_password, old_password, user_id, type);
      return res.status(200).json(response)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async workerChangePassword(req, res, next) {
    try {
      const { id: worker_id, password } = req.body;
      console.log(logged_id)
      const response = await AdminService.workerChangePassword(worker_id, password);
      return res.status(200).json(response)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getProfile(req, res, next) {
    try {
      let user_id = req.user.id;
      let user = await AdminService.getProfile(user_id)
      return res.status(200).json(user[0])
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  // User Managemant 
  static async getUsers(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      let type = req.query.type;
      let is_blocked = req.query.is_blocked;
      let is_rewarded = req.query.is_rewarded;
      console.log(limit + ' ' + page);
      let users = await AdminService.getUsers(keyword, page, limit, type, is_blocked, is_rewarded)

      return res.status(200).json(users)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getUserDetail(req, res, next) {
    try {
      let id = req.query.id
      const user = await AdminService.getUserDetail(id);
      console.log(user);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }



  static async editUserDetail(req, res, next) {
    try {

      const { id: logged_id, first_name, last_name, email, phone_number, country_code, profile_image, whatsapp_number, whatsapp_country_code, company_name, trade_license, emirate_id, passport, country_abbr, type } = req.body

      const response = await AdminService.updateUserProfile(logged_id, first_name, last_name, email, phone_number, country_code, profile_image, whatsapp_number, whatsapp_country_code, type, company_name, trade_license, emirate_id, passport, country_abbr);
      if (response) {
        return res.status(200).json({ message: "user profile updated" });
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async blockUser(req, res, next) {
    try {
      console.log(1)
      let id = req.body.id;
      let unblock = req.body.unblock;
      const response = await AdminService.blockUser(id, unblock);
      if (response) {
        console.log(3)
        return res.status(200).json({ message: 'block status updated' })
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  // Project Management

  static async getWorkers(req, res, next) {
    try {
      const { keyword, limit, page, is_blocked } = req.query;
      let users = await AdminService.getWorkers(keyword, page, limit, is_blocked)
      return res.status(200).json(users)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getServices(req, res, next) {
    try {
      const { id, keyword, limit, page, is_blocked } = req.query;
      let services = await AdminService.getServices(id, keyword, page, limit, is_blocked)
      return res.status(200).json(services)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Edit Service Location
  static async editService(req, res, next) {
    try {

      const { id, editExtras, editDescription, editLocation, editSchedule, editTiming, editVehicle, is_powered } = req.body
      if (editLocation) {
        await AdminService.editServiceLocation(editLocation.emirate_id, editLocation.areas, id);
      }
      if (editExtras) {
        await AdminService.editServiceExtras(id, editExtras);
      }
      if (editDescription) {
        await AdminService.editServiceHighlights(id, editDescription.description, editDescription.highlights);
      }
      if (editSchedule) {
        await AdminService.editServiceSchedule(editSchedule.type, editSchedule.days, id);
      }
      if (editTiming) {
        await AdminService.addServiceTiming(id, editTiming.service_time, editTiming.reach_time);
      }
      if (editVehicle) {
        await AdminService.editServiceVehicle(id, editVehicle);
      }
      if (is_powered) {
        await AdminService.editServicePowered(id, is_powered);
      }

      return res.status(200).json({ message: "Service Updated." });

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async blockService(req, res, next) {
    try {
      const { id, is_blocked } = req.body;
      let response = await AdminService.blockService(id, is_blocked);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async addService(req, res, next) {
    try {
      const { id, service, location, schedule, timing, vehicle, description, is_powered, extras } = req.body
      const response = await AdminService.addService(id, service, location, schedule, timing, vehicle, description, is_powered, extras);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async addCoupon(req, res, next) {
    try {
      const { name, type, amount, percent, min_amount, expire_on, category_type, category_id, sub_category_type, sub_category_id } = req.body
      const response = await AdminService.addCoupon(name, type, amount, percent, min_amount, expire_on, category_type, category_id, sub_category_type, sub_category_id);
      return res.status(200).json({ message: "Coupon added" });

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async sendReward(req, res, next) {
    try {
      const { emails, title, image, description } = req.body
      await AdminService.sendReward(emails, title, image, description);
      return res.status(200).json({ message: "Reward Sent Successfully" });

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async addCommission(req, res, next) {
    try {
      const { commission } = req.body
      await AdminService.addCommission(commission);
      return res.status(200).json({ message: "Commission Added" });

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getCommission(req, res, next) {
    try {
      let response = await AdminService.getCommission();
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getRewards(req, res, next) {
    try {
      const { limit, page } = req.query;
      let rewards = await AdminService.getRewards(page, limit)
      return res.status(200).json(rewards)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async addRechargePlan(req, res, next) {
    try {
      const { amount, credit } = req.body
      const response = await AdminService.addRechargePlan(amount, credit);
      return res.status(200).json({ id: response.insertId, message: "Recharge plan added" });

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  static async getRechargePlans(req, res, next) {
    try {
      const { keyword, limit, page } = req.query;
      const response = await AdminService.getRechargePlans(keyword, limit, page);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getCoupons(req, res, next) {
    try {
      const { keyword, limit, page, type } = req.query;
      const response = await AdminService.getCoupons(keyword, limit, page, type);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async editWithdrawlRequest(req, res, next) {
    try {
      const { id, status } = req.body;
      let response = await AdminService.editWithdrawlRequest(id, status);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  static async getOwnerWorkers(req, res, next) {
    try {
      const { id, keyword, limit, page, is_blocked } = req.query;
      let workers = await AdminService.getOwnerWorkers(id, keyword, page, limit, is_blocked)
      return res.status(200).json(workers)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getCustomerDetails(req, res, next) {
    try {
      const { id } = req.query;
      let user = await AdminService.getCustomerDetails(id)
      return res.status(200).json(user)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getOrders(req, res, next) {
    try {
      const { keyword, limit, page, status } = req.query;
      let orders = await AdminService.getOrders(keyword, limit, page, status)
      return res.status(200).json(orders)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getOrderDetail(req, res, next) {
    try {
      const { id: order_id } = req.query;
      const order = await AdminService.getOrder(order_id);
      const services = await AdminService.getOrderDetails(order_id);
      console.log("service", services)
      order['services'] = services
      return res.status(200).json(order);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getOrderInvoice(req, res, next) {
    try {
      const { id: order_id } = req.query;
      const order = await AdminService.getOrder(order_id);
      const services = await AdminService.getOrderDetails(order_id);
      console.log("service", services)
      order['services'] = services
      return res.status(200).json(order);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getOwnerDetails(req, res, next) {
    try {
      const { id } = req.query;
      let owner = await AdminService.getOwner(id);
      if (owner.length > 0) {
        let user = await AdminService.getOwnerDetails(id)
        return res.status(200).json(user)
      } else {
        return res.status(400).json({ 'error': "bad_request", 'error_description': "Owner does not exist" });
      }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getWorkerDetail(req, res, next) {
    try {
      const { id } = req.query;
      let worker = await AdminService.getWorker(id);
      console.log(worker)
      if (worker.length > 0) {
        let user = await AdminService.getWorkerDetails(id)
        return res.status(200).json(user)
      } else {
        return res.status(400).json({ 'error': "bad_request", 'error_description': "Worker does not exist" });
      }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getVehicleMake(req, res, next) {
    try {
      const { type } = req.query;
      const response = await AdminService.getVehicleMake(type);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getVehicleModel(req, res, next) {
    try {
      const { year, make } = req.query;
      const response = await AdminService.getVehicleModel(year, make);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getOwnerServices(req, res, next) {
    try {
      const { id } = req.query;
      let services = await AdminService.getOwnerServices(id)
      return res.status(200).json(services)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getAddressRequests(req, res, next) {
    try {
      const { keyword, limit, page } = req.query;
      const response = await AdminService.getAddressRequests(keyword, limit, page);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getAdminOrderInvoice(req, res, next) {
    try {
      const { id: order_id } = req.query;
      const invoice={}
      invoice["details"] = await AdminService.getAdminOrderDetail(order_id);
      invoice["transactions"] = await AdminService.getOrderTransactions(order_id);
      return res.status(200).json(invoice);
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getOwnerWithdrawls(req, res, next) {
    try {
      const { id } = req.query;
      let withdrawls = await AdminService.getOwnerWithdrawls(id)
      return res.status(200).json(withdrawls)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getOwnerBlockDays(req, res, next) {
    try {
      const { id } = req.query;
      let services = await AdminService.getOwnerBlockDays(id)
      return res.status(200).json(services)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async editCustomerVehicle(req, res, next) {
    try {
      const { id, type, make, model, year, plate_no, image, color, description } = req.body;
      const response = await AdminService.editCustomerVehicle(id, type, make, model, year, plate_no, image, color, description);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async editCustomerAddress(req, res, next) {
    try {
      const { id, type, emirate_id, area_id, street_name, address, house_no, extra_direction, lat, lng } = req.body;
      const response = await AdminService.editCustomerAddress(id, type, emirate_id, area_id, street_name, address, house_no, extra_direction, lat, lng);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteCustomerAddress(req, res, next) {
    try {
      const { id } = req.body;
      const response = await AdminService.deleteCustomerAddress(id);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteCoupon(req, res, next) {
    try {
      const { id } = req.body;
      const response = await AdminService.deleteCoupon(id);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteRechargePlan(req, res, next) {
    try {
      const { id } = req.body;
      const response = await AdminService.deleteRechargePlan(id);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteCustomerVehicle(req, res, next) {
    try {
      const { id } = req.body;
      const response = await AdminService.deleteCustomerVehicle(id);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getService(req, res, next) {
    try {
      const { id: service_id } = req.query;
      const response = await AdminService.getServiceDetail(service_id);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  //get companies
  static async getCompanies(req, res, next) {
    try {
      const { keyword, limit, page } = req.query;
      let companies = await AdminService.getCompanies(keyword, page, limit)
      return res.status(200).json(companies)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //get withdrawls
  static async getWithdrawls(req, res, next) {
    try {
      const { keyword, limit, page, is_accepted } = req.query;
      let withdrawls = await AdminService.getWithdrawls(keyword, page, limit, is_accepted)
      return res.status(200).json(withdrawls)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //get time slots
  static async getTimeSlots(req, res, next) {
    try {
      const { limit, page } = req.query;
      let withdrawls = await AdminService.getTimeSlots(page, limit)
      return res.status(200).json(withdrawls)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async editWorker(req, res, next) {
    try {
      const { id, first_name, last_name, phone_number, country_code, profile_image, password } = req.body;
      await AdminService.editWorker(id, first_name, last_name, phone_number, country_code, profile_image, password);
      return res.status(200).json({
        message: 'Worker Updated'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  static async addWorker(req, res, next) {
    try {
      console.log(req.user);
      const { company_id: id, profile_image, first_name, last_name, password, country_code, phone_number, username } = req.body;
      console.log(req.body)
      const response = await AdminService.addWorker(id, profile_image, first_name, last_name, password, country_code, phone_number, username);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async addCategory(req, res, next) {
    try {
      const { name, image } = req.body;
      const response = await AdminService.addCategory(name, image);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async addSubCategory(req, res, next) {
    try {
      const { id, name } = req.body;
      const response = await AdminService.addSubCategory(name, id);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async addEmirate(req, res, next) {
    try {
      const { name } = req.body;
      const response = await AdminService.addEmirate(name);
      return res.status(200).json(response);

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getCategories(req, res, next) {
    try {
      const { id } = req.query;
      let categories = await AdminService.getCategories(id)

      return res.status(200).json(categories)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getSubCategories(req, res, next) {
    try {
      const { id } = req.query;
      let categories = await AdminService.getSubCategories(id)
      return res.status(200).json(categories)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getEmirates(req, res, next) {
    try {
      let emirates = await AdminService.getEmirates()

      return res.status(200).json(emirates)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getEmirateAreas(req, res, next) {
    try {
      const { id } = req.query;
      let areas = await AdminService.getEmirateAreas(id)

      return res.status(200).json(areas)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getCategoryDetail(req, res, next) {
    try {
      let id = req.query.id
      const category = await AdminService.getCategoryDetail(id);
      console.log(category);
      return res.status(200).json(category)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getEmirateDetail(req, res, next) {
    try {
      let id = req.query.id
      const category = await AdminService.getEmirateDetail(id);
      console.log(category);
      return res.status(200).json(category)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  static async editCategory(req, res, next) {
    try {
      const { id, name } = req.body;
      await AdminService.editCategory(id, name);
      return res.status(200).json({
        message: 'Category Updated'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  static async addUser(req, res, next) {
    try {
      const { first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type, company_name, trade_license, passport, emirate_id, profile_image, country_abbr } = req.body;
      const response = await AdminService.addUser(first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type, company_name, trade_license, passport, emirate_id, profile_image, country_abbr)
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      console.log(err)
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async staffList(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      console.log(limit + ' ' + page);
      let users = await AdminService.getStaffList(keyword, page, limit)
      return res.status(200).json(users)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getStaffDetail(req, res, next) {
    try {
      let id = req.query.id;
      let type = req.query.type;
      const user = await AdminService.getStaffDetail(id, type);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteUser(req, res, next) {
    try {
      let id = req.body.id;
      let logged_user = req.user.user_type;
      console.log("type", logged_user)
      const response = await AdminService.deleteUser(id, logged_user);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }


  static async deleteCategory(req, res, next) {
    try {
      const { id } = req.body;
      const response = await AdminService.deleteCategory(id);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deactivateUser(req, res, next) {
    try {
      let id = req.body.id;
      let is_deactivate = req.body.is_deactivate;
      let deactivate_reason = req.body.deactivate_reason;
      let logged_user = req.user.user_type;
      console.log("type", logged_user)
      const response = await AdminService.deactivateUser(id, logged_user, is_deactivate, deactivate_reason);
      if (response) {
        return res.status(200).json("User deactivate status changed.")
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async verifyUser(req, res, next) {
    try {
      let id = req.body.id;
      let is_verified = req.body.is_verified;
      let logged_user = req.user.user_type;
      console.log("type", logged_user)
      const response = await AdminService.verifyUser(id, logged_user, is_verified);
      if (response) {
        return res.status(200).json("User verified status changed.")
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async blockStaff(req, res, next) {
    try {
      console.log(1)
      let id = req.body.id;
      let unblock = req.body.unblock
      const response = await AdminService.blockUser(id, unblock);
      if (response) {
        console.log(3)
        return res.status(200).json({ message: 'Block status updated' })
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deleteWorker(req, res, next) {
    try {
      let id = req.body.id;
      let response = await AdminService.deleteWorker(id);
      if (response.affectedRows > 0) {
        return res.status(200).json({
          message: 'Worker Deleted.'
        })
      } else {
        return res.status(400).json({ 'error': "bad_request", 'error_description': "Worker does not exist" });
      }

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  // Content Management

  //add content
  static async addContent(req, res, next) {
    try {
      let title = req.body.title;
      let content = req.body.content;
      let image = req.body.image;
      console.log(req.body)
      const response = await AdminService.addContent(title, content, image);
      if (response) {
        return res.status(200).json(response);
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Get Contents
  static async getContents(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const contents = await AdminService.getContents(keyword, limit, page);
      return res.status(200).json(contents)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Get Content Details
  static async getContentDetail(req, res, next) {
    try {
      let id = req.query.content_id
      const content = await AdminService.getContentDetail(id);
      return res.status(200).json(content[0])
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Edit content
  static async editContent(req, res, next) {
    try {
      let id = req.body.id;
      let title = req.body.title;
      let content = req.body.content;
      let image = req.body.image;
      await AdminService.editContent(id, title, content, image);
      return res.status(200).json({
        message: 'content Updated.'
      })

    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Delete content
  static async deleteContent(req, res, next) {
    try {
      let content_id = req.body.id;
      await AdminService.deleteContent(content_id);
      return res.status(200).json({
        message: 'content Deleted.'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Delete content
  static async deleteContact(req, res, next) {
    try {
      let contact_id = req.body.id;
      var response = await AdminService.deleteContact(contact_id);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Send Mail
  static async sendNotification(req, res, next) {
    try {
      let email = req.body.emails;
      let subject = req.body.subject;
      let push_content = req.body.push_content;
      let email_content = req.body.email_content;
      let type = req.body.type;
      let notification_type = req.body.notification_type;
      if (type == 0 && notification_type == 0) {

        await AdminService.sendMail(null, subject, email_content);

      } else if (type == 1 && notification_type == 0) {

        await AdminService.sendMail(email, subject, email_content);

      } else if (type == 0 && notification_type == 1) {

        await AdminService.sendPush(null, subject, push_content);

      } else if (type == 1 && notification_type == 1) {

        await AdminService.sendPush(email, subject, push_content);

      } else if (type == 1 && (notification_type == null || notification_type == 2)) {

        await AdminService.sendPush(email, subject, push_content);
        await AdminService.sendMail(email, subject, email_content);

      } else if (type == 0 && (notification_type == null || notification_type == 2)) {

        await AdminService.sendPush(null, subject, push_content);
        await AdminService.sendMail(null, subject, email_content);

      }

      return res.status(200).json({
        message: 'Mail Sent Successfully'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //Get Contacts
  static async getContacts(req, res, next) {
    try {
      let keyword = req.query.keyword;
      let limit = req.query.limit;
      let page = req.query.page;
      const contacts = await AdminService.getContacts(keyword, limit, page);
      return res.status(200).json(contacts)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //DB Backup
  static async dbBackup(req, res, next) {
    try {
      const response = await AdminService.dbBackup();
      return res.status(200).json({ dbname: response })
      // if (response) {
      //   return res.status(200).json("Backup Successfully Completed.")
      // }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

}
module.exports = AdminController;