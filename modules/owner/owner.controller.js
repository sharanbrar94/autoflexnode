const { Console } = require("winston/lib/winston/transports");
const logger = require("../../config/logger"); // importiing winston logger module
const OwnerService = require("./owner.service");
class OwnerController {
    static async addWorker(req, res, next) {
        try {
            console.log(req.user);
            let id = req.user.id;
            const { profile_image, first_name, last_name, password, country_code, phone_number, username } = req.body;
            const response = await OwnerService.addWorker(id, profile_image, first_name, last_name, password, country_code, phone_number, username);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getWorkers(req, res, next) {
        try {
            const { id } = req.user;
            const { keyword, limit, page, is_blocked } = req.query;
            let users = await OwnerService.getWorkers(id, keyword, page, limit, is_blocked)
            return res.status(200).json(users)
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async updateUserProfile(req, res, next) {
        try {
            console.log(req.body);

            const {
                first_name,
                last_name,
                email,
                phone_number,
                country_code,
                address,
                city,
                state,
                country,
                zip,
                profile_image,
                lat,
                lng,
                country_abbr,
            } = req.body;
            const id = req.user.id;

            const data = await ProfileService.updateUserProfile(
                id,
                first_name,
                last_name,
                email,
                phone_number,
                country_code,
                address,
                city,
                state,
                country,
                zip,
                profile_image,
                lat,
                lng
            );

            if (data) {
                return res
                    .status(200)
                    .json({ message: "Profile updated successfully." });
            }
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({ error: "bad_request", error_description: err });
        }
    }

    //Get Content Details
    static async getContentDetail(req, res, next) {
        try {
            let id = req.query.id;
            const content = await ProfileService.getContentDetail(id);
            return res.status(200).json(content[0])
        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //get worker detail
    static async getWorkerDetail(req, res, next) {
        try {
            const { id } = req.query;
            const project = await OwnerService.getWorkerDetail(id);
            return res.status(200).json(project)
        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Edit worker details
    static async editWorker(req, res, next) {
        try {
            const { id, first_name, last_name, phone_number, country_code, profile_image, is_blocked } = req.body;
            await OwnerService.editWorker(id, first_name, last_name, phone_number, country_code, profile_image, is_blocked);
            return res.status(200).json({
                message: 'Worker Updated'
            })
        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }


    //Edit account details
    static async editAccount(req, res, next) {
        try {
            const { id, bank_name, account_name, branch, account_no, iban_number, address, is_default } = req.body;
            await OwnerService.editAccount(id, bank_name, account_name, branch, account_no, iban_number, address, is_default);
            return res.status(200).json({
                message: 'Account Updated'
            })
        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //delete worker
    static async deleteWorker(req, res, next) {
        try {
            let id = req.body.id;
            let response = await OwnerService.deleteWorker(id);
            if (response.affectedRows > 0) {
                return res.status(200).json({
                    message: 'Worker Deleted.'
                })
            } else {
                return res.status(400).json({ 'error': "bad_request", 'error_description': "Worker does not exist" });
            }

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //delete Service
    static async deleteService(req, res, next) {
        try {
            let id = req.body.id;
            let response = await OwnerService.deleteService(id);
            if (response.affectedRows > 0) {
                return res.status(200).json({
                    message: 'Service Deleted.'
                })
            } else {
                return res.status(400).json({ 'error': "bad_request", 'error_description': "Service does not exist" });
            }

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Block Worker
    static async blockWorker(req, res, next) {
        try {
            const { id, is_blocked } = req.body;
            let response = await OwnerService.blockWorker(id, is_blocked);
            return res.status(200).json(response)
        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Create Service
    static async addService(req, res, next) {
        try {
            const { id } = req.user;
            const { service, location, schedule, timing, vehicle, description, is_powered, extras } = req.body
            const response = await OwnerService.addService(id, service, location, schedule, timing, vehicle, description, is_powered, extras);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Create Service
    static async addAccount(req, res, next) {
        try {
            const { id } = req.user;
            const { bank_name, account_name, branch, account_no,iban_number,address, is_default } = req.body
            const response = await OwnerService.addAccount(id, bank_name, account_name, branch, account_no,iban_number,address, is_default);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }


    //Get Orders
    static async getOrders(req, res, next) {
        try {
            const { id } = req.user;
            const { status, limit, page } = req.query;
            console.log("status", status)
            const response = await OwnerService.getOrders(id, status, limit, page);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Get Orders
    static async getAvailableWorkers(req, res, next) {
        try {
            const { id } = req.user;
            const { arrival_time, arrival_date, limit, page } = req.query;
            const response = await OwnerService.getAvailableWorkers(id, limit, page, arrival_time, arrival_date);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Get Accounts
    static async getAccounts(req, res, next) {
        try {
            const { id } = req.user;
            const { limit, page } = req.query;
            const response = await OwnerService.getAccounts(id, limit, page);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //get Order detail 
    static async getOrderDetail(req, res, next) {
        try {
            const { id } = req.user;
            const { id: order_id } = req.query;
            const order = await OwnerService.getOrder(order_id);
            const services = await OwnerService.getOrderDetails(order_id);
            order['services'] = services
            return res.status(200).json(order);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Get Service
    static async getService(req, res, next) {
        try {
            const { id: logged_id } = req.user;
            const { id: service_id } = req.query;
            const response = await OwnerService.getService(logged_id, service_id);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    // Get Dashboard
    static async getDashboard(req, res, next) {
        try {
            const { id: logged_id } = req.user;
            const { dashboard } ={}
            let company = await OwnerService.getCompanyById(logged_id);
            dashboard['new_orders'] = await OwnerService.getOrdersCount(company[0].id, 0);
            dashboard['assigned'] = await OwnerService.getOrdersCount(company[0].id, 1);
            dashboard['in_progess'] = await OwnerService.getOrdersCount(company[0].id, 2);
            dashboard['completed'] = await OwnerService.getOrdersCount(company[0].id, 3);
            dashboard['cancelled'] = await OwnerService.getOrdersCount(company[0].id,4);
            // const response = await OwnerService.get(company[0].id, 1, null, null);
            // const response = await OwnerService.getOrdersByCompanyId(company[0].id, 2, null, null);
            return res.status(200).json(dashboard);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Get Services
    static async getServices(req, res, next) {
        try {
            let id = req.user.id;
            const response = await OwnerService.getServices(id);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Edit Service Location
    static async editService(req, res, next) {
        try {

            const { id, editExtras, editDescription, editLocation, editSchedule, editTiming, editVehicle, is_powered, discount } = req.body
            if (editLocation) {
                await OwnerService.editServiceLocation(editLocation.emirate_id, editLocation.areas, id);
            }
            if (editExtras) {
                await OwnerService.editServiceExtras(id, editExtras);
            }
            if (editDescription) {
                await OwnerService.editServiceHighlights(id, editDescription.description, editDescription.highlights);
            }
            if (editSchedule) {
                await OwnerService.editServiceSchedule(editSchedule.type, editSchedule.days, id);
            }
            if (editTiming) {
                await OwnerService.addServiceTiming(id, editTiming.service_time, editTiming.reach_time);
            }
            if (editVehicle) {
                await OwnerService.editServiceVehicle(id, editVehicle);
            }
            if (is_powered) {
                await OwnerService.editServicePowered(id, is_powered);
            }
            if (discount) {
                await OwnerService.editServiceDiscount(id, discount);
            }

            return res.status(200).json({ message: "Service Updated." });

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Block Days
    static async blockDays(req, res, next) {
        try {

            let id = req.user.id;
            const { days } = req.body;
            const response = await OwnerService.blockDays(id, days);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Change Order Status
    static async editOrderStatus(req, res, next) {
        try {
            let id = req.user.id;
            const { id: order_id, status } = req.body;
            const response = await OwnerService.editOrderStatus(id, order_id, status);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Assign Order
    static async assignOrder(req, res, next) {
        try {
            let id = req.user.id;
            const { id: order_id, worker_id } = req.body;
            const response = await OwnerService.assignOrder(id, order_id, worker_id);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //Block Days
    static async withdrawlRequest(req, res, next) {
        try {
            let id = req.user.id;
            const { amount } = req.body;
            const response = await OwnerService.withdrawlRequest(id, amount);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async getblockDays(req, res, next) {
        try {
            let id = req.user.id;
            const response = await OwnerService.getblockDays(id);
            return res.status(200).json(response);
        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    static async contactUs(req, res, next) {
        try {
            let user_id = req.user.id;
            const { message, phone_number, country_code, first_name, last_name, email } = req.body;
            await ProfileService.contactUs(user_id, message, phone_number, country_code, first_name, last_name, email);
            return res.status(200).json({ message: "Success" });
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({ error: "bad_request", error_description: err });
        }
    }
}

module.exports = OwnerController;
