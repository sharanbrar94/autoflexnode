var express = require('express');
var router = express.Router();
const valid = require('../../config/validator')
const OwnerController = require("./owner.controller");
let auth = require("../../middleware/auth")


// Worker management
router.post('/worker', auth.checkToken, OwnerController.addWorker);
router.get('/workers', auth.checkToken, OwnerController.getWorkers);
router.get('/worker', auth.checkToken, OwnerController.getWorkerDetail);
router.put('/worker', auth.checkToken, OwnerController.editWorker);
router.delete('/worker', auth.checkToken, OwnerController.deleteWorker);
router.put('/worker/block', auth.checkToken, OwnerController.blockWorker);

// Service management
router.post('/service', auth.checkToken, OwnerController.addService);
router.get('/service', auth.checkToken, OwnerController.getService);
router.get('/services', auth.checkToken, OwnerController.getServices);
router.put('/service', auth.checkToken, OwnerController.editService);
router.delete('/service', auth.checkToken, OwnerController.deleteService);

//Dashboard
router.get('/dashboard', auth.checkToken, OwnerController.getDashboard);

//Bank Account management
router.post('/account', auth.checkToken, OwnerController.addAccount);
router.get('/account', auth.checkToken, OwnerController.getAccounts);
router.put('/account', auth.checkToken, OwnerController.editAccount);
// router.put('/service', auth.checkToken, OwnerController.editService);
// router.delete('/service', auth.checkToken, OwnerController.deleteService);

//Orders Management
router.get('/order/listing',auth.checkToken, OwnerController.getOrders);
router.get('/order-detail',auth.checkToken, OwnerController.getOrderDetail);
router.post('/order/status',auth.checkToken, OwnerController.editOrderStatus);
router.post('order/assign',auth.checkToken,OwnerController.assignOrder);
router.get('/worker/available',auth.checkToken,OwnerController.getAvailableWorkers);

//Block Days
router.post('/days/block', auth.checkToken, OwnerController.blockDays);
router.get('/days', auth.checkToken, OwnerController.getblockDays);

//Withdrawl Requests

router.post('/withdrawl-request', auth.checkToken, OwnerController.withdrawlRequest);

module.exports = router;