const { sign } = require('jsonwebtoken');
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const Common = require('../../middleware/common')
const logger = require('../../config/logger');
class OwnerService {

    static async addWorker(id, profile_image, first_name, last_name, password, country_code, phone_number, username) {
        let user = await this.getUserByUsername(username)
        if (user.length > 0) {
            throw ("Username already exist!");
        }

        let company = await this.getCompanyById(id)
        console.log("company", company)
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        const response = await connection.query('insert into users set first_name=?,last_name=?,username=?,password=?, country_code=?, phone_number=?,user_type=?, company_id=?,profile_image=?', [first_name, last_name, company[0].name + "." + username, password, country_code, phone_number, 3, company[0].id, profile_image]);
        console.log(response)
        if (response) {
            return ({ id: response.insertId, message: "Worker added " })
        }
    }

    static async deleteWorker(id) {
        const response = await connection.query('delete from users where id = ?', [id]);
        return response
    }

    static async deleteService(id) {
        const response = await connection.query('delete from services where id = ?', [id]);
        return response
    }

    static async getWorkers(id, keyword, page, limit, is_blocked) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        const company = await this.getCompanyById(id);
        console.log(company)
        console.log("company id ", company[0].id)
        var limit = parseInt(lmt)
        let sqlQuery = `select * from users where users.user_type = ? and users.company_id = ? `
        if (keyword) {
            sqlQuery += ` and (users.first_name like  '%${keyword}%' or users.last_name like  '%${keyword}%'   or users.phone_number like  '%${keyword}%'  ) `
        }
        if (is_blocked) {
            sqlQuery += ` and users.is_blocked = ? `
        }
        sqlQuery += ` order by users.id desc LIMIT ${limit} OFFSET ${offset}`
        let user = await connection.query(sqlQuery, [3, company[0].id, is_blocked])

        let main_array = [];
        user.map(item => {
            delete item.password;
            main_array.push(item);
        })

        return { count: user.length, user }
    }

    static async editWorker(id, first_name, last_name, phone_number, country_code, profile_image, is_blocked) {
        let sqlQuery = 'update companies set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (phone_number != undefined) {
            params.push(phone_number)
            sql.push('phone_number=?')
        }

        if (country_code != undefined) {
            params.push(country_code)
            sql.push('country_code=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        if (is_blocked != undefined) {
            params.push(is_blocked)
            sql.push('is_blocked=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where user_id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        const response = await connection.query(sqlQuery, params)
        return response
    }

    static async editAccount(id, bank_name, account_name, branch, account_no, iban_number, address, is_default) {
        let sqlQuery = 'update user_accounts set '
        let params = []
        let sql = []
        if (bank_name != undefined) {
            params.push(bank_name)
            sql.push('bank=?')
        }
        if (account_name != undefined) {
            params.push(account_name)
            sql.push('account_name=?')
        }
        if (branch != undefined) {
            params.push(branch)
            sql.push('branch=?')
        }
        
        if (address != undefined) {
            params.push(address)
            sql.push('address=?')
        }
        if (account_no != undefined) {
            params.push(account_no)
            sql.push('account_no=?')
        }
        if (iban_number != undefined) {
            params.push(iban_number)
            sql.push('iban_number=?')
        } if (is_default != undefined) {
            if (is_default == 0) {
                var exist = await connection.query('select * from user_accounts where is_default= 1 and id = ? and user_id = ?', [id, logged_id])
                if (exist.length > 0) {
                    await connection.query('update user_accounts set is_default = 1 where id = (select MAX(id) from user_accounts where is_default= 0 and user_id = ?) ', [id, logged_id])
                    params.push(is_default)
                    sql.push('is_default=?')
                } else {
                    params.push(is_default)
                    sql.push('is_default=?')
                }
            }
            if (is_default == 1) {
                await connection.query('update user_accounts set is_default= 0 where id != ? and user_id = ?', [id, logged_id])
            }
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        const response = await connection.query(sqlQuery, params)
        return response
    }

    static async getWorkerDetail(id) {
        const workers = await connection.query('select u.*,c.* from users as u left join companies as c on u.id=c.user_id where u.id=?', [id]);
        return workers[0]
    }

    static async getUserByUsername(username) {
        let sqlQuery = `select * from users where username=? and user_type= 3`
        return await connection.query(sqlQuery, [username])
    }

    static async blockWorker(id, is_blocked) {
        let sqlQuery = 'update users set is_blocked=? where id=? '
        await connection.query(sqlQuery, [is_blocked, id])
        return is_blocked == 0 ? "Worker Unblocked" : "Worker Blocked"

    }

    static async getCompanyById(id) {
        let sqlQuery = `select c.*,(select Avg(rating) from reviews where reviews.company_id=c.id) as rating from users as u left join companies as c on c.user_id=u.id where u.id=? and u.user_type = 2`
        var response = await connection.query(sqlQuery, [id])
        return response
    }

    static async updateUserProfile(user_id, first_name, last_name, email, phone_number, country_code, address, city, state, country, zip, profile_image, lat, lng) {

        let query = []
        let param = []
        if (first_name) {
            query.push("first_name=?")
            param.push(first_name)
        }
        if (last_name) {
            query.push("last_name=?")
            param.push(last_name)
        }
        if (email) {
            query.push("email=?")
            param.push(email)
        }
        if (phone_number) {
            query.push("phone_number=?")
            param.push(phone_number)
        }
        if (country_code) {
            query.push("country_code=?")
            param.push(country_code)
        }
        if (address) {
            query.push("address=?")
            param.push(address)
        }
        if (city) {
            query.push("city=?")
            param.push(city)
        }
        if (state) {
            query.push("state=?")
            param.push(state)
        }
        if (country) {
            query.push("country=?")
            param.push(country)
        }
        if (zip) {
            query.push("zip=?")
            param.push(zip)
        }
        if (profile_image) {
            query.push("profile_image=?")
            param.push(profile_image)
        }
        if (lat) {
            query.push("lat=?")
            param.push(lat)
        }
        if (lng) {
            query.push("lng=?")
            param.push(lng)
        }

        if (country_abbr) {
            query.push("country_abbr=?")
            param.push(country_abbr)
        }


        param.push(user_id)

        let querystring = '';

        for (var i = 0; i < query.length; i++) {
            querystring = querystring + ' ' + query[i]
        }
        let querytrim = querystring.trim()

        let actualstring = querytrim.replace(/ /g, ",");


        let sqlQuery = 'update users set ' + actualstring + ' where id=?';
        console.log("query", query)
        console.log("param", param)
        console.log("string", actualstring)

        return await connection.query(sqlQuery, param)

    }

    static async getContentDetail(id) {
        return await connection.query('select * from static_pages where id=?', [id]);
    }

    static async changePassword(new_password, old_password, user_password, user_id) {
        let response = compareSync(old_password, user_password);
        console.log(response);
        if (!response) {
            throw ('Please enter correct old password!');
        } else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            let sqlQuery = `update users set password=? where id=?`
            let params = [password, user_id]
            var response = await connection.query(sqlQuery, params)
            console.log(response);
            return response;
        }
    }

    static async contactUs(user_id, message, phone_number, country_code, first_name, last_name, email) {
        // let subject = 'Contact Us'
        // const response = await Common.sendMail(email, message, subject);
        //console.log(response)
        return await connection.query('insert into contact_us set user_id=?, description=?, phone_number=?, country_code=?, first_name=?, last_name=?, email=?', [user_id, message, phone_number, country_code, first_name, last_name, email])


    }

    static async addService(id, service, location, schedule, timing, vehicle, description, is_powered, extras) {
        let company = await this.getCompanyById(id);
        console.log(company)
        let services = await this.getCompanyService(company[0].id, service.category_id, service.sub_category_id)
        console.log("services", services)
        if (services.length > 0) {
            throw ("Service already exist!");
        } else {
            var response = await this.addCompanyService(company[0].id, service.category_id, service.sub_category_id, service.price)
            var service_id = response.insertId
        }

        var location_response = await this.addServiceLocation(service_id, location.emirate_id, location.areas)
        console.log(location_response)
        var schedule_response = await this.addServiceSchedule(service_id, schedule.type, schedule.days)
        console.log(schedule_response)
        var service_time_response = await this.addServiceTiming(service_id, timing.service_time, timing.reach_time)
        console.log(service_time_response)
        var service_vehicles_response = await this.addServiceVehicles(service_id, vehicle)
        console.log(service_vehicles_response)
        var service_description_response = await this.addServiceDescription(service_id, description.description, description.highlights)
        console.log(service_description_response)
        var service_powered_response = await this.addServicePowered(service_id, is_powered)
        console.log(service_powered_response)
        var service_extras_response = await this.addServiceExtras(service_id, extras)
        console.log(service_extras_response)
        // var service_discount_response = await this.addServiceDiscount(service_id, discount)
        // console.log(service_discount_response)

        if (service_id) {
            return ({ id: service_id, message: "Service added" })
        }
    }

    static async getServices(id) {
        let company = await this.getCompanyById(id);
        if (company.length == 0) {
            var services = []
        } else {
            var services = await this.getServiceByCompanyId(company[0].id)
        }
        return services
    }

    static async getOrders(id, status, limit, page) {
        console.log(status)
        let company = await this.getCompanyById(id);
        let orders = await this.getOrdersByCompanyId(company[0].id, status, limit, page)
        return orders
    }


    static async getAvailableWorkers(id, limit, page, arrival_time, arrival_date,) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        let company = await this.getCompanyById(id);
        let response = await connection.query(`select * from users as u1 where u1.id not in (SELECT u.id FROM users as u left join orders as o on o.worker_id=u.id where o.arrival_time<? and arrival_date=? and o.status!=2 ) and u1.user_type=3 and u1.company_id=? limit ${limit} offset ${offset}`, [arrival_time, arrival_date, company[0].id])
        response.map(ele => {
            delete ele.password
        })
        return response
    }

    static async getAccounts(id, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        var accounts = await connection.query(`select * from users_accounts where user_id=? limit ${limit} offset ${offset}`, [id])

        return accounts
    }

    static async getOrdersByCompanyId(id, status, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        var services = await connection.query("select id from services where company_id=?", [id])

        var data = []
        for (var i in services) {
            data.push(services[i].id)
        }
        var query = "select o.id as order_id,o.*,uv.* from orders as o left join order_items as ot on ot.order_id=o.id left join user_vehicles as uv on uv.id=o.vehicle_id where ot.service_id in (?) and o.is_created=1"
        if (status) {
            query += ` and o.status = ${status} `
        }
        query += ` group by o.id LIMIT ${limit} OFFSET ${offset}`
        console.log("query", query)
        var orders = await connection.query(query, [data])

        var sqlQueryCount = "select count(o.id) as count,o.id as order_id,o.*,uv.* from orders as o left join order_items as ot on ot.order_id=o.id left join user_vehicles as uv on uv.id=o.vehicle_id where ot.service_id in (?) and o.is_created=1"
        if (status) {
            sqlQueryCount += ` and o.status = ${status} `
        }
        var counts = await connection.query(sqlQueryCount, [data])
       
        return {count:counts[0].count,orders}
    }

    static async getOrdersCount(id, status) {
    
        var services = await connection.query("select id from services where company_id=?", [id])

        var data = []
        for (var i in services) {
            data.push(services[i].id)
        }
        var query = "select o.id as order_id,o.*,uv.* from orders as o left join order_items as ot on ot.order_id=o.id left join user_vehicles as uv on uv.id=o.vehicle_id where ot.service_id in (?) and o.is_created=1"
        if (status) {
            query += ` and o.status = ${status} `
        }
        console.log("query", query)
        var orders = await connection.query(query, [data])

        return {count:orders.length}
    }


    static async getService(logged_id, id) {
        var company = await this.getServiceById(id, logged_id);
        if (!company) {
            throw ("Service doe not exist")
        } else {
            let service = {}
            service.service = company
            service.location = await this.getServiceLocationById(id);
            service.schedule = await this.getServiceScheduleById(id);
            service.extras = await this.getServiceExtrasById(id);
            service.description = await this.getServiceHighlightById(id);
            service.vehicle = await this.getServiceVehicleById(id);
            console.log(service)
            return service
        }
    }

    static async getOrderDetail(id) {
        var query = "Select * from orders as o left join users as u on u.id=o.user_id left join user_vehicles as u_v on u_v.id=o.vehicle_id left join user_addresses as u_a on u_a.id=o.address_id left join users as w on w.id=o.worker_id left join services as s on s.id= o.service_id left join categories as c on c.id=s.category_id left join sub_categories as s_c on s_c.id=s.sub_category_id left join companies as comp on comp.id=s.company_id where o.id =?"
        var orderDetail = await connection.query(query, [id])
        console.log("order", orderDetail)
        return orderDetail
    }

    static async getServiceById(id, logged_id) {
        let response = await connection.query('select *,(select Avg(rating) from reviews where reviews.company_id=companies.id) as rating  from services left join companies on companies.id=services.company_id where services.id=? and companies.user_id=?', [id, logged_id])
        return response[0]
    }

    static async addAccount(id, bank_name, account_name, branch, account_no,iban_number,address, is_default) {
        var accounts = await connection.query("select * from user_accounts where user_id=?", [id])
        if (accounts.length == 0) {
            is_default = 1
        } else {
            if (is_default == 1) {
                await connection.query('update user_accounts set is_default= 0 where user_id = ?', [id])
            } else {
                is_default = 0
            }
        }
        let response = await connection.query('insert into user_accounts( user_id,bank, account_name, branch, account_no, address, iban_number,is_default) values (?,?,?,?,?,?,?,?)', [id, bank_name, account_name, branch, account_no, address,iban_number,is_default])
        return response
    }

    static async getServiceLocationById(id) {
        let response = await connection.query('select loc.id,a.name as area_name,e.name as emirate_name from service_locations as loc left join areas as a on loc.area_id=a.id left join emirates as e on loc.emirate_id=e.id where loc.service_id=?', [id])
        return response
    }

    static async getServiceScheduleById(id) {
        let response = await connection.query('select * from service_business_hours where service_id=?', [id])

        let day = [];
        for (var i in response) {
            day.push(response[i].id)
        }
        let day_ids = [...new Set(day)];
        console.log(day_ids)
        let time_slot = await connection.query('select ds.service_day_id,ts.* from day_slots as ds left join time_slots as ts on ts.id=ds.time_slot_id where ds.service_day_id in (?)', [day_ids])
        response.map(a => {
            a.time_slot = []
            for (var i in time_slot) {
                if (a.id == time_slot[i].service_day_id) {
                    a.time_slot.push(time_slot[i])
                }
            }
        })
        console.log(response);
        return response
    }

    static async getServiceExtrasById(id) {
        let response = await connection.query('select * from service_extras where service_id=?', [id])
        return response
    }

    static async getServiceHighlightById(id) {
        let response = await connection.query('select * from service_highlights where service_id=?', [id])
        return response
    }

    static async getServiceVehicleById(id) {
        let response = await connection.query('select vehicle.id,vehicle.rate,types.name as vehicle_name, types.description as vehicle_description, types.image from service_vehicles as vehicle left join vehicle_types as types on vehicle.vehicle_type_id= types.id where service_id=?', [id])
        return response
    }

    static async userByID(user_id) {
        return await connection.query('select * from users where users.id=?', [user_id])
    }

    static async getCompanyById(id) {
        return await connection.query('select c.* from users as u left join companies as c on c.user_id= u.id where u.id=?', [id])
    }

    static async withdrawlRequest(id, amount) {
        // var query= "select * from orders as o left join services as s on o.service_id= s.id left join companies as c on c.id = s.company_id where c.user_id  = ? "
        // var response =  await connection.query(query, [id])

        // var balance_query = `select SUM(amount) from transactions where order_id IN ${response}`
        // var response =  await connection.query(balance_query)
        // console.log(response)

        // var request_query = `select SUM(amount) from withdrawl_requests where user_id = ? `
        // var response =  await connection.query(request_query,id)
        // console.log(response)

        var request_query = `insert into withdraw_requests (amount, user_id) values (?,?) `
        var response = await connection.query(request_query, [amount, id])
        console.log(response)
        return { "message": "Request successfully submitted" }
    }

    static async getOrder(order_id) {
        var order = await connection.query('select comp.name as company_name,cat.name as category_name,o.id as order_id,o.*,ua.*,if(u1.last_name is NOT null ,CONCAT(u1.first_name," ",u1.last_name),u1.first_name) as worker_name,u1.profile_image as worker_image,u1.username as worker_username,u1.phone_number as worker_phone , u1.country_code as worker_country_code,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as customer_name,u.email as customer_email, u.phone_number as customer_phone,u.country_code as customer_country_code,u.profile_image as customer_image from orders as o  left join users as u on u.id=o.user_id left join users as u1 on u1.id=o.worker_id left join companies as comp on comp.id=(select company_id from order_items as ot where order_id=o.id group by company_id) left join categories as cat on cat.id=(select category_id from order_items as ot where ot.order_id=o.id group by category_id) left join user_vehicles as u_v on u_v.id=o.vehicle_id left join user_addresses as u_a on u_a.id=o.address_id left join time_slots as t_s on t_s.id=o.arrival_time left join user_addresses as ua on ua.id=o.address_id where o.id = ? and o.is_created=1', [order_id])
        return order[0]
    }

    static async getOrderDetails(order_id) {
        var services = await connection.query('select * from order_items where order_id=?', [order_id])
        var service_ids = [];
        for (var i in services) {
            service_ids.push(services[i].service_id)
        }
        let service_id = [...new Set(service_ids)];
        console.log(service_id)
        var service = await connection.query('select s.id,c.name as category_name,sub.name as sub_category_name,comp.name as company_name,s.price,s.discount,s.service_time,s.reach_time,s.description,s.is_powered,s.is_blocked,s.created_at from services as s left join categories as c on c.id=s.category_id left join sub_categories as sub on sub.id =s.sub_category_id left join companies as comp on comp.id=s.company_id where s.id IN (?)', [service_id])

        for (var i in service) {
            var extras = await connection.query('select * from order_items where order_id =? and service_id=?', [order_id, service[i].id])
            var extra_ids = [];
            for (var i in extras) {
                extra_ids.push(extras[i].extra_service_id)
            }
            console.log("extra ids", extra_ids)
            if (extra_ids.length > 0) {
                var extra_services = await connection.query('select ot.service_price,ot.quantity,ot.price,se.name,se.description,se.is_multiple from service_extras as se left join order_items as ot on ot.extra_service_id = se.id where se.id IN (?) and ot.order_id=? and ot.service_id=?', [extra_ids,order_id, service[i].id])
                service[i].extra_services = extra_services
            } else {
                service[i].extra_services = []
            }
            var service_highlights = await connection.query('select * from service_highlights where service_id = ?', [service[i].id])
            service[i].highlights = service_highlights
        }
        return service
    }

    static async getServiceByCompanyId(id) {
        return await connection.query('select s.*,c.name as category_name,s_c.name as sub_category_name from services as s left join categories as c on s.category_id=c.id left join sub_categories as s_c on s.sub_category_id= s_c.id where s.company_id=? ', [id])
    }

    static async getCompanyService(id, category_id, sub_category_id) {
        return await connection.query('select * from services where category_id=? and sub_category_id=? and company_id=?', [category_id, sub_category_id, id])
    }

    static async addCompanyService(id, category_id, sub_category_id, price) {
        return await connection.query('insert into services set category_id=? , sub_category_id=?, price=?, company_id=?', [category_id, sub_category_id, price, id])
    }

    static async addServiceTiming(id, service_time, reach_time) {
        return await connection.query('update services set service_time=? , reach_time=? where id = ?', [service_time, reach_time, id])
    }

    static async addServiceLocation(id, emirate_id, areas) {
        var data = []
        for (var i in areas) {
            data.push([emirate_id, areas[i], id])
        }
        console.log(data)
        return await connection.query('insert into service_locations (emirate_id, area_id,service_id) values ?', [data])
    }

    static async addServiceVehicles(id, vehicle) {
        var data = []
        for (var i in vehicle) {
            data.push([vehicle[i].type, vehicle[i].price, id])
        }
        console.log(data)
        return await connection.query('insert into service_vehicles (vehicle_type_id, rate,service_id) values ?', [data])
    }

    static async addServiceSchedule(id, type, days) {
        var data = []
        if (type == 0) {
            var day = [1, 2, 3, 4, 5, 6, 7]
            var slot = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
            for (var i in day) {
                for (var j in slot) {
                    data.push([day[i], slot[j], 2, 0, id])
                }
            }
            return await connection.query('insert into service_business_hours (day,time_slot_id,worker,type,service_id) values ?', [data])
        } else if (type == 1) {
            for (var i in days) {
                var res = await connection.query('insert into service_business_hours (day,worker,type,service_id) values (?,?,?,?)', [days[i].day, days[i].worker, type, id])
                for (var j in days[i].time_slot) {
                    data.push([res.insertId, days[i].time_slot[j]])
                }
                await connection.query('insert into day_slots (service_day_id,time_slot_id) values ? ', [data])
            }
            console.log(data)
            return
        }
    }

    static async addServiceDescription(id, description, hightlights) {
        await connection.query('update services set description=? where id=?', [description, id])
        var data = [];
        for (var i in hightlights) {
            data.push([hightlights[i].type, hightlights[i].description, id])
        }
        console.log(data)
        return await connection.query('insert into service_highlights (type , description, service_id) values ?', [data])
    }

    static async addServiceExtras(id, extras) {
        var data = [];
        for (var i in extras) {
            data.push([extras[i].name, extras[i].description, extras[i].rate, extras[i].is_multiple, id])
        }
        console.log(data)
        return await connection.query('insert into service_extras (name , description, rate,is_multiple, service_id) values ?', [data])
    }

    static async addServicePowered(id, is_powered) {
        return await connection.query('update services set is_powered=? where id=?', [is_powered, id])
    }

    static async addServiceDiscount(id, discount) {
        return await connection.query('update services set discount=? where id=?', [discount, id])
    }

    static async editServiceLocation(emirate_id, areas, id) {
        var delete_query = "delete from service_locations where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceLocation(id, emirate_id, areas)
    }

    static async editServiceSchedule(type, days, id) {
        var delete_query = "delete from service_business_hours where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceSchedule(id, type, days)
    }

    static async editServiceExtras(id, extras) {
        var delete_query = "delete from service_extras where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceExtras(id, extras)
    }

    static async editServiceVehicle(id, vehicles) {
        var delete_query = "delete from service_vehicles where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceVehicles(id, vehicles)
    }

    static async editServicePowered(id, id_powered) {
        var update_query = "update services set is_powered=? where id =?"
        await connection.query(update_query, [id_powered, id])
    }

    static async editServiceDiscount(id, discount) {
        var update_query = "update services set discount=? where id =?"
        await connection.query(update_query, [discount, id])
    }

    static async editServiceHighlights(id, description, highlights) {
        var delete_query = "delete from service_highlights where service_id =?"
        await connection.query(delete_query, [id])
        return await this.addServiceDescription(id, description, highlights)
    }

    static async blockDays(id, days) {
        let company = await this.getCompanyById(id)
        const dates = await connection.query('select * from block_days where company_id=?', [company[0].id])
        var date = []
        for (var i in dates) {
            date.push([dates[i].id])
        }
        console.log(date)
        if (date.length > 0) {
            var response = await connection.query('delete from block_days where id in ?', [date])
            console.log(response)
        }
        var data = []
        for (var i in days) {
            data.push([days[i].date, company[0].id])
        }
        console.log(data)
        await connection.query('insert into block_days (date,company_id) values ?', [data])
        return "Blocked Days Added"
    }

    static async getblockDays(id) {
        let company = await this.getCompanyById(id)
        return await connection.query('select * from block_days where company_id=?', [company[0].id])
    }

    static async editOrderStatus(id, order_id, status) {
        return await connection.query('update orders set status =? where id =? and user_id=? and is_created=1', [status, order_id, id])
    }

    static async assignOrder(id, order_id, worker_id) {
        return await connection.query('update orders set worker_id =? , status= 1  where id =? and user_id=? and is_created=1', [worker_id, order_id, id])
    }

}

module.exports = OwnerService;