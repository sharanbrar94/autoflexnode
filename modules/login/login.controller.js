require('dotenv').config();
const logger = require('../../config/logger'); // importiing winston logger module
const jwt = require('jsonwebtoken')
const LoginService = require("./login.service");
const { phone } = require('faker');

class LoginController {
  static async signup(req, res, next) {

    try {
      let inputs = req.body;
      let lang = req.body.lang ? req.body.lang : "en";
      const { first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type, fcm_id, device_id, device_type, company_name, trade_license, passport, emirate_id } = inputs
      const result = await LoginService.signup(first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type, device_id, fcm_id, device_type, company_name, trade_license, passport, emirate_id, lang)
      return res.status(200).json(result)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      console.log(err)
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async getUserProfile(req, res, next) {
    try {
      let user_id = req.user.id;
      let user = await LoginService.getUserProfile(user_id)
      delete user[0].password;
      return res.status(200).json(user[0])
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async login(req, res, next) {
    try {
      let inputs = req.body;
      let user = await LoginService.login(inputs);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async forgotPassword(req, res, next) {
    try {
      let email = req.body.email;
      const result = await LoginService.forgotPassword(email);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async resetPassword(req, res, next) {
    try {
      let token = req.body.token;
      let password = req.body.password;
      let result = await LoginService.resetPassword(token, password)
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async resetNotification(req, res, next) {
    try {
      let id = req.user.id;
      let email = req.body.is_email_notified;
      let push = req.body.is_push_notified;
      let sms = req.body.is_sms_notified;
      let result = await LoginService.resetNotification(id, email, push, sms)
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async verifyEmail(req, res, next) {
    try {
      let token = req.body.token;
      let result = await LoginService.verifyEmail(token);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async verifyPhone(req, res, next) {
    try {
      const { code, phone_number, country_code } = req.body;
      let id = req.user.id;
      let result = await LoginService.verifyPhone(code, phone_number, country_code, id);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async resendEmail(req, res, next) {

    try {
      let user_id = req.user.id;
      let user = await LoginService.getUserProfile(user_id)
      let email = user[0].email;
      let result = await LoginService.resendEmail(email);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async resendPhone(req, res, next) {
    try {
      let user_id = req.user.id;
      let user = await LoginService.getUserProfile(user_id)
      let phone = user[0].phone_number;
      let country_code = user[0].country_code;
      let result = await LoginService.resendPhone(phone, country_code);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async changePassword(req, res, next) {
    try {
      let user_id = req.user.id;
      let old_password = req.body.old_password;
      let new_password = req.body.new_password;
      await LoginService.changePassword(new_password, old_password, req.user.password, user_id)
      return { message: "Password Changed." }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async logout(req, res, next) {
    try {
      let device_id = req.body.device_id;
      await LoginService.logout(req.user.id, device_id);
      return res.status(200).json({ message: "You are logged out!" })
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async updateUserProfile(req, res, next) {
    try {
      let logged_id = req.user.id;
      let type = req.user.user_type;
      const { first_name, last_name, email, phone_number, country_code, profile_image, whatsapp_number, whatsapp_country_code, company_name, trade_license, emirate_id, passport } = req.body

      const result = await LoginService.updateUserProfile(logged_id, first_name, last_name, email, phone_number, country_code, profile_image, whatsapp_number, whatsapp_country_code, type, company_name, trade_license, emirate_id, passport);
      if (result) {
        return res.status(200).json({ message: "Customer profile updated" });
      }
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  static async deactivateAccount(req, res, next) {
    try {
      let deactivate_reason = req.body.deactivate_reason;
      let password = req.body.password;
      let user_id = req.user.id
      let user_pass = req.user.password
      let result = await LoginService.deactivateAccount(deactivate_reason, password, user_pass, user_id);
      return res.status(200).json(result);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
}


// static async login(req, res, next) {
//   try {
//     let inputs = req.body;
//     let user = await LoginService.login(inputs);
//     return res.status(200).json(user)
//   } catch (err) {
//     logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
//     return res.status(400).json({ 'error': "bad_request", 'error_description': err });
//   }
// }
module.exports = LoginController;
































































































