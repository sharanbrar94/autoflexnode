var express = require('express');
const { checkSchema } = require('express-validator');
let userSchema = require("./login.validation.json")
var router = express.Router();
const valid = require('../../config/validator')
const LoginController = require("./login.controller");

let auth = require("../../middleware/auth")
router.post('/signup', auth.checkOptionalToken, checkSchema(userSchema.signup), valid, LoginController.signup);
router.post('/login', LoginController.login);
router.get('/profile', auth.checkToken, LoginController.getUserProfile);
router.put('/profile', auth.checkToken, LoginController.updateUserProfile);
router.post('/verify/email',auth.checkOptionalToken, checkSchema(userSchema.verifyEmail), valid, LoginController.verifyEmail);
router.post('/resend/verification-email', auth.checkToken, LoginController.resendEmail);
router.post('/resend/verification-Phone', auth.checkToken, LoginController.resendPhone);
router.post('/verify/phone', auth.checkToken, LoginController.verifyPhone);
router.post('/forgot/password', auth.checkOptionalToken, checkSchema(userSchema.forgotPassword), valid, LoginController.forgotPassword);
router.post('/reset/password', auth.checkOptionalToken, checkSchema(userSchema.resetPassword), valid, LoginController.resetPassword);
router.post('/reset/notifications', auth.checkToken, LoginController.resetNotification);
// router.put('/change/password', auth.checkToken, checkSchema(userSchema.changePassword), valid, LoginController.changePassword);
router.post('/logout', auth.checkToken, checkSchema(userSchema.logout), valid, LoginController.logout);
router.post('/deactivate/account', auth.checkToken, checkSchema(userSchema.deactivateAccount), valid, LoginController.deactivateAccount)

module.exports = router;