const { sign } = require('jsonwebtoken');
const nodemailer = require('nodemailer')
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
// const client = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN)
const logger = require('../../config/logger');
let Common = require("../../middleware/common");
const { cloneDeep } = require('lodash');
class LoginService {
    static async signup(first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type, device_id, fcm_id, device_type, company_name, trade_license, passport, emirate_id, lang) {
        let user = await this.getUserProfileByEmail(email, type);
        console.log(user)
        if (user[0]) {
            // if (lang == "en") {
            throw ("Email already exist!");
            // }
            // if (lang == "ar") {
            //     throw ('البريد الالكتروني موجود مسبقا!');
            // }
        }
        let user_phone = await this.getUserProfileByPhone(phone_number, country_code);
        if (user_phone[0]) {
            // if (lang == "en") {
            throw ("Phone number already exist!");
            // } if (lang == "ar") {
            //     throw ("رقم الهاتف موجود بالفعل");
            // }
        }
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        if (whatsapp_number == undefined) {
            whatsapp_number = phone_number
            whatsapp_country_code = country_code
        }
        const email_verify_token = await Common.generateRandomString(15);
        // const phone_verification_code = await Common.generateRandomNumber(6);
        const phone_verification_code = 111111;
        var sqlQuery = 'insert into users(first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, user_type) values (?,?,?,?,?,?,?,?,?)';
        let params = [first_name, last_name, email, password, country_code, phone_number, whatsapp_number, whatsapp_country_code, type]
        let userInsert = await connection.query(sqlQuery, params);
        if (type == 2) {
            var Query = "insert into companies(company_name, trade_license, passport, emirate_id , user_id ) values (?,?,?,?,?) "
            let params = [company_name, trade_license, passport, emirate_id, userInsert.insertId]
            await connection.query(Query, params);

        }

        user = await this.getUserProfile(userInsert.insertId)
        let token = await this.generateToken(user);
        await this.insertOnUpdateDeviceInfo(device_id, fcm_id, device_type, userInsert.insertId)
        let email_subject = 'Email Verification'
        let text = "Verify your email by clicking on the below link."
        let link_text = "Verify Email"
        let url = `https://app-transfer.com:3008/verify-email/${email_verify_token}`
        // await Common.sendMail(email, url, email_subject, text, link_text);
        // if (phone_number && user[0].is_sms_notified == 1) {
        let message = "Your verification code is " + phone_verification_code
        let send_msg = await Common.sendMessage(phone_number, message)
        console.log(send_msg);
        // }
        delete user[0].password
        delete user[0].email_verify_token
        return ({ 'message': "User login successfully", token: token, user: user[0] })

    }

    static async getUserProfile(id) {
        console.log("user profile ")
        let sqlQuery = `select * from users where id=${id}`
        return await connection.query(sqlQuery)

    }

    static async generateToken(user) {
        let token = await sign({ user: user[0] }, 'secretkey', { expiresIn: "2 Days" });
        return token

    }

    static async getUserProfileByEmail(email, type) {
        let sqlQuery = `select * from users where email=? and user_type=?`
        return await connection.query(sqlQuery, [email, type])
    }

    static async getUserProfileByUsername(username, type) {
        let sqlQuery = `select * from users where username=? and user_type=?`
        return await connection.query(sqlQuery, [username, type])

    }

    static async getUserProfileByPhone(phone_no, country_code) {
        let sqlQuery = `select * from users where phone_number=? and country_code=?`
        return await connection.query(sqlQuery, [phone_no, country_code])

    }

    static async getUserByToken(token) {
        let sqlQuery = `select * from users where email_verify_token=?`
        return await connection.query(sqlQuery, [token])

    }

    static async getUserByCode(code, id) {

        let sqlQuery = `select * from users where phone_verify_code=? and id=?`
        return await connection.query(sqlQuery, [code, id])

    }

    static async getUserProfileByToken(token) {
        let sqlQuery = `select * from users where reset_pass_token=?`
        return await connection.query(sqlQuery, [token])

    }

    static async resetpass(email, password) {
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        let sqlQuery = 'update users set password=? where email=?'
        let params = [password, email]
        let sql = 'update users set reset_pass_token=null where email=?'
        let param = [email]
        await connection.query(sqlQuery, params);
        return await connection.query(sql, param);

    }

    static async updateEmailVerifyToken(email) {
        let sqlQuery = 'update users set email_verify_token=null, is_email_verified = 1 where email=?'
        let params = [email]
        return await connection.query(sqlQuery, params);

    }

    static async updateEmailResendToken(token, email) {
        let sqlQuery = 'update users set email_verify_token=?, is_email_verified = 0 where email=?'
        let params = [token, email]
        return await connection.query(sqlQuery, params);

    }

    static async updatePhoneResendCode(token, phone, country_code) {
        let sqlQuery = 'update users set phone_verify_code=?, is_phone_verified = 0 where phone_number=? and country_code =?'
        let params = [token, phone, country_code]
        return await connection.query(sqlQuery, params);
    }

    static async updatePhoneVerifyCode(Phone_no, country_code) {
        let sqlQuery = 'update users set phone_verify_code=null, is_phone_verified = 1 where phone_number=? and country_code=?'
        let params = [Phone_no, country_code]
        return await connection.query(sqlQuery, params);

    }

    static async insertOnUpdateDeviceInfo(device_id, fcm_id, device_type, user_id) {
        let expired_at = null;
        let sqlQuery = `insert into user_logins(user_id,device_id,fcm_id,device_type,expired_at) values(?,?,?,?,?) ON DUPLICATE KEY UPDATE user_id=?,fcm_id=?,device_type=?,expired_at=?`
        let params = [user_id, device_id, fcm_id, device_type, expired_at, user_id, fcm_id, device_type, expired_at]
        return await connection.query(sqlQuery, params)

    }

    // static async changePassword(new_password, old_password, user_password, user_id) {
    //     let result = compareSync(old_password, user_password);
    //     if (!result) {
    //        throw ('Please enter correct old password!');
    //     } else {
    //         const salt = genSaltSync(10);
    //         let password = hashSync(new_password, salt);
    //         let sqlQuery = `update users set password=? where id=?`
    //         let params = [password, user_id]
    //         return await connection.query(sqlQuery, params)
    //     }
    // }

    static async logout(user_id, device_id) {
        console.log(user_id);
        let date = new Date();
        let sqlQuery = 'update user_logins set expired_at=? where device_id=?'
        let params = [date, device_id]
        return await connection.query(sqlQuery, params)

    }

    static async updateUserProfile(id, first_name, last_name, email, phone_number, country_code, profile_image, whatsapp_number, whatsapp_country_code, type, company_name, trade_license, emirate_id, passport) {
        console.log("type", type)
        let sqlQuery = 'update users set '
        let params = []
        let sql = []
        if (first_name != undefined) {
            params.push(first_name)
            sql.push('first_name=?')
        }
        if (last_name != undefined) {
            params.push(last_name)
            sql.push('last_name=?')
        }
        if (email != undefined) {
            params.push(email)
            sql.push('email=?')
        }
        if (phone_number != undefined) {
            params.push(phone_number)
            sql.push('phone_number=?, is_phone_verified = 0, phone_verify_code = 111111')
        }
        if (country_code != undefined) {
            params.push(country_code)
            sql.push('country_code=?, is_phone_verified = 0, phone_verify_code = 111111')
        }

        if (whatsapp_number != undefined) {
            params.push(whatsapp_number)
            sql.push('whatsapp_number=?')
        }
        if (whatsapp_country_code != undefined) {
            params.push(whatsapp_country_code)
            sql.push('whatsapp_country_code=?')
        }
        if (profile_image != undefined) {
            params.push(profile_image)
            sql.push('profile_image=?')
        }
        sqlQuery += sql.toString()
        sqlQuery += ' where id=?'
        params.push(id)
        console.log(sqlQuery);
        console.log(params);
        if (type == 2 && (trade_license || company_name || emirate_id || passport)) {
            let sqlQuery = 'update companies set '
            let params = []
            let sql = []
            if (trade_license != undefined) {
                params.push(trade_license)
                sql.push('trade_license=? , is_trade_license_verified = 0')
            }
            if (company_name != undefined) {
                params.push(company_name)
                sql.push('company_name=?')
            }
            if (passport != undefined) {
                params.push(passport)
                sql.push('passport=? , is_passport_verified = 0')
            }
            if (emirate_id != undefined) {
                params.push(emirate_id)
                sql.push('emirate_id=?, is_emirate_id_verified = 0')
            }
            sqlQuery += sql.toString()
            sqlQuery += ' where user_id=?'
            params.push(id)
            console.log(sqlQuery);
            console.log(params);
            await connection.query(sqlQuery, params)
        }
        return await connection.query(sqlQuery, params)

    }

    static async updateResetPasswordToken(email, token) {
        console.log("token");
        let sqlQuery = 'update users set reset_pass_token=? where email=?'
        let params = [token, email]
        return await connection.query(sqlQuery, params)

    }

    static async getUser(param, param_name) {
        let sqlQuery = `select * from users where ${param_name}=?`
        return await connection.query(sqlQuery, [param])

    }

    static async socialLogin(fb_id, google_id, apple_id) {

        // const salt = genSaltSync(10);
        // password = hashSync(password, salt);
        var sqlQuery = 'insert into users(fb_id, google_id, apple_id) values (?,?,?)';
        let params = [fb_id, google_id, apple_id]
        return await connection.query(sqlQuery, params)

    }

    static async forgotPassword(email) {
        let user = await this.getUserProfileByEmail(email);
        if (user.length > 0) {
            console.log("user");
            let token = Date.now()
            let pass = await this.updateResetPasswordToken(email, token)
            //  console.log(pass);
            let subject = 'Reset Password'
            let text = 'You can reset you password by clicking on this link:'
            let url = `https://app-transfer.com:3008/reset-password/${token}`
            let link_text = 'Reset Password here.'
            await Common.sendMail(email, url, subject, text, link_text);

            return { message: "Email Sent!" }
        } else {
            throw ("Email does not exist");
        }

    }

    static async resetPassword(token, password) {
        // const salt = genSaltSync(10);
        // password = hashSync(password, salt);
        let user = await this.getUserProfileByToken(token);
        if (user.length > 0) {
            await this.resetpass(user[0].email, password)
            return ({ message: "Password changed!" })
        } else {
            throw ('Invalid token');
        }

    }

    static async resetNotification(id, email, push, sms) {
        if (email) {
            await this.updateEmailNotification(id, email)
        }
        if (push) {
            await this.updatePushNotification(id, push)
        }
        if (sms) {
            await this.updateSmsNotification(id, sms)
        }

        return { message: "Notification status updated." }

    }

    static async updateEmailNotification(id, email) {
        if (email == 0) {
            let sqlQuery = 'update users set is_email_notified=0 where id=?'
            let params = [id]
            return await connection.query(sqlQuery, params)
        } else {
            let sqlQuery = 'update users set is_email_notified=1 where id=?'
            let params = [id]
            return await connection.query(sqlQuery, params)
        }

    }

    static async updatePushNotification(id, push) {
        if (push == 0) {
            let sqlQuery = 'update users set is_push_notified=0 where id=?'
            let params = [id]
            return await connection.query(sqlQuery, params)
        } else {
            let sqlQuery = 'update users set is_push_notified=1 where id=?'
            let params = [id]
            return await connection.query(sqlQuery, params)
        }

    }

    static async updateSmsNotification(id, sms) {
        if (sms == 0) {
            let sqlQuery = 'update users set is_sms_notified=0 where id=?'
            let params = [id]
            return await connection.query(sqlQuery, params)
        } else {
            let sqlQuery = 'update users set is_sms_notified=1 where id=?'
            let params = [id]
            return await connection.query(sqlQuery, params)
        }


    }

    static async verifyEmail(token) {
        let user = await this.getUserByToken(token);
        if (user.length > 0) {
            let email = user[0].email;
            await this.updateEmailVerifyToken(email);
            return { message: 'Email verified successfully' }
        } else {
            throw ("Invalid token");
        }
    }

    static async verifyPhone(code, phone_number, country_code, id) {
        client
            .verify
            .services(process.env.TWILIO_SERVICE_SID)
            .verificationChecks
            .create({
                to: `${country_code}${phone_number}`,
                code: code
            })
            .then(async (data) => {
                console.log(data)
                await this.updatePhoneVerifyCode(phone_number, country_code);
                return callback(null, { message: 'Phone verified successfully' })
            })

    }

    static async resendEmail(email) {
        let user = await this.getUserProfileByEmail(email);
        if (user.length > 0) {
            const email_verify_token = await Common.generateRandomString(15);
            await this.updateEmailResendToken(email_verify_token, email)
            let subject = 'Email Verification'
            let url = `https://app-transfer.com:3008/verify-email/${email_verify_token}`
            let text = "To verify you email, please click on the link below."
            let link_text = "Verify your email here"
            await Common.sendMail(email, url, subject, text, link_text);
            return { message: 'Email sent successfully' }
        } else {
            throw ("User does not exist.");
        }
    }

    static async resendPhone(phone, country_code) {
        let user = await this.getUserProfileByPhone(phone, country_code);
        if (user.length > 0) {
            // let phone_verification_code = await Common.generateRandomNumber(6);
            // let phone_verification_code = "111111"
            // await this.updatePhoneResendCode(phone_verification_code, phone, country_code)
            // if(user.is_sms_notified == 1){
            // let message = "Your verification code is " + phone_verification_code
            // let send_msg = await Common.sendMessage(phone_number, message)
            client
                .verify
                .services(process.env.TWILIO_SERVICE_SID)
                .verifications
                .create({
                    to: `${country_code}${phone}`,
                    channel: 'sms'
                })
                .then((data) => {
                    return callback(null, data)
                })

            // }
        } else {
            throw ("User does not exist.");
        }
    }

    static async deactivateAccount(deactivate_reason, password, user_pass, user_id) {
        if (password) {
            let passwordResult = compareSync(password, user_pass);
            if (passwordResult == false) {
                throw ("Please enter correct password");
            } else {
                let sqlQuery = 'update users set deactivate_reason=?,is_deactivated=1 where id=?'
                let params = [deactivate_reason, user_id]
                await connection.query(sqlQuery, params)
                return ({ id: user_id, message: "Account deactivated successfully!" })
            }
        }
    }

    static async login(inputs) {

        let user;
        if (inputs.type == 3) {
            if (inputs.username) {
                user = await this.getUser(inputs.username, "username");
                console.log(user)
            } else {
                throw ("Please enter your username.");
            }

            if (user[0]) {
                console.log("worker login");
                console.log(user[0].password);
                if (inputs.password) {
                    console.log(inputs.password);
                    let passwordResult = compareSync(inputs.password, user[0].password);
                    console.log(passwordResult);
                    if (!passwordResult) {
                        throw ("Please enter correct password");
                    }
                } else {
                    throw ("Please enter password");
                }
            } else {
                throw ("User does not exist.");
            }
            user = await this.getUserProfileByUsername(inputs.username, inputs.type);
            if (user.length == 0) {
                throw ("User does not exist.")
            }
        } else {

            if (inputs.email) {
                console.log("email");
                user = await this.getUser(inputs.email, "email");
                console.log(user)

            } else {
                throw ("email is required.");
            }
            if (!inputs.password) {
                if (inputs.fb_id) {
                    user = await this.getUser(inputs.fb_id, "fb_id");

                }
                if (inputs.google_id) {
                    user = await this.getUser(inputs.google_id, "google_id");

                }
                if (inputs.apple_id) {
                    user = await this.getUser(inputs.apple_id, "apple_id");
                }
            }
            if (user[0]) {
                console.log("login");
                console.log(user[0].password);
                if (inputs.password) {
                    console.log(inputs.password);
                    let passwordResult = compareSync(inputs.password, user[0].password);
                    console.log(passwordResult);
                    if (!passwordResult) {
                        throw ("Please enter correct password");
                    }
                }
            } else {
                if (inputs.fb_id || inputs.google_id || inputs.apple_id) {
                    console.log("social_signin");
                    let email = inputs.email;
                    let password = inputs.password ? inputs.password : null;
                    let fb_id = inputs.fb_id ? inputs.fb_id : null;
                    let google_id = inputs.google_id ? inputs.google_id : null
                    let apple_id = inputs.apple_id ? inputs.apple_id : null
                    await LoginService.socialLogin(fb_id, google_id, apple_id);

                }
                else {
                    throw ("Please enter correct email");
                }
            }
            user = await this.getUserProfileByEmail(inputs.email, inputs.type);
            if (user.length == 0) {
                throw ("User does not exist.")
            }
        }
        let user_id = user[0].id;
        await this.insertOnUpdateDeviceInfo(inputs.device_id, inputs.fcm_id, inputs.device_type, user_id)
        await this.generateToken(user);
        let auth_token = await this.generateToken(user);
        delete user[0].password;
        return ({ 'message': "User login successfully", 'token': auth_token, 'user': user[0] });
    }

}
module.exports = LoginService;













































