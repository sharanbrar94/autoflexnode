
const { response } = require("express");
let Common = require("../../middleware/common")

class ChatService {
  static async userStatus(status, user_id) {
    const query = `UPDATE users SET status = ?, last_seen_at = now() WHERE id = ?`;
    return await connection.query(query, [status, user_id]);
  }

  static async saveMessage(user_data) {

    const query = `INSERT  INTO message (user_to, user_from, message,media,conversation_id) values (?,?,?,?,?)`;
    return await connection.query(query, [user_data.user_to, user_data.user_from, user_data.message, user_data.media, user_data.conversation_id]);
  }

  static async checkBlockedUser(user_to, user_from) {
    const query = `select * from block_user where (blocked_user=? and blocked_by=?) OR (blocked_by=? and blocked_user=?)`;
    return await connection.query(query, [user_to, user_from, user_to, user_from]);
  }

  static async getUserProfile(id) {
    let sqlQuery = `select * from users where id=${id}`
    return await connection.query(sqlQuery)
  }

  static async deleteMessage(deleted_for_me, user, id) {
    let query;

    if (deleted_for_me == 1) {
      query = `UPDATE message SET deleted_by = ? WHERE  (user_from = ? OR user_to=?) AND id = ?  `;
      return await connection.query(query, [user, user, user, id]);
    } else {
      query = `DELETE from message WHERE user_from = ? AND id = ?`;
      return await connection.query(query, [user, id]);
    }
  }

  static async deleteConversation(user, user_to) {
    let query = `select * from message  where (user_from = ? and user_to =?) OR (user_from = ? and user_to =?) `;
    var response = await connection.query(query, [user, user_to, user_to, user]);
    console.log(response)
    var deleted_msgs = []
    var msgs = []
    response.filter((ele) => {
      if (ele.deleted_by != user) {
        deleted_msgs.push(ele.id)
      } else if (ele.deleted_by == null || ele.deleted_by == user) {
        msgs.push(ele.id)
      }
    })

    if (deleted_msgs.length > 0) {
      let delete_query = "delete from message where id IN (?)"
      var del_response = await connection.query(delete_query, [deleted_msgs]);
      console.log("delete response", del_response)
    }
    if (msgs.length > 0) {

      let update_query = "update message set deleted_by = ? where id IN (?)"
      var up_response = await connection.query(update_query, [user, msgs]);
      console.log("delete response", up_response)
    }
    return response
    // if (deleted_for_me == 1) {
    //   query = `UPDATE message SET deleted_for_me = ? WHERE  (user_from = ? OR user_to=?) AND id = ?  `;
    //   return await connection.query(query, [deleted_for_me, user, user, id]);
    // } else {
    //   query = `DELETE from message WHERE user_from = ? AND id = ?`;
    //   return await connection.query(query, [user, id]);
    // }
  }

  static async messageSeen(is_read, user_from, user_to) {
    const query = `UPDATE message SET is_read = ? WHERE  user_from = ? AND user_to = ?`;
    return await connection.query(query, [
      parseInt(is_read),
      user_from,
      user_to,
    ]);
  }

  static async getUserConversation(user_from, user_to, page, limit, keyword) {
    var offset = 0
    if (page && limit) {
      var offset = ((page < 1 ? 1 : page) - 1) * limit;
    } else if (limit) {
      var limit = limit
    } else {
      var limit = 20
    }
    // var query = `SELECT m.*,(CASE WHEN user_from like ${user_from} THEN IF(u2.last_name is null,u2.first_name,CONCAT(u2.first_name , ' ', u2.last_name ))
    // ELSE IF(u1.last_name is null,u1.first_name,CONCAT(u1.first_name , ' ', u1.last_name ))
    // END )AS participant,IF(m.user_from like ${user_from},1, 0) as from_me
    //                 FROM message m
    //                     JOIN users u1 ON m.user_from=u1.id
    //                 JOIN users u2 ON m.user_to=u2.id
    //                     WHERE  m.id IN (
    //                 SELECT MAX(id)
    //                     FROM message
    //                 WHERE  ( user_from = ? AND deleted_for_me = 0 ) OR user_to = ? 
    //                     GROUP BY conversation_id ) AND (deleted_by is NULL OR deleted_by NOT LIKE ${user_from})`
    var query = `select m.* , u.first_name,u.last_name,u.profile_image,u.email,u.phone_number,u.country_code,u.description,u.status,u.last_seen_at,IF(u.last_name is null,u.first_name,CONCAT(u.first_name , ' ', u.last_name )) as participant, IF(m.user_from like ${user_from},1, 0) as from_me from message as m left join users as u on u.id = IF(m.user_from like ${user_from}, m.user_to,m.user_from) where m.id in (  SELECT MAX(id) FROM message WHERE user_from like ${user_from} OR user_to like ${user_from}  
    GROUP BY conversation_id )   AND (deleted_by is NULL OR deleted_by NOT like ${user_from})`
    if (keyword && keyword != "") {
      query += ` and IF(u.last_name is null,u.first_name,CONCAT(u.first_name , ' ', u.last_name )) like '%${keyword}%'  `
    }


    query += `  ORDER BY m.id DESC LIMIT ${limit} OFFSET ${offset}`;
    var list = await connection.query(query, [user_from, user_to]);

    let main_array = [];
    list.map(item => {
      delete item.password;
      main_array.push(item);
    })
    return main_array
  }

  static async getUserListing(keyword) {

    var query = `SELECT * from users where user_type = 1 `
    if (keyword) {
      query += `and IF(last_name is null, first_name,CONCAT(first_name,"",last_name)) like '%${keyword}%'  `
    }
    var user = await connection.query(query);
    let main_array = [];
    user.map(item => {
      delete item.password;
      main_array.push(item);
    })
    console.log(main_array)
    return main_array
  }

  static async getMessages(current_user, user_with, limit, page) {
    var offset = 0
    if (page && limit) {
      var offset = ((page < 1 ? 1 : page) - 1) * limit;
    } else if (limit) {
      var limit = limit
    } else {
      var limit = 20
    }
    const update_query = "UPDATE message SET is_read = ? WHERE  user_from = ? AND user_to = ?"
    await connection.query(update_query, [ 1 , user_with, current_user ]);

    const query = `SELECT * FROM (
      SELECT id, message, user_from, user_to ,is_read,created_at,IF(user_from like ${current_user},1, 0) as from_me FROM message WHERE (( user_from = ?   AND user_to = ? ) OR ( user_from = ? AND user_to = ?))  AND (deleted_by is NULL OR deleted_by NOT LIKE ${current_user}) ORDER BY id DESC 
    ) message  ORDER BY message.id DESC LIMIT ${limit} OFFSET ${offset}`;

    const result = await connection.query(query, [
      current_user,
      user_with,
      user_with,
      current_user
    ]);



    return result.reverse()

  }

  static async searchUser(name, id, block_by, status) {
    const query = `SELECT  IF(last_name is null, first_name,CONCAT(first_name,"",last_name)) as name, email 
    FROM users 
    WHERE  IF(last_name is null, first_name,CONCAT(first_name,"",last_name)) like ? AND id != ? AND id  NOT IN(SELECT blocked_user from block_user where  blocked_by = ? and status = ?)
    `;

    return await connection.query(query, [`%${name}%`, id, block_by, status]);
  }

  static async userDeviceInfo(id) {
    const query = `SELECT device_type, fcm_id from user_logins WHERE user_id = ? AND expired_at IS NULL`;
    return await connection.query(query, [id]);
  }

  static async blockUser(blocked_user, blocked_by) {
    const query = "select * from block_user where blocked_user=? and blocked_by=?"
    const response = await connection.query(query, [blocked_user, blocked_by])
    console.log(response)
    if (response.length > 0) {
      const query = `Delete from block_user where id = ? `;
      return await connection.query(query, response[0].id)
    } else {
      const query = `INSERT INTO block_user(blocked_user, blocked_by) values (?,?)`;
      return await connection.query(query, [blocked_user, blocked_by])
    }
    // const query = `INSERT INTO block_user(blocked_user, blocked_by) SELECT * from ( SELECT ?, ?) as temp WHERE NOT EXISTS( SELECT id from block_user WHERE blocked_user = ? AND blocked_by = ? AND status = 1)`;

  }

  static async unblockUser(status, blocked_user, blocked_by) {
    const query = `UPDATE block_user SET status = ? WHERE block_user = ? AND block_by = ?`;
    return await connection.query(query, [status, block_user, blocked_by]);
  }

  static async checkUserBlockStatus(blocked_user, blocked_by) {
    const query = `SELECT COUNT(id) as blocked_status from block_user WHERE blocked_user = ? AND blocked_by = ? `;
    var response = await connection.query(query, [blocked_user, blocked_by]);
    console.log(response)
    return response
  }
}

module.exports = ChatService;
