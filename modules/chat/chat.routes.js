const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");
const ChatController = require("./chat.controller");
const chatSchema = require("./chat.validation.json");
const auth = require("../../middleware/auth");
const valid = require("../../config/validator");

// GET /chat/listing
// POST /chat/message
// PUT /chat/message (read)
// DEL /chat/message (for me, for everyone)
// GET /chat/messages (old)
// PUT /user/online

router.get("/chat/listing", auth.checkToken, ChatController.getUserConversation);
router.get("/user/listing", ChatController.getUserListing);
router.post("/send/message", auth.checkToken, checkSchema(chatSchema.saveMessage), valid, ChatController.saveMessage);
router.delete("/chat/message", auth.checkToken, ChatController.deleteMessage);
router.delete("/chat/conversation", auth.checkToken, ChatController.deleteConversation);
router.get("/chat/messages", auth.checkToken, ChatController.getMessages);
router.post("/user/online", auth.checkToken, ChatController.userStatus);
router.get('/user/profile', ChatController.getUserProfile);
router.put("/message/seen", auth.checkToken, ChatController.messageSeen);
router.post("/search/user", auth.checkToken, ChatController.searchUser);
router.post("/block/user", auth.checkToken, ChatController.blockUser);
module.exports = router;
