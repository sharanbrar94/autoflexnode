const axios = require("axios");
var http = require("http");
var https = require("https");
var fs = require("fs");
// var rootCas = require('ssl-root-cas/latest').create();
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
// default for all https requests
// (whether using https directly, request, or another module)
// require('https').globalAgent.options.ca = rootCas;
var users = [];

module.exports = function (io) {
  io.on("connection", (socket) => {
    console.log("Socket running...");
    console.log("socket id", socket.id);
    socket.on("init", async (user) => {
      console.log("Socket init...");
      const data_temp = user;
      console.log(data_temp);
      await axios.post(
        `${process.env.API_URL}/api/user/online`,
        data_temp,
        {
          headers: { Authorization: `${data_temp.token}` }
        }
      );
      var user_id= user.user_from;
      var data={
        user_id:user_id,
        socket:socket.id
      }
      users.push(data)
      console.log(users)
      // users[user_id] = socket.id;
    });

    socket.on("message_sent", async (data) => {
      console.log("Socket message sent...");
      console.log(data)
      const data_temp = data;

      await axios.post(
        `${process.env.API_URL}/api/send/message`,
        data_temp,
        {
          headers: { Authorization: `${data_temp.token}` }
        }
      );
      const response_obj = {
        message: data_temp.message,
        sender: data_temp.user_from,
        createdAt: new Date(data_temp.date)
      };

      socket.to(users[user_id]).emit("message_received", response_obj);
      // socket.to(users[data_temp.user_from]).emit("message_received", response_obj);
    });

    socket.on("delete-message", async (data) => {
      await axios.delete(
        `${process.env.API_URL}/api/delete/message/:messageId/${data.message_id}`
      );
    });

    socket.on("disconnect", async (data) => {
      console.log("Socket disconnect...");
      const data_temp = data;
      console.log(data_temp);
      await axios.post(
        `${process.env.API_URL}/api/user/online`,
        data_temp,
        {
          headers: { Authorization: `${data_temp.token}` }
        }
      );
    });
  });
};
