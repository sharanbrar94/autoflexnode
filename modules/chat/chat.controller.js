const ChatService = require("../chat/chat.service");
const logger = require("../../config/logger");
const { off } = require("../../config/logger");
const _ = require("lodash");

class chatController {
  static async getUserConversation(req, res) {
    try {
      const { id } = req.user;
      const { keyword, page, limit } = req.query;

      const response = await ChatService.getUserConversation(id, id, page, limit, keyword);

      return res.status(200).json(response);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res
        .status(400)
        .json({ error: "bad_request", error_description: err.message });
    }
  }

  static async getUserListing(req, res) {
    try {
      const { keyword } = req.query;
      const response = await ChatService.getUserListing(keyword);

      return res.status(200).json(response);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res
        .status(400)
        .json({ error: "bad_request", error_description: err.message });
    }
  }

  static async saveMessage(req, res) {
    try {
      const { user_to, message, media, user_from } = req.body;



      const firstNumber = await Math.min(user_from, user_to);
      const secondNumber = await Math.max(user_from, user_to);
      const conversation_id = `${firstNumber}_${secondNumber}`;
      req.body.user_from = user_from;
      req.body.conversation_id = conversation_id;

      var blocked_user = await ChatService.checkBlockedUser(user_to, user_from);
      console.log(blocked_user)
      if (blocked_user.length > 0) {
        return res.status(400).json({
          error: "bad_request",
          error_description: "Something went wrong.",
        });
      } else {
        await ChatService.saveMessage(req.body);
        return res.status(200).json({ message: "Message saved!" });

      }

      // const deviceInfo = await ChatService.userDeviceInfo(user_to);
      // console.log(deviceInfo)
      // _.forEach(deviceInfo, (data) => {
      //   console.log("Element: ", data);

      //   // notif.push(data.device_type, message, data.fcm_id);

      //   require("../pushnotification/push").push(
      //     data.device_type,
      //     message,
      //     data.fcm_id
      //   );
      // });

    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong.",
      });
    }
  }

  static async deleteMessage(req, res) {
    try {
      const { deleted_for_me, id } = req.body;

      const { id: user } = req.user;

      let response = await ChatService.deleteMessage(deleted_for_me, user, id);

      if (response.affectedRows > 0) {
        return res.status(200).json({ message: "Message has been deleted!" });
      } else {
        return res.status(401).json({ message: "You can't delete this message" });
      }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async deleteConversation(req, res) {
    try {
      const { id: user_to } = req.body;

      const { id: user } = req.user;

      let response = await ChatService.deleteConversation(user, user_to);
      return res.status(200).json(response);
      // } else {
      // if (response > 0) {
      //   return res.status(200).json({ message: "Message has been deleted!" });
      // } else {
      //   return res.status(401).json({ message: "You can't delete this message" });
      // }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async userStatus(req, res) {
    try {
      const { status } = req.body;
      const { id } = req.user;
      await ChatService.userStatus(status, id);
      return res.status(200).json({ status: status, message: "status updated" });
    } catch (err) {
      console.log("error: ", err);
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async getUserProfile(req, res) {
    try {
      const { id: user_id } = req.query;
      let user = await ChatService.getUserProfile(user_id)
      delete user[0].password;
      return res.status(200).json(user[0])
    } catch (err) {
      console.log("error: ", err);
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong",
      });
    }
  }

  static async getMessages(req, res) {
    try {
      const { user_with, limit, page_id } = req.query;
      // console.log("Param:", req.params);

      const { id: current_user } = req.user;

      const findRes = await ChatService.checkUserBlockStatus(
        user_with,current_user
        
      );

      const response = {};
      response.blocked_status = findRes[0].blocked_status;

      var response = await ChatService.getMessages(
        current_user,
        user_with,
        limit,
        page_id
      );

      response.count = response.length;
      response.message = response;
      return res.status(200).json(response);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong" + err.message,
      });
    }
  }

  static async messageSeen(req, res) {
    try {
      const { id: user_to } = req.user;
      const { user_from } = req.body;
      let response = await ChatService.messageSeen(1, user_from, user_to);
      console.log("response: ", response);
      if (response.affectedRows > 0) {
        return res.status(200).json({ message: "Message seen!" });
      } else {
        return res.status(400).json({ message: "Error in executing your request." });
      }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong: ",
      });
    }
  }

  static async searchUser(req, res) {
    try {
      const { name } = req.body;
      const { id } = req.user;
      const response = await ChatService.searchUser(name, id, id, 1);

      return res.status(200).json(response);
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong" + err.message,
      });
    }
  }

  static async blockUser(req, res) {
    try {
      const { id: blocked_user } = req.body;
      const { id: blocked_by } = req.user;
  
      await ChatService.blockUser(blocked_user, blocked_by);

      return res.status(200).json({ message: "Status changed." });
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({
        error: "bad_request",
        error_description: "Something went wrong " + err.message,
      });
    }
  }


}

module.exports = chatController;
