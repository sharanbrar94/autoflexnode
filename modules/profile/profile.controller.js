const logger = require("../../config/logger"); // importiing winston logger module
const ProfileService = require("./profile.service");
class ProfileController {
  static async changePassword(req, res, next) {
    try {
      let user_id = req.user.id;
      console.log(user_id)
      let response = await ProfileService.userByID(user_id);
      console.log(response)
      let user_password = response[0].password;
      let old_password = req.body.old_password;
      let new_password = req.body.new_password;
      await ProfileService.changePassword(
        new_password,
        old_password,
        user_password,
        user_id
      );
      return res.status(200).json({ message: "Password changed" });
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res
        .status(400)
        .json({ error: "bad_request", error_description: err });
    }
  }

  static async updateUserProfile(req, res, next) {
    try {
      console.log(req.body);

      const {
        first_name,
        last_name,
        email,
        phone_number,
        country_code,
        address,
        city,
        state,
        country,
        zip,
        profile_image,
        lat,
        lng,
        country_abbr,
      } = req.body;
      const id = req.user.id;

      const data = await ProfileService.updateUserProfile(
        id,
        first_name,
        last_name,
        email,
        phone_number,
        country_code,
        address,
        city,
        state,
        country,
        zip,
        profile_image,
        lat,
        lng
      );

      if (data) {
        return res
          .status(200)
          .json({ message: "Profile updated successfully." });
      }
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res
        .status(400)
        .json({ error: "bad_request", error_description: err });
    }
  }

    //Get Content Details
    static async getContentDetail(req, res, next) {
      try {
        let id = req.query.id;
        const content = await ProfileService.getContentDetail(id);
        return res.status(200).json(content[0])
      }
      catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
        return res.status(400).json({ 'error': "bad_request", 'error_description': err });
      }
    }

  static async contactUs(req, res, next) {
    try {
      let user_id = req.user.id;
      const{ message,phone_number,country_code,first_name,last_name,email} = req.body;
      await ProfileService.contactUs( user_id,message,phone_number,country_code,first_name,last_name,email);
      return res.status(200).json({ message: "Success" });
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res
        .status(400)
        .json({ error: "bad_request", error_description: err });
    }
  }
}

module.exports = ProfileController;
