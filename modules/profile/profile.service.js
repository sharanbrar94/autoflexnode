const { sign } = require('jsonwebtoken');
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const Common = require('../../middleware/common')
const logger = require('../../config/logger');
class ProfileService {

    static async updateUserProfile(user_id, first_name, last_name, email, phone_number, country_code, address, city, state, country, zip, profile_image, lat, lng) {

        let query = []
        let param = []
        if (first_name) {
            query.push("first_name=?")
            param.push(first_name)
        }
        if (last_name) {
            query.push("last_name=?")
            param.push(last_name)
        }
        if (email) {
            query.push("email=?")
            param.push(email)
        }
        if (phone_number) {
            query.push("phone_number=?")
            param.push(phone_number)
        }
        if (country_code) {
            query.push("country_code=?")
            param.push(country_code)
        }
        if (address) {
            query.push("address=?")
            param.push(address)
        }
        if (city) {
            query.push("city=?")
            param.push(city)
        }
        if (state) {
            query.push("state=?")
            param.push(state)
        }
        if (country) {
            query.push("country=?")
            param.push(country)
        }
        if (zip) {
            query.push("zip=?")
            param.push(zip)
        }
        if (profile_image) {
            query.push("profile_image=?")
            param.push(profile_image)
        }
        if (lat) {
            query.push("lat=?")
            param.push(lat)
        }
        if (lng) {
            query.push("lng=?")
            param.push(lng)
        }

        if (country_abbr) {
            query.push("country_abbr=?")
            param.push(country_abbr)
        }


        param.push(user_id)

        let querystring = '';

        for (var i = 0; i < query.length; i++) {
            querystring = querystring + ' ' + query[i]
        }
        let querytrim = querystring.trim()

        let actualstring = querytrim.replace(/ /g, ",");


        let sqlQuery = 'update users set ' + actualstring + ' where id=?';
        console.log("query", query)
        console.log("param", param)
        console.log("string", actualstring)

        return await connection.query(sqlQuery, param)

    }
    static async getContentDetail(id) {
        return await connection.query('select * from static_pages where id=?', [id]);
    }
    static async changePassword(new_password, old_password, user_password, user_id) {
        let result = compareSync(old_password, user_password);
        console.log(result);
        if (!result) {
            throw ('Please enter correct old password!');
        } else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            let sqlQuery = `update users set password=? where id=?`
            let params = [password, user_id]
            var response = await connection.query(sqlQuery, params)
            console.log(response);
            return response;
        }
    }

    static async contactUs(user_id, message, phone_number, country_code, first_name, last_name, email) {
        // let subject = 'Contact Us'
        // const result = await Common.sendMail(email, message, subject);
        //console.log(result)
        return await connection.query('insert into contact_us set user_id=?, description=?, phone_number=?, country_code=?, first_name=?, last_name=?, email=?', [user_id, message, phone_number, country_code, first_name, last_name, email])


    }
    static async userByID(user_id) {
        return await connection.query('select * from users where users.id=?', [user_id])
    }
}

module.exports = ProfileService;