var express = require('express');
const { checkSchema } = require('express-validator');
let ProfileSchema = require("./profile.validation.json")
var router = express.Router();
const valid = require('../../config/validator')
const ProfileController = require("./profile.controller");
let auth = require("../../middleware/auth")
router.put('/change/password', auth.checkToken, checkSchema(ProfileSchema.changePassword), valid, ProfileController.changePassword);
router.put('/profile', auth.checkToken, ProfileController.updateUserProfile);
router.get('/content',  ProfileController.getContentDetail);
router.post('/contact-us',  auth.checkToken, checkSchema(ProfileSchema.email), valid, ProfileController.contactUs);
// checkSchema(ProfileSchema.ccontactUs)
module.exports = router;