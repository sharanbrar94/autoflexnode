var express = require('express');
var router = express.Router();
const multer = require('multer');
var cors = require("cors")
const { checkSchema } = require('express-validator');
let uploadSchema = require("./upload.validation.json")
const valid = require('../../config/validator')
const UploadController = require('./upload.controller')
const upload = multer({
    fileFilter(req, file, cb) {
    //    console.log(file)  
    //   if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
        cb(null, true);
    //   } else {
    //     cb(null, false);
    //     return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    //   }
    }
  });

//upload image
router.post('/upload/local', upload.single('file'), UploadController.uploadImageToLocal)
router.get('/local/file', checkSchema(uploadSchema.getImage), valid, UploadController.getFromLocal)

router.post('/upload/aws', upload.single('file'), UploadController.uploadImageToAws)
router.get('/aws/file', checkSchema(uploadSchema.getImage), valid, UploadController.getFromAws)

router.post('/upload/gc', upload.single('file'), UploadController.uploadImageToGc)
router.get('/gc/file', checkSchema(uploadSchema.getImage), valid, UploadController.getFromGc)

router.post('/upload/azure', upload.single('file'), UploadController.uploadImageToAzure)
router.get('/azure/file', checkSchema(uploadSchema.getImage), valid, UploadController.getFromAzure)





// "uploadImage": {
//     "file": {
//         "custom": {
//             "options": ("value", { "req", "path" }) => !!"req"."files"["path"],
//                 "errorMessage": "File is required."
//     }
// }
// },


module.exports = router;
