const { sign } = require('jsonwebtoken');
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const Common = require('../../middleware/common')
const logger = require('../../config/logger');
class WorkerService {

    static async getOrder(id, order_id) {
        var order = await connection.query('select o.*,ot.*,if(u1.last_name is NOT null ,CONCAT(u1.first_name," ",u1.last_name),u1.first_name) as worker_name,if(u.last_name is NOT null ,CONCAT(u.first_name," ",u.last_name),u.first_name) as user_name from orders as o left join order_items as ot on ot.order_id=o.id left join users as u on u.id=o.user_id left join users as u1 on u1.id=o.worker_id left join user_vehicles as u_v on u_v.id=o.vehicle_id left join user_addresses as u_a on u_a.id=o.address_id left join time_slots as t_s on t_s.id=o.arrival_time where o.id = ? and o.user_id=? and o.is_created=1', [order_id, id])
        return order[0]
    }

    static async getService(order_id) {
        var services = await connection.query('select * from order_items where order_id=? ', [order_id])
        var service_ids = [];
        for (var i in services) {
            service_ids.push(services[i].service_id)
        }
        let service_id = [...new Set(service_ids)];
        console.log("services id", service_id)
        var service = await connection.query('select s.id,c.name as category_name,sub.name as sub_category_name,comp.name as company_name,s.price,s.discount,s.service_time,s.reach_time,s.description,s.is_powered,s.is_blocked,s.created_at from services as s left join categories as c on c.id=s.category_id left join sub_categories as sub on sub.id =s.sub_category_id left join companies as comp on comp.id=s.company_id where s.id IN (?)', [service_id])
        console.log(service)
        for (var i in service) {
            var extras = await connection.query('select * from order_items where order_id =? and service_id=?', [order_id, service[i].id])
            var extra_ids = [];
            for (var i in extras) {
                extra_ids.push(extras[i].extra_service_id)
            }
            console.log("extra ids", extra_ids)
            if (extra_ids.length > 0) {
                var extra_services = await connection.query('select * from service_extras where id IN (?)', [extra_ids])
                service[i].extra_services = extra_services
            } else {
                service[i].extra_services = []
            }
            var service_highlights = await connection.query('select * from service_highlights where service_id =?', [service[i].id])
            service[i].highlights = service_highlights
        }
        return service
    }

    static async getOrders(id, status, limit, page) {
        // const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        // var limit = parseInt(lmt)
        // LIMIT ${ limit } OFFSET ${ offset }
        let orders = await this.getOrdersByWorkerId(id, status, limit, page)

        return orders
    }


    static async getCompanyById(id) {
        let sqlQuery = `select c.* from users as u left join companies as c on c.user_id=u.id where u.id=? and u.user_type = 2`
        var response = await connection.query(sqlQuery, [id])
        return response
    }

    static async getOrdersByWorkerId(id, status, limit, page) {
        const { offset: offset, limit: lmt } = await Common.calculateOffset(page, limit)
        var limit = parseInt(lmt)
        var query = "select o.id as order_id,o.*,uv.* from orders as o left join order_items as ot on ot.order_id=o.id left join user_vehicles as uv on uv.id=o.vehicle_id where o.worker_id = ? and o.is_created =1"
        if (status) {
            query += ` and o.status = ${status} `
        }
        query += ` LIMIT ${limit} OFFSET ${offset}`
        console.log("query", query)
        var orders = await connection.query(query, [id])
        console.log(orders)
        return orders
    }

    static async getOrderDetails(order_id) {
        var services = await connection.query('select * from order_items where order_id=?', [order_id])
        var service_ids = [];
        for (var i in services) {
            service_ids.push(services[i].service_id)
        }
        let service_id = [...new Set(service_ids)];

        var service = await connection.query('select s.id,c.name as category_name,sub.name as sub_category_name,comp.name as company_name,s.price,s.discount,s.service_time,s.reach_time,s.description,s.is_powered,s.is_blocked,s.created_at from services as s left join categories as c on c.id=s.category_id left join sub_categories as sub on sub.id =s.sub_category_id left join companies as comp on comp.id=s.company_id where s.id IN (?)', [service_id])

        for (var i in service) {
            var extras = await connection.query('select * from order_items where order_id =? and service_id=?', [order_id, service[i].id])
            var extra_ids = [];
            for (var i in extras) {
                extra_ids.push(extras[i].extra_service_id)
            }
            console.log("extra ids", extra_ids)
            if (extra_ids.length > 0) {
                var extra_services = await connection.query('select * from service_extras where id IN (?)', [extra_ids])
                service[i].extra_services = extra_services
            } else {
                service[i].extra_services = []
            }
            var service_highlights = await connection.query('select * from service_highlights where service_id = ?', [service[i].id])
            service[i].highlights = service_highlights
        }
        return service
    }

    static async editOrderStatus(id, order_id, status) {
        return await connection.query('update orders set status =? where id =? and user_id=? and is_created=1', [status, order_id, id])
    }

}

module.exports = WorkerService;