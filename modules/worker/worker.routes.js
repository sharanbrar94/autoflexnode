var express = require('express');
var router = express.Router();
const valid = require('../../config/validator')
const WorkerController = require("./worker.controller");
let auth = require("../../middleware/auth")



//Orders Management
router.get('/order/listing',auth.checkToken, WorkerController.getOrders);
router.get('/order-detail',auth.checkToken, WorkerController.getOrderDetail);
router.post('/order/status',auth.checkToken, WorkerController.editOrderStatus);



module.exports = router;