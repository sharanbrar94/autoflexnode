const { Console } = require("winston/lib/winston/transports");
const logger = require("../../config/logger"); // importiing winston logger module
const WorkerService = require("./worker.service");
class WorkerController {
    static async getOrder(req, res, next) {
        try {
            const { id } = req.user;
            const { id: order_id } = req.query;
            const order = await WorkerService.getOrder(id, order_id);
            const services = await WorkerService.getService(order_id);
            order['services'] = services

            return res.status(200).json(order);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }


    //Get Orders
    static async getOrders(req, res, next) {
        try {
            const { id } = req.user;
            const { status, limit, page } = req.query;
            console.log("status", status)
            const response = await WorkerService.getOrders(id, status, limit, page);
            return res.status(200).json(response);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }

    //get Order detail 
    static async getOrderDetail(req, res, next) {
        try {
            const { id } = req.user;
            const { id: order_id } = req.query;
            const order = await WorkerService.getOrder(id, order_id);
            const services = await WorkerService.getOrderDetails(order_id);
            order['services'] = services
            return res.status(200).json(order);

        }
        catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({ 'error': "bad_request", 'error_description': err });
        }
    }


        //Change Order Status
        static async editOrderStatus(req, res, next) {
            try {
                let id = req.user.id;
                const { id: order_id, status } = req.body;
                const response = await WorkerService.editOrderStatus(id, order_id, status);
                return res.status(200).json(response);
    
            }
            catch (err) {
                logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
                return res.status(400).json({ 'error': "bad_request", 'error_description': err });
            }
        }
}

module.exports = WorkerController;
