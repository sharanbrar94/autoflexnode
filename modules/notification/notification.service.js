module.exports = class NotificationService {
    static async saveNotification(data) {
      const query = `INSERT INTO notifications SET ?`;
      return await connection.query(query, [data]);
    }
  
    static async addNotificationUsers(notification_users) {
      const query = `INSERT INTO notification_users(notification_id, user_id) VALUES ?`;
      return await connection.query(query, [notification_users]);
    }
  
    static async getNotification(user_id) {
      const query = `SELECT title, message, created_at from notifications join notification_users on notifications.id = notification_users.notification_id WHERE user_id = ? order by created_at desc`;
      return await connection.query(query, [user_id]);
    }
  };
  
