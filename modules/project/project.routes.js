const express = require('express');
const router = express.Router();
const { checkSchema } = require('express-validator');
const valid = require('../../config/validator')
let auth = require("../../middleware/auth")
let projectSchema = require('./project.validation.json');

const ProjectController = require('./project.controller');

router.get('/projects', auth.checkToken, ProjectController.getProjects)
router.get('/project/:id', auth.checkToken, ProjectController.projectDetails);
router.post('/project', auth.checkToken, checkSchema(projectSchema.addProject), valid, ProjectController.addProject);
router.put('/project/:id', auth.checkToken, checkSchema(projectSchema.editProject), valid, ProjectController.editProject);
router.delete('/project/:id', auth.checkToken, ProjectController.deleteProject);

module.exports = router;