const e = require('express');
const logger = require('../../config/logger')
const ProjectService = require('./project.service');
class ProjectController {
    static async getProjects(req, res, next) {
        try {
            const result = await ProjectService.getProjects(req.user.id)
            console.log(result)
            return res.status(200).json(result)
        }
        catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            })
        }
    }
    static async projectDetails(req, res, next) {
        try {
            let project_id = req.params.id


            const result = await ProjectService.projectDetails(req.user.id, project_id)
            console.log(result)
            return res.status(200).json(result)


        }
        catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            })
        }
    }
    static async addProject(req, res, next) {
        try {
            let user_id = req.user.id
            let inputs = req.body;
            let title = inputs.title;
            let description = inputs.description;
            let price = inputs.price;
            let location = inputs.location;
            let images = inputs.images
            const result = await ProjectService.addProject(user_id, title, description, price, location, images);
            if (result) {
                return res.status(200).json({
                    message: 'Project Added'
                })
            }
        }
        catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            })
        }
    }
    static async editProject(req, res, next) {
        try {
            let project_id = req.params.id
            const result = await connection.query('select * from projects where projects.id=? and user_id=?', [project_id, req.user.id]);
            let user_id = req.user.id
            let inputs = req.body;
            let title = inputs.title || inputs.title == null ? inputs.title : result[0].title;
            let description = inputs.description || inputs.description == null ? inputs.description : result[0].description;
            let price = inputs.price || inputs.price == null ? inputs.price : result[0].price;
            let location = inputs.location || inputs.location == null ? inputs.location : result[0].location;
            let images = inputs.images;
            const result_1 = await ProjectService.editProject(user_id, project_id, title, description, price, location, images);
            if (result_1) {
                return res.status(200).json(result_1)
            }


            return res.status(200).json({
                message: 'Project Updated'
            })


        }
        catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            })
        }
    }
    static async deleteProject(req, res, next) {
        try {
            let project_id = req.params.id;
            const result = await ProjectService.deleteProject(req.user.id, project_id)

            return res.status(200).json(result)

            return res.status(200).json({ message: "Project deleted" })

        }
        catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            })
        }
    }


}
module.exports = ProjectController;