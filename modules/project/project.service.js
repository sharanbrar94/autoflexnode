class ProjectService {

  static async getProjects(user_id) {
      const projects = await connection.query('select * from projects where user_id=? order by id desc', [user_id]);
      if (projects.length > 0) {
          const images_result = await this.getImages();
          projects.forEach(project => {
              var main_array = [];
              images_result.filter(image => {
                  if (image.project_id == project.id) {
                      main_array.push(image.image);
                  }
              })
              project.images = main_array;
          })
          return projects
      }
      else {
          return projects;
      }
  }
  static async getImages() {
      return await connection.query('select project_id ,image from project_images ')
  }
  static async projectDetails(user_id, project_id) {
      const projects = await connection.query('select * from projects where user_id=? and projects.id=?', [user_id, project_id])
      if (projects.length > 0) {
          const images_result = await this.getImages();
          var main_array = [];
          images_result.filter(image => {
              if (image.project_id == projects[0].id) {
                  main_array.push(image.image);
              }
          })
          projects[0].images = main_array;
          return projects[0]
      }
      else {
          return projects;
      }
  }
  static async addProject(user_id, title, description, price, location, images) {
      const result = await connection.query('insert into projects set user_id=?,title=?,description=?,price=?,location=?', [user_id, title, description, price, location]);
      if (result) {
          const project_images = [];
          images.forEach(image => {
              project_images.push([result.insertId, image])
          })
          return await connection.query('insert into project_images (project_id,image) values ? ', [project_images])
      }
  }
  static async editProject(user_id, project_id, title, description, price, location, images) {
      const result = await connection.query('update projects set title=?,description=?,price=?,location=? where projects.id=? and user_id=?', [title, description, price, location, project_id, user_id]);
      if (result) {
          const result_2 = await connection.query('delete from project_images where  project_id=?', [project_id]);
          if (images.length == 0) {
              return
          }
          const values = [];
          images.forEach(image => {
              values.push([project_id, image])
          })
          const result_3 = await connection.query('insert into project_images (project_id,image) values ?', [values]);
          if(result.affectedRows>0){
              return ({message:'project updated'})
          }
          else{
              return ({error:"bad_request", error_description:"You can't update this project"})
          }
      }
  }
  static async deleteProject(user_id, project_id) {
      const result = await connection.query('delete from projects where id=? and user_id=? ', [project_id, user_id]);
      if (result) {
          console.log("result :",result)
          const result_2 = await connection.query('delete from project_images where project_id=?', [project_id])
          if(result.affectedRows>0){
              console.log(1)
              return {message:'project deleted...'}
          }
          else{
              console.log(2)
              return {"error":"bad_request", "error_description":"You can't delete this project"}
          }
      }
  }

    static async getProjects(user_id) {
        const projects = await connection.query('select * from projects where user_id=? order by id desc', [user_id]);
        if (projects.length > 0) {
            const images_result = await this.getImages();
            projects.forEach(project => {
                var main_array = [];
                images_result.filter(image => {
                    if (image.project_id == project.id) {
                        main_array.push(image.image);
                    }
                })
                project.images = main_array;
            })
            return projects
        }
        else {
            return projects;
        }

    }

    static async getImages() {
        return await connection.query('select project_id ,image from project_images ')
    }

    static async projectDetails(user_id, project_id) {
        const projects = await connection.query('select * from projects where user_id=? and projects.id=?', [user_id, project_id])
        if (projects.length > 0) {
            const images_result = await this.getImages();
            var main_array = [];
            images_result.filter(image => {
                if (image.project_id == projects[0].id) {
                    main_array.push(image.image);
                }
            })
            projects[0].images = main_array;
            return projects[0]
        }
        else {
            return projects;
        }

    }

    static async addProject(user_id, title, description, price, location, images) {
        const result = await connection.query('insert into projects set user_id=?,title=?,description=?,price=?,location=?', [user_id, title, description, price, location]);
        if (result) {
            const project_images = [];
            images.forEach(image => {
                project_images.push([result.insertId, image])
            })
            return await connection.query('insert into project_images (project_id,image) values ? ', [project_images])
        }
    }

    static async editProject(user_id, project_id, title, description, price, location, images) {
        const result = await connection.query('update projects set title=?,description=?,price=?,location=? where projects.id=? and user_id=?', [title, description, price, location, project_id, user_id]);
        if (result) {
            const result_2 = await connection.query('delete from project_images where  project_id=?', [project_id]);

            if (images.length == 0) {
                return
            }

            const values = [];
            images.forEach(image => {
                values.push([project_id, image])
            })
            const result_3 = await connection.query('insert into project_images (project_id,image) values ?', [values]);
            return result

        }
    }

    static async deleteProject(user_id, project_id) {
        const result = await connection.query('delete from projects where projects.id=? and user_id=? ', [project_id, user_id]);
        if (result) {
            return await connection.query('delete from project_images where project_id=?', [project_id])
        }
    }


}
module.exports = ProjectService;